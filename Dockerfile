FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/beebeecar-rest-*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]