package com.ithive.beebeecarrest.securities;

import com.ithive.beebeecarrest.resources.UtilisateurResources;
import lombok.Data;

import java.util.Date;

@Data
public class UserToken {

    public UserToken(Date pDateLimite, UtilisateurResources pUtilisateurResources){
        this.dateLimite = pDateLimite;
        this.utilisateurResources = pUtilisateurResources;
    }

    private Date dateLimite;
    private UtilisateurResources utilisateurResources;


}
