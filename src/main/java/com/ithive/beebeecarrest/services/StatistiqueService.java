package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.model.sp.*;
import com.ithive.beebeecarrest.repositories.StoredProcedureRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.statistique.*;
import com.ithive.beebeecarrest.utilities.DateUtilities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StatistiqueService {






    // Fonction permettant d'obtenur le taux d'occupation des 12 derniers mois de la flotte des véhicules
    public static Object GetTauxOccupationVehicule(
            TauxOccupationInputResources resources,
            TokenRessources token,
            UtilisateurRepository utilisateurRepository,
            StoredProcedureRepository storedProcedureRepository){
        try {

            // On vérifie que l'utilisateur est bien connecté
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            if(utilisateur == null)
                return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);

            // On set la date de référence au premier jour du mois recherché
            Date dateReference = DateUtilities.convertStringToUtilDate(resources.getDateReference());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateReference);
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            // Appel de la procédure stockée
            List<TauxOccupationVehicule> tauxOccupation = storedProcedureRepository.getTauxOccupationVehiculeForLastYear(
                    utilisateur.getOrgOrganisme().getOrgId(),
                    calendar.getTime()
            );

            List<TauxOccupationParcOutputResources> result = new ArrayList<>();
            for (TauxOccupationVehicule res : tauxOccupation){
                TauxOccupationParcOutputResources item = new TauxOccupationParcOutputResources(res);
                result.add(item);
            }
            return result;
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }


    // Fonction permettant de récupérer le taux d'occupation des sites mois par mois
    public static Object GetTauxOccupationSite(
            TauxOccupationInputResources resources,
            TokenRessources token,
            UtilisateurRepository utilisateurRepository,
            StoredProcedureRepository storedProcedureRepository){
        try {

            // On vérifie que l'utilisateur est bien connecté
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            if(utilisateur == null)
                return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);

            // On set la date de référence au premier jour du mois recherché
            Date dateReference = DateUtilities.convertStringToUtilDate(resources.getDateReference());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateReference);
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            // Appel de la procédure stockée
            List<TauxOccupationSite> tauxOccupation = storedProcedureRepository.getTauxOccupationSiteForLastYear(
                    utilisateur.getOrgOrganisme().getOrgId(),
                    calendar.getTime()
            );


            List<TauxOccupationSiteOutputResources> result = new ArrayList<>();

            for (TauxOccupationSite res : tauxOccupation){
                TauxOccupationSiteOutputResources item = new TauxOccupationSiteOutputResources(res);
                result.add(item);
            }
            return result;
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }

    // Fonction permettant de récupérer l'état du parc
    public static Object GetEtatParc(
            TauxOccupationInputResources input,
            TokenRessources token,
            StoredProcedureRepository storedProcedureRepository,
            UtilisateurRepository utilisateurRepository
    ){
        try {
            // Récupération de l'utilisateur via son token d'authentification
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            // On converti la date de string à objet Date
            Date dateReference = DateUtilities.convertStringToUtilDate(input.getDateReference());
            // On appelle la procédure sotckée
            EtatParc etatParc = storedProcedureRepository.getEtatParc(utilisateur.getOrgOrganisme().getOrgId(), dateReference);

            return new EtatParcResources(etatParc);
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }

    public static Object GetNombreReservationsParSiteParMois(
            TauxOccupationInputResources input,
            TokenRessources token,
            StoredProcedureRepository storedProcedureRepository,
            UtilisateurRepository utilisateurRepository){
        try {
            // Récupération de l'utilisateur via son token d'authentification
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            // On converti la date de string à objet Date
            Date dateReference = DateUtilities.convertStringToUtilDate(input.getDateReference());

            // Appel de la procédure stockée
            List<NombreReservationParSite> nbReservation = storedProcedureRepository.getNombreReservationParSite(
                    utilisateur.getOrgOrganisme().getOrgId(),
                    dateReference
            );

            List<NombreReservationSiteOutputResources> resources = new ArrayList<>();

            for(NombreReservationParSite reservationParSite : nbReservation){
                resources.add(new NombreReservationSiteOutputResources(reservationParSite));
            }

            return resources;
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }

    // Fonction retournant le nombre d'utilisateurs actifs sur les 12 derniers mois
    public static Object GetNombreUtilisateurActif(
            TauxOccupationInputResources input,
            TokenRessources token,
            StoredProcedureRepository storedProcedureRepository,
            UtilisateurRepository utilisateurRepository){
        try {
            // Récupération de l'utilisateur via son token d'authentification
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            // On converti la date de string à objet Date
            Date dateReference = DateUtilities.convertStringToUtilDate(input.getDateReference());


            // Appel de la procédure stockée
            List<NombreUtilisateurActif> nbUsers = storedProcedureRepository.getNombreUtilisateurActifMonthByMonth(
                    utilisateur.getOrgOrganisme().getOrgId(),
                    dateReference
            );

            List<NombreUtilisateurActifOutputResources> resources = new ArrayList<>();
            for (NombreUtilisateurActif nbUser : nbUsers){
                resources.add(new NombreUtilisateurActifOutputResources(nbUser));
            }

            return nbUsers;
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }

    public static Object GetNombreReservationsParSiteAndEtatParMois(
            TauxOccupationInputResources input,
            TokenRessources token,
            StoredProcedureRepository storedProcedureRepository,
            UtilisateurRepository utilisateurRepository){
        try{
            // Récupération de l'utilisateur via son token d'authentification
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
            // On converti la date de string à objet Date
            Date dateReference = DateUtilities.convertStringToUtilDate(input.getDateReference());


            // Appel de la procédure stockée
            List<NombreReservationParSiteParEtat> nbUsers = storedProcedureRepository.getNombreReservationParSiteParEtat(
                    utilisateur.getOrgOrganisme().getOrgId(),
                    dateReference
            );

            List<NombreReservationSiteEtatOutputResources> resources = new ArrayList<>();

            for(NombreReservationParSiteParEtat nbUser : nbUsers){
                resources.add(new NombreReservationSiteEtatOutputResources(nbUser));
            }

            return resources;
        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }
    /*public static Object getUtilisateurActif(
            TauxOccupationInputResources input,
            TokenRessources token,
            DemandeLocationRepository demandeRepository,
            UtilisateurRepository utilisateurRepository){
        try {
            // Récupération de l'utilisateur via son token d'authentification
            Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);

            // On converti la date de string à objet Date
            Date dateReference = DateUtilities.convertStringToUtilDate(input.getDateReference());

            // Récupération et filtrage des demandes
            List<DemandeLocation> demandes = demandeRepository.findAll().stream()
                    .filter((demande) ->
                            demande.getUtiUtilisateur().getOrgOrganisme().getOrgId() == utilisateur.getOrgOrganisme().getOrgId()
                                    && DateUtilities.isSameDay(demande.getDemDatedebut(), dateReference))
                    .collect(Collectors.toList());


        }catch (Exception e){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
        }
    }*/
}
