package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the eng_entretiengarage database table.
 * 
 */
@Entity
@Table(name="eng_entretiengarage")
@NamedQuery(name="EntretienGarage.findAll", query="SELECT e FROM EntretienGarage e")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "engId")
public class EntretienGarage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ENG_ID", unique=true, nullable=false)
	private int engId;

	//bi-directional many-to-one association to VehVehicule
	@ManyToOne
	@JoinColumn(name="ENG_VEH_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Vehicule vehVehicule;

	public EntretienGarage() {
	}

	public int getEngId() {
		return this.engId;
	}

	public void setEngId(int engId) {
		this.engId = engId;
	}

	public Vehicule getVehVehicule() {
		return this.vehVehicule;
	}

	public void setVehVehicule(Vehicule vehVehicule) {
		this.vehVehicule = vehVehicule;
	}

}