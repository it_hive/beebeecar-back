package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.exceptions.VehiculeNotFoundException;
import com.ithive.beebeecarrest.model.Marque;
import com.ithive.beebeecarrest.model.Site;
import com.ithive.beebeecarrest.model.Vehicule;
import com.ithive.beebeecarrest.repositories.HistoriqueKilometrageRepository;
import com.ithive.beebeecarrest.repositories.MarqueRepository;
import com.ithive.beebeecarrest.repositories.ModeleRepository;
import com.ithive.beebeecarrest.repositories.SiteRepository;
import com.ithive.beebeecarrest.repositories.StoredProcedureRepository;
import com.ithive.beebeecarrest.repositories.VehiculeRepository;
import com.ithive.beebeecarrest.resources.ArchiveVehiculeResources;
import com.ithive.beebeecarrest.resources.ModeleResources;
import com.ithive.beebeecarrest.resources.VehiculeResources;
import com.ithive.beebeecarrest.services.EmailService;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.repositories.*;
import com.ithive.beebeecarrest.resources.*;
import com.ithive.beebeecarrest.resources.Vehicule.GetVehiculesDispoResources;
import com.ithive.beebeecarrest.services.ModeleService;
import com.ithive.beebeecarrest.services.UtilisateurService;
import com.ithive.beebeecarrest.services.VehiculeService;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class VehiculeController {

	private final VehiculeRepository repository;
	private final SiteRepository repositorySite;
	private final MarqueRepository repositoryMarque;
	private final ModeleRepository repositoryModele;
	private final HistoriqueKilometrageRepository repositoryHistoriqueKilometrage;
	private final UtilisateurRepository repositoryUtilisateur;
	private final StoredProcedureRepository spv;

	private static Logger logger = LogManager.getLogger(VehiculeController.class);
	
	public VehiculeController(
			VehiculeRepository repository, 
			SiteRepository repositorySite, 
			MarqueRepository repositoryMarque, 
			ModeleRepository repositoryModele,
			HistoriqueKilometrageRepository repositoryHistoriqueKilometrage,
			UtilisateurRepository repositoryUtilisateur,
			StoredProcedureRepository spv) {
		this.repository = repository;
		this.repositorySite = repositorySite;
		this.repositoryMarque = repositoryMarque;
		this.repositoryModele = repositoryModele;
		this.repositoryHistoriqueKilometrage = repositoryHistoriqueKilometrage;
		this.repositoryUtilisateur = repositoryUtilisateur;
		this.spv = spv;
	}

	/**
	 * Procédure de test de fonctionnalité
	 */
	@GetMapping("/procedure-test")
	public void procedureTest() {
		
		try {
			spv.getReservationsForToday(Long.valueOf("1"), DateUtilities.convertStringToUtilDate("17/05/2019"));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("test");
	}
	
	@GetMapping("/voitures")
	public List<VehiculeResources> all() {
		return VehiculeService.BotoVos(repository.findAll());
	}

	@GetMapping("/testmail")
	public void testmail() {
		System.out.println("Test mail !");
		
		EmailResources email = EmailService.createBody(null, null, null, false);
		
		EmailService.send(email);

	}
//	@GetMapping("/test")
//	public void test() {
//		EmailService.readTemplate("U");
//	}
	
//	@GetMapping("/test")
//	public List<Utilisateur> test() {
//		System.out.println("Test !");
//		
//		boolean vIn = true;
//		byte vOut = (byte)(vIn?1:0);
//		
//		System.out.println(vOut);
//		return repositoryUtilisateur.findAllByIsAdmin(vOut);
//	}
//	

	@PostMapping("/voiture/new")
	public VehiculeResources newVoiture(@RequestBody VehiculeResources newVoiture) {
		logger.info("/voiture/new");
		return VehiculeService.createBO(repository, repositorySite, repositoryModele, newVoiture);
	}

	@GetMapping("/voiture/{id}")
	public VehiculeResources one(@PathVariable Long id) {		
		return VehiculeService.BOToVO(repository.findById(id)
				.orElseThrow(() -> new VehiculeNotFoundException(id)));
	}

	@PutMapping("/voiture/{id}")
	public VehiculeResources replaceVoiture(@RequestBody VehiculeResources newVoiture, @PathVariable Long id) {

		return VehiculeService.BOToVO(repository.findById(id)
			.map(voiture -> { 
				voiture = VehiculeService.VOToBO(newVoiture, repositorySite, repositoryModele);
				return repository.save(voiture);
			})
			.orElseGet(() -> {
				return repository.save(VehiculeService.VOToBO(newVoiture, repositorySite, repositoryModele));
			}));
	}

	@DeleteMapping("/voiture/{id}")
	public void deleteVoiture(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
	@GetMapping("/marques")
	public List<Marque> allMarque() {		
		return repositoryMarque.findAll();
	}
	
	@GetMapping("/modeles/{marId}")
	public List<ModeleResources> allModeleFromMarque(@PathVariable Long marId) {
		
		return ModeleService.BotoVos(repositoryModele.findAllByMarMarque_marId(marId));
	}
	
	@GetMapping("/voitures/org/{orgId}")
	public List<VehiculeResources> allFromOrganisme(@PathVariable Long orgId) {	
		
		//Liste des sites de l'organisme
		List<Site> allSites = repositorySite.findAllByOrgOrganisme_orgId(orgId);
		List<Long> allSitesIds = new ArrayList<Long>();
		for(Site s : allSites) {
			allSitesIds.add(s.getSitId());
		}
		return VehiculeService.BotoVos(repository.findAllBySitSite_sitIdIsIn(allSitesIds));
	}
	
//	@GetMapping()
	@GetMapping(path = "/voitures/sit/{sitId}", params = { "page", "size" })
	public List<VehiculeResources> allFromSite(@PathVariable Long sitId) {	
		
		return VehiculeService.BotoVos(repository.findAllBySitSite_sitId(sitId));
	}
	
	@PutMapping("/voiture/archivage/{id}")
	public VehiculeResources archivage(@RequestBody ArchiveVehiculeResources archiveVehicule, @PathVariable Long id) {
		
		VehiculeResources vehRes = VehiculeService.BOToVO(repository.findById(id)
				.orElseThrow(() -> new VehiculeNotFoundException(id)));
		vehRes.setDateFinService(archiveVehicule.getDateFindeService());
		
		return VehiculeService.replaceVehicule(repository, repositorySite, vehRes, repositoryModele);
		 
	}

	// Route récupérant la liste des véhicules dispos pour une période de temps, en fonction de l'organisme de l'utilisateur
	@PostMapping("/voiture/vehiculeDispos/{userToken}")
	public Object vehiculesDispoForPeriode(@RequestBody GetVehiculesDispoResources activiteVehicule, @PathVariable String userToken) {

		// On récupère l'utilisateur à partir de son token
		TokenRessources token = new TokenRessources();
		token.setToken(userToken);
		Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, repositoryUtilisateur);
		//Si pas d'utilisateur récupéré, renvoie d'un échec
		if(utilisateur == null)
			return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);

		return VehiculeService.GetVehiculesForPeriode(activiteVehicule, utilisateur, spv);

	}
}
