package com.ithive.beebeecarrest.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtilities {

	private static Logger logger = LogManager.getLogger(DateUtilities.class);
	
	public static Date convertStringToUtilDate(String dateString) throws ParseException {
		if (dateString == null || dateString.isEmpty()) {
			return null;
		}
		logger.debug("convertStringToUtilDate - String : " + dateString);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.parse(dateString);
	}
	
	public static String convertUtilDateToString(Date date) {
		if (date == null) {
			return null;
		}
		logger.debug("convertStringToUtilDate - Date : " + date.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}


	// Fonction permettant de savoir si 2 dates sont le même jour
	public static boolean isSameDay(Date date1, Date date2){
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
				cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);

		return sameDay;
	}

	// Fonction permettant de savoir si une date de référence est compris entre 2 dates
	public static boolean isBetweenDate(Date dateRef, Date date1, Date date2){
		if((dateRef.after(date1) || isSameDay(dateRef, date1)) && (dateRef.before(date2) || isSameDay(dateRef, date2)))
			return true;
		return false;
	}
}
