package com.ithive.beebeecarrest.exceptions;

public class OrganismeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrganismeNotFoundException(Long id) {
		super("Impossible de trouver l'organisme #" + id);
	}
}
