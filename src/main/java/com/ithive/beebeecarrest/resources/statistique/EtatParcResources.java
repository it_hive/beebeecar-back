package com.ithive.beebeecarrest.resources.statistique;

import com.ithive.beebeecarrest.model.sp.EtatParc;
import lombok.Data;

@Data
public class EtatParcResources {

    private Integer NombreVehiculeDisponible;

    private Integer NombreVehiculeIndisponible;

    public EtatParcResources(){}

    public EtatParcResources(EtatParc etat){
        setNombreVehiculeDisponible(etat.getVehiculeDispo());
        setNombreVehiculeIndisponible(etat.getVehiculeIndispo());
    }
}
