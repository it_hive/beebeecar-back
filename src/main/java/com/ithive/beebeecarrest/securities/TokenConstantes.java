package com.ithive.beebeecarrest.securities;

public class TokenConstantes {


    public static final String HEADER = "BeeBeeCarHeader";
    public static final Long SECONDES = 86400000l; // 24 h
    public static final String SECRET = "PhraseSecretQuePersonneNeDoitSavoir";
}
