package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class SiteResources {

	private Long sitId;

	private String adresse;

	private String codePostal;

	private String nom;

	private String ville;
    
	private Long orgId;
	
	public SiteResources() {
		super();
	}
	
}
