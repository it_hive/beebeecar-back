package com.ithive.beebeecarrest.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ithive.beebeecarrest.model.Modele;
import com.ithive.beebeecarrest.model.Site;

public interface ModeleRepository extends JpaRepository<Modele, Long> {
	
	List<Modele> findAllByMarMarque_marId(Long marId);
	
}
