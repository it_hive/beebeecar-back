package com.ithive.beebeecarrest.resources;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
public class ModeleResources {

	private Long modId;

	private String libelle;
    
	public ModeleResources() {
		super();
	}
	
}
