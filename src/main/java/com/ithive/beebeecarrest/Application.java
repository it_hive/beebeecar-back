package com.ithive.beebeecarrest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import com.ithive.beebeecarrest.repositories.UtilisateurRepository;

@SpringBootApplication
@ComponentScan({"com.ithive.beebeecarrest"})
@EnableSwagger2
//@EntityScan("com.ithive.beebeecarrest.model")
//@EnableJpaRepositories("com.ithive.beebeecarrest.repository")
public class Application {

	
//	@Autowired
//	private UtilisateurRepository userRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
