package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.exceptions.DemandeLocationNotFoundException;
import com.ithive.beebeecarrest.exceptions.UtilisateurNotFoundException;
import com.ithive.beebeecarrest.exceptions.VehiculeNotFoundException;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.model.sp.ActiviteVehicule;
import com.ithive.beebeecarrest.repositories.*;
import com.ithive.beebeecarrest.resources.ActiviteVehiculeInputResources;
import com.ithive.beebeecarrest.resources.ActiviteVehiculeOutputResources;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.services.DemandeLocationService;
import com.ithive.beebeecarrest.services.UtilisateurService;
import com.ithive.beebeecarrest.services.VehiculeService;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class PlanningController {

	private final StoredProcedureRepository storedProcedureRepository;
	private final VehiculeRepository vehiculeRepository;
	private final DemandeLocationRepository demLocRepository;
	private final UtilisateurRepository utilisateurRepository;

	private static Logger logger = LogManager.getLogger(PlanningController.class);

	private UtilisateurService utilisateurService;
	
	public PlanningController(StoredProcedureRepository storedProcedureRepository,
							  VehiculeRepository vehiculeRepository,
							  DemandeLocationRepository demLocRepository,
							  UtilisateurRepository utilisateurRepository,
							  OrganismeRepository pOrganismeRepo,
							  EtatDemandeRepository pEtatDemandeRepo) {

		this.storedProcedureRepository = storedProcedureRepository;
		this.vehiculeRepository = vehiculeRepository;
		this.demLocRepository = demLocRepository;
		this.utilisateurRepository = utilisateurRepository;

		this.utilisateurService = new UtilisateurService(utilisateurRepository, pOrganismeRepo, pEtatDemandeRepo, demLocRepository);
	}

	@PostMapping("/planning/{userToken}")
	public Object planning(@RequestBody ActiviteVehiculeInputResources newRequest, @PathVariable String userToken) {
		
//		List<ActiviteVehicule> actList = storedProcedureRepository.getActiviteVehiculeOrganismeForPeriode(Long.valueOf("29"), new Date(119, 3, 1), new Date(119, 3, 21));
		logger.info("Requête planning : " + newRequest.toString());

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		List<ActiviteVehicule> actList;
		List<ActiviteVehiculeOutputResources> outputs = new ArrayList<>();
		try {
			// On vérifie que l'utilisateur est bien connecté
			Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
			if(utilisateur == null)
				return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);

			actList = storedProcedureRepository.getActiviteVehiculeOrganismeForPeriode(
					Long.valueOf(utilisateur.getOrgOrganisme().getOrgId()),
					DateUtilities.convertStringToUtilDate(newRequest.getDateDeb()), 
					DateUtilities.convertStringToUtilDate(newRequest.getDateFin())
				);
			
//			actList = storedProcedureRepository.getActiviteVehiculeOrganismeForPeriode(
//					Long.valueOf(1),
//					DateUtilities.convertStringToUtilDate(newRequest.getDateDeb()), 
//					DateUtilities.convertStringToUtilDate(newRequest.getDateFin())
//				);
			
			for(ActiviteVehicule act : actList) {
				ActiviteVehiculeOutputResources output = new ActiviteVehiculeOutputResources();
				output.setVehicule(VehiculeService.BOToVO(vehiculeRepository.findById(act.getVehId())
						.orElseThrow(() -> new VehiculeNotFoundException(act.getVehId()))));
				output.setDemandeLocation(DemandeLocationService.BOToVO(demLocRepository.findById(act.getDemId())
						.orElseThrow(() -> new DemandeLocationNotFoundException(act.getDemId()))));
				output.setUtilisateur(this.utilisateurService.boToVoSimple(utilisateurRepository.findById(act.getUtiId())
						.orElseThrow(() -> new UtilisateurNotFoundException(act.getUtiId()))));
				output.setDtdDate(DateUtilities.convertUtilDateToString(act.getDtdDate()));
				output.setEdmLibelle(act.getEdmLibelle());
				outputs.add(output);
			}
		} catch (Exception e) {
//			e.printStackTrace();
			return MessageErreur.creaMessageErreur(e.getMessage());
		}
		
		return outputs;
	}
	
}
