package com.ithive.beebeecarrest.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.ithive.beebeecarrest.model.*;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ithive.beebeecarrest.exceptions.ModeleNotFoundException;
import com.ithive.beebeecarrest.exceptions.OrganismeNotFoundException;
import com.ithive.beebeecarrest.exceptions.SiteNotFoundException;
import com.ithive.beebeecarrest.model.Site;
import com.ithive.beebeecarrest.repositories.MarqueRepository;
import com.ithive.beebeecarrest.repositories.ModeleRepository;
import com.ithive.beebeecarrest.repositories.OrganismeRepository;
import com.ithive.beebeecarrest.repositories.SiteRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.repositories.VehiculeRepository;
import com.ithive.beebeecarrest.resources.SiteResources;
import com.ithive.beebeecarrest.resources.VehiculeResources;

public class SiteService {

	private static Logger logger = LogManager.getLogger(SiteService.class);

	private SiteRepository siteRepository;
	private OrganismeRepository organismeRepository;
	private VehiculeRepository vehiculeRepository;

	public SiteService(SiteRepository p_siteRepository, OrganismeRepository p_organismeRepository,
			VehiculeRepository p_vehiculeRepository){
		this.siteRepository = p_siteRepository;
		this.organismeRepository = p_organismeRepository;
		this.vehiculeRepository = p_vehiculeRepository;
	}

	public List<SiteResources> getAllSite(UtilisateurResources utilisateurConnect){
        return BOtoVOs(this.siteRepository.findAllByOrgOrganisme_orgId(utilisateurConnect.getIdOrganisme()));
    }

	public List<SiteResources> BotoVos(List<Site> listBO) {
		List<SiteResources> listVO = new LinkedList<SiteResources>();
		for (Site bo : listBO) {
			listVO.add(BOToVO(bo));
		}
		return listVO;
	}

	public Site VOToBO(
			SiteResources vo) {
		Site bo = new Site();
		
		bo.setSitId(vo.getSitId());
		bo.setSIT_Adresse(vo.getAdresse());
		bo.setSIT_CodePostal(vo.getCodePostal());
		bo.setSIT_Nom(vo.getNom());
		bo.setSIT_Ville(vo.getVille());
		bo.setOrgOrganisme(this.organismeRepository.findById(vo.getOrgId())
				.orElseThrow(() -> new OrganismeNotFoundException(vo.getOrgId())));
		
		return bo;
	}
	
	public SiteResources BOToVO(Site bo) {
		SiteResources vo = new SiteResources();

		vo.setSitId(bo.getSitId());
		vo.setAdresse(bo.getSIT_Adresse());
		vo.setCodePostal(bo.getSIT_CodePostal());
		vo.setNom(bo.getSIT_Nom());
		vo.setVille(bo.getSIT_Ville());
		vo.setOrgId(bo.getOrgOrganisme().getOrgId());

		return vo;
	}

	public List<SiteResources> BOtoVOs(List<Site> sites){
	    List<SiteResources> listRessource = new LinkedList<>();
	    for(Site site : sites){
	        listRessource.add(BOToVO(site));
        }
	    return  listRessource;
    }
	
	public Object createBO(SiteResources vo, UtilisateurResources utiConnect) {

	    // vérification droit, l'utilisateur doit être un admin
        if(!utiConnect.isAdmin()){
            MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_CREATION_ADMIN);
        }
	    // on passe l'id de l'organsime avec l'utilisateur connecté
        vo.setOrgId(utiConnect.getIdOrganisme());

		logger.debug("Création SITE");
		Site bo = VOToBO(vo);
		
		this.siteRepository.save(bo);
		return BOToVO(bo);
	}

	public Object delete(Long siteId,  UtilisateurResources utiConnect){

        // vérification droit, l'utilisateur doit être un admin
        if(!utiConnect.isAdmin()){
            MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_CREATION_ADMIN);
        }

		Optional<Site> optional = this.siteRepository.findById(siteId);
		if(optional.isPresent()){
			Site site = optional.get();

			if(!site.getVehVehicules().isEmpty()){
				return MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_VEHICULE_EXISTANT);
			}

			logger.info("suppression du site : " + site.getSitId() + " - " + site.getSIT_Nom());
			this.siteRepository.delete(site);
			return MessageErreur.createMessageErreur(MessageErreur.CODE_OK);

		} else {
			return MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_INEXISTANT);
		}

	}

	public Object modification(SiteResources siteResources, UtilisateurResources utilisateurConnect){

        // vérification droit, l'utilisateur doit être un admin
        if(!utilisateurConnect.isAdmin()){
            MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_CREATION_ADMIN);
        }

	    // on récupère l'objet bo par l'id
        Optional<Site> opt = this.siteRepository.findById(siteResources.getSitId());

        // on vérifie que le site avec l'id
        if(opt.isPresent()){
            Site bo = opt.get();

            bo.setSIT_Adresse(siteResources.getAdresse());
            bo.setSIT_CodePostal(siteResources.getCodePostal());
            bo.setSIT_Nom(siteResources.getNom());
            bo.setSIT_Ville(siteResources.getVille());

            this.siteRepository.save(bo);

            return BOToVO(bo);

        } else {
            return MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_INEXISTANT);
        }

    }

    public Object getSite(long id, UtilisateurResources utilisateurConnect){

	    Optional<Site> opt = this.siteRepository.findById(id);
	    if(opt.isPresent()){
	        Site site = opt.get();

	        // vérification que l'utilisateiur connecté est bien du bon organisme
            if(site.getOrgOrganisme().getOrgId().equals(utilisateurConnect.getIdOrganisme())){
                return this.BOToVO(site);
            } else {
                return MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_DROIT_ACCES);
            }
        } else {
            return MessageErreur.createMessageErreur(MessageErreur.CODE_SITE_INEXISTANT);
        }
    }
	
}
