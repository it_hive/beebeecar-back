package com.ithive.beebeecarrest.model.sp;

import java.util.Date;

import lombok.Data;

@Data
public class ReservationToday {

	private Long demId;
	private String utiNom;
	private String utiPrenom;
	private String edmLibelle;
	private Date demDateDemande;
	private Date demDateDebut;
	private Date demDateFin;
	private Byte demAllowCovoiturage;
	private String modLibelle;
	private String marLibelle;


	public ReservationToday() {
		super();
	}

	public ReservationToday(Long demId, String utiNom, String utiPrenom, String edmLibelle, Date demDateDemande,
			Date demDateDebut, Date demDateFin, Byte demAllowCovoiturage, String modLibelle, String marLibelle) {
		super();
		this.demId = demId;
		this.utiNom = utiNom;
		this.utiPrenom = utiPrenom;
		this.edmLibelle = edmLibelle;
		this.demDateDemande = demDateDemande;
		this.demDateDebut = demDateDebut;
		this.demDateFin = demDateFin;
		this.demAllowCovoiturage = demAllowCovoiturage;
		this.modLibelle = modLibelle;
		this.marLibelle = marLibelle;
	}
	
}
