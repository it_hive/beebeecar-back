package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.Vehicule;
import com.ithive.beebeecarrest.model.sp.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class StoredProcedureRepositoryImpl implements StoredProcedureRepository {

	@PersistenceContext
	private EntityManager entityManager;

	private static Logger logger = LogManager.getLogger(StoredProcedureRepositoryImpl.class);

	private final String GET_ACTIVITE_VEHICULE_ORGANISME_FOR_PERIODE = "CALL sp_GetActiviteVehiculeOrganismeForPeriode(:inOrganismeId, :inDateDebut, :inDateFin)";
	private final String GET_VEHICULE_ORGANISME_ACTIF_FOR_PERIODE = "CALL sp_GetVehiculeOrganismeActifForPeriode(:inOrganismeId, :inDateDebut, :inDateFin)";
	private final String GET_RESERVATION_FOR_TODAY = "CALL sp_GetReservationsForToday(:inOrganismeId, :inDateRef)";
	private final String GET_VEHICULE_DISPO_PERIODE = "CALL sp_GetVehiculeDispoForPeriode(:inOrganismeId, :inDateDebut, :inDateFin)";
	private final String GET_TAUX_OCCUPATION_VEHICULE = "CALL sp_GetTauxOccupationVehiculeByMonth(:inOrganismeId, :inDateRef)";
	private final String GET_TAUX_OCCUPATION_SITE = "CALL sp_GetTauxOccupationSiteByMonth(:inIdOrganisme, :inDateRef)";
	private final String GET_ETAT_PARC = "CALL sp_GetEtatParc(:inIdOrganisme, :inDateRef)";
	private final String GET_NOMBRE_RESERVATION_SITE = "CALL sp_GetNombreDemandeParMoisParSite(:inIdOrganisme, :inDateRef)";
	private final String GET_NOMBRE_RESERVATION_SITE_ETAT = "CALL sp_GetNombreDemandeParMoisParSiteAndEtat(:inIdOrganisme, :inDateRef)";
	private final String GET_NOMBRE_UTILISATEUR_ACTIF = "CALL sp_GetNombreUtilisateurActifsMonthByMonth(:inIdOrganisme, :inDateRef)";

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ActiviteVehicule> getActiviteVehiculeOrganismeForPeriode(
			Long organismeId, 
			Date dateDebut,
			Date dateFin) {

		@SuppressWarnings("unchecked")
		List<Object[]> results = this.em.createNativeQuery(GET_ACTIVITE_VEHICULE_ORGANISME_FOR_PERIODE)
				.setParameter("inOrganismeId", organismeId)
				.setParameter("inDateDebut", dateDebut)
				.setParameter("inDateFin", dateFin).getResultList();

		List<ActiviteVehicule> actList = results.stream()
				.map(result -> new ActiviteVehicule((Long)((Integer)result[0]).longValue(), 
						(Long)((Integer)result[1]).longValue(), 
						(Byte) ((Integer) ((Boolean) result[2]?1:0)).byteValue(),
						(Long)((Integer)result[3]).longValue(), 
						(String) result[4], 
						(String) result[5], 
						(Date) result[6], 
						(String) result[7]))
				.collect(Collectors.toList());
		
		logger.info("Retour procedure" + actList.toString());
		logger.info("After proc");

		return actList;

	}

	@Override
	public List<Vehicule> getAllActifVehiculeByOrganisme(
			Long organismeId, 
			Date dateDebut, 
			Date dateFin) {

		logger.info("OrganismeId : " + organismeId);
		logger.info("DateDebut : " + dateDebut.toString());
		logger.info("DateFin : " + dateFin.toString());
				
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(dateDebut);
//		cal.set(Calendar.HOUR_OF_DAY, 0);
//		cal.set(Calendar.MINUTE, 0);
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MILLISECOND, 0);
//		
//		Calendar cal2 = Calendar.getInstance();
//		cal2.setTime(dateFin);
//		cal2.set(Calendar.HOUR_OF_DAY, 0);
//		cal2.set(Calendar.MINUTE, 0);
//		cal2.set(Calendar.SECOND, 0);
//		cal2.set(Calendar.MILLISECOND, 0);
//		
//		logger.debug("DateDebut : " + cal.getTime().toString());
//		logger.debug("DateFin : " + cal2.getTime().toString());
		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
//		@SuppressWarnings("unchecked")
//		List<Vehicule> results = this.em.createNativeQuery(GET_VEHICULE_ORGANISME_ACTIF_FOR_PERIODE, Vehicule.class)
//				.setParameter("inOrganismeId", organismeId)
//				.setParameter("inDateDebut", cal.getTime())
//				.setParameter("inDateFin", cal2.getTime())
//				.getResultList();
		
		@SuppressWarnings("unchecked")
		List<Vehicule> results = this.em.createNativeQuery(GET_VEHICULE_ORGANISME_ACTIF_FOR_PERIODE, Vehicule.class)
				.setParameter("inOrganismeId", organismeId)
				.setParameter("inDateDebut", dateDebut)
				.setParameter("inDateFin", dateFin)
				.getResultList();
		
		logger.info(results);
		
		return results;
	}

	@Override
	public List<ReservationToday> getReservationsForToday(Long organismeId, Date dateRef) {

		List<Object[]> results = this.em.createNativeQuery(GET_RESERVATION_FOR_TODAY)
				.setParameter("inOrganismeId", organismeId)
				.setParameter("inDateRef", dateRef).getResultList();
		
//		private Long demId;
//		private String utiNom;
//		private String utiPrenom;
//		private String edmLibelle;
//		private Date demDateDemande;
//		private Date demDateDebut;
//		private Date demDateFin;
//		private Byte demAllowCovoiturage;
//		private String modLibelle;
//		private String marLibelle;
		
		List<ReservationToday> resList = results.stream()
				.map(result -> new ReservationToday((Long)((Integer)result[0]).longValue(), 
						(String) result[1], 
						(String) result[2],
						(String) result[3], 
						(Date) result[4], 
						(Date) result[5], 
						(Date) result[6],
						(Byte) ((Integer) ((Boolean) result[7]?1:0)).byteValue(),
						(String) result[8],
						(String) result[9]))
				.collect(Collectors.toList());
		
		logger.info(resList);
		
		return null;
	}

	// Fonction permettant de récupérer la lsite des véhicules disponibles pour une période donnée
	@Override
	public List<Vehicule> getVehiculesDispoForPeriode(
			Long organismeId,
			Date dateDebut,
			Date dateFin) {

		logger.info("OrganismeId : " + organismeId);
		logger.info("DateDebut : " + dateDebut.toString());
		logger.info("DateFin : " + dateFin.toString());


		@SuppressWarnings("unchecked")
		List<Vehicule> results = this.em.createNativeQuery(GET_VEHICULE_DISPO_PERIODE, Vehicule.class)
				.setParameter("inOrganismeId", organismeId)
				.setParameter("inDateDebut", dateDebut)
				.setParameter("inDateFin", dateFin)
				.getResultList();

		logger.info(results);

		return results;
	}

	@Override
	// Fonction permettant de récupérer le taux d'occupation des véhicules de l'organisme sur l'année passée
	public List<TauxOccupationVehicule> getTauxOccupationVehiculeForLastYear(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_TAUX_OCCUPATION_VEHICULE)
				.setParameter("inOrganismeId", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();

		List<TauxOccupationVehicule> tauxOccupation = results.stream()
				.map(result -> new TauxOccupationVehicule(
						((Integer)result[0]).longValue(),
						(String) result[1],
						((BigDecimal) result[2]).floatValue(),
						((BigDecimal) result[3]).floatValue(),
						((BigDecimal) result[4]).floatValue(),
						((BigDecimal) result[5]).floatValue(),
						((BigDecimal) result[6]).floatValue(),
						((BigDecimal) result[7]).floatValue(),
						((BigDecimal) result[8]).floatValue(),
						((BigDecimal) result[9]).floatValue(),
						((BigDecimal) result[10]).floatValue(),
						((BigDecimal) result[11]).floatValue(),
						((BigDecimal) result[12]).floatValue(),
						((BigDecimal) result[13]).floatValue()))
				.collect(Collectors.toList());

		logger.info("Retour procedure" + tauxOccupation.toString());
		logger.info("After proc");

		return tauxOccupation;
	}

	@Override
	public List<TauxOccupationSite> getTauxOccupationSiteForLastYear(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_TAUX_OCCUPATION_SITE)
				.setParameter("inIdOrganisme", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();

		List<TauxOccupationSite> tauxOccupation = results.stream()
				.map(result -> new TauxOccupationSite(
						((Integer)result[0]).longValue(),
						(String) result[1],
						((BigDecimal) result[2]).floatValue(),
						((BigDecimal) result[3]).floatValue(),
						((BigDecimal) result[4]).floatValue(),
						((BigDecimal) result[5]).floatValue(),
						((BigDecimal) result[6]).floatValue(),
						((BigDecimal) result[7]).floatValue(),
						((BigDecimal) result[8]).floatValue(),
						((BigDecimal) result[9]).floatValue(),
						((BigDecimal) result[10]).floatValue(),
						((BigDecimal) result[11]).floatValue(),
						((BigDecimal) result[12]).floatValue(),
						((BigDecimal) result[13]).floatValue()))
				.collect(Collectors.toList());

		logger.info("Retour procedure" + tauxOccupation.toString());
		logger.info("After proc");

		return tauxOccupation;
	}

	// Fonction appelant la procédure stockée d'état du parc
	@Override
	public EtatParc getEtatParc(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_ETAT_PARC)
				.setParameter("inIdOrganisme", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();

		Object[] result = results.get(0);

		EtatParc etatParc = new EtatParc();
		etatParc.setVehiculeDispo((Integer) result[0]);
		etatParc.setVehiculeIndispo((Integer) result[1]);

		return etatParc;
	}

	@Override
	// Fonction appelant la procédure stockée retournant le nombre de réservation par mois par site
	public List<NombreReservationParSite> getNombreReservationParSite(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_NOMBRE_RESERVATION_SITE)
				.setParameter("inIdOrganisme", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();

		List<NombreReservationParSite> nombreReservationParSites = results.stream()
				.map(result -> new NombreReservationParSite(
						((Integer)result[0]).longValue(),
						(String) result[1],
						((Integer) result[2]),
						((Integer) result[3]),
						((Integer) result[4]),
						((Integer) result[5]),
						((Integer) result[6]),
						((Integer) result[7]),
						((Integer) result[8]),
						((Integer) result[9]),
						((Integer) result[10]),
						((Integer) result[11]),
						((Integer) result[12]),
						((Integer) result[13])))
				.collect(Collectors.toList());

		return nombreReservationParSites;

	}

	@Override
	public List<NombreReservationParSiteParEtat> getNombreReservationParSiteParEtat(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_NOMBRE_RESERVATION_SITE_ETAT)
				.setParameter("inIdOrganisme", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();

		List<NombreReservationParSiteParEtat> nombreReservationParSites = results.stream()
				.map(result -> new NombreReservationParSiteParEtat(
						((Integer)result[0]).longValue(),
						(String) result[1],
						((Integer)result[2]).longValue(),
						(String) result[3],
						((Integer) result[4]),
						((Integer) result[5]),
						((Integer) result[6]),
						((Integer) result[7]),
						((Integer) result[8]),
						((Integer) result[9]),
						((Integer) result[10]),
						((Integer) result[11]),
						((Integer) result[12]),
						((Integer) result[13]),
						((Integer) result[14]),
						((Integer) result[15])))
				.collect(Collectors.toList());

		return nombreReservationParSites;

	}

	@Override
	// Appel de la procédure stockée retournant le nombre d"utilisateur actif mois par mois
	public List<NombreUtilisateurActif> getNombreUtilisateurActifMonthByMonth(Long organismeId, Date dateReference) {
		List<Object[]> results = this.em.createNativeQuery(GET_NOMBRE_UTILISATEUR_ACTIF)
				.setParameter("inIdOrganisme", organismeId)
				.setParameter("inDateRef", dateReference).getResultList();


		List<NombreUtilisateurActif> nombreReservationParSites = results.stream()
				.map(result -> new NombreUtilisateurActif(
						((Integer) result[0]),
						((Integer) result[1]),
						((Integer) result[2]),
						((Integer) result[3]),
						((Integer) result[4]),
						((Integer) result[5]),
						((Integer) result[6]),
						((Integer) result[7]),
						((Integer) result[8]),
						((Integer) result[9]),
						((Integer) result[10]),
						((Integer) result[11])))
				.collect(Collectors.toList());

		return nombreReservationParSites;
	}
}
