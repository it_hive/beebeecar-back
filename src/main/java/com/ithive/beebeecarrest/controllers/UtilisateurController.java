package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.model.Organisme;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.repositories.DemandeLocationRepository;
import com.ithive.beebeecarrest.repositories.EtatDemandeRepository;
import com.ithive.beebeecarrest.repositories.OrganismeRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.resources.ConnexionRessources;
import com.ithive.beebeecarrest.resources.EmailResources;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import com.ithive.beebeecarrest.securities.TokenRegister;
import com.ithive.beebeecarrest.services.EmailService;
import com.ithive.beebeecarrest.services.UtilisateurService;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class UtilisateurController {

    private UtilisateurService utilisateurService;

    private static Logger logger = LogManager.getLogger(UtilisateurController.class);

    public UtilisateurController(UtilisateurRepository pRepository, OrganismeRepository pOrganismeRepo,
                                 EtatDemandeRepository pEtatDemandeRepo, DemandeLocationRepository pDemandeLocationRepo){

        this.utilisateurService = new UtilisateurService(pRepository, pOrganismeRepo, pEtatDemandeRepo, pDemandeLocationRepo);

    }

    /**
     * Route qui retourne l'ensemble des utilisateurs qui sont dans le même organisme que l'utilisateur connecté
     *
     * Erreurs :
     *   501 -> L'utilisateur n'est pas connecté
     * @param token token de l'utilisateur connecté
     * @return List</UtilisateurRessource>liste des utilisateurs du même organisme
     */
    @GetMapping("/utilisateurs")
    public Object all(@RequestHeader(name = "token", defaultValue = "")String token){
        logger.debug(" /utilisateur - all");

        // Récupération de l'utilisateur connecté à travers le token
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        // récupérer les autres utilisateurs du même organisme
        return this.utilisateurService.getUtilisateurByOrganisme(utiConnecte);
    }

    /**
     * Route qui ajoute en base un nouvel utilisateur dans l'organisme de l'utilisateur connecté
     *
     * Erreurs ;
     *   501 -> L'utilisateur n'est pas connecté
     *   206 -> L'utilisateur connecté n'est pas un admin, il n'a donc pas le droit de créer un nouvel utilisateur
     *   203 -> Création d'un utilisateur, le numéro de téléphone n'est pas au bon format
     *   204 -> Création d'un utilisateur, le mail n'est pas au bon format
     *   205 -> création d'un utilisateur, un utilisateur avec le même mail et le même organisme existe déjà
     *
     * @param newUtilisateur données du nouvel utilisateur et token de l'utilisateur connecté
     * @return UtilisateurResources utilisateur ajouté dans la base de données
     */
    @PostMapping("/utilisateur/new")
    public Object newUtilisateur(@RequestHeader(name = "token", defaultValue = "")String token,
                                 @RequestBody UtilisateurResources newUtilisateur) throws NoSuchAlgorithmException {
        logger.debug("post /utilisateurs - newUtilisateur");

        // Récupération de l'utilisateur connecté à travers le token
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        return this.utilisateurService.createBO(newUtilisateur, utiConnecte);

    }

    /**
     * Permet la connexion d'un utilisateur
     * @param connexionRessources contient le mail/identifiant et le mot de passe
     * @return TokenRessources objet contenant le token de connexion
     */
    @PostMapping("/utilisateur/connexionToken")
    public Object connectionUtilisateurToken(@RequestBody ConnexionRessources connexionRessources) throws NoSuchAlgorithmException {
        return this.utilisateurService.connexionToken(connexionRessources);
    }

    /**
     * Retourne l'utilisateur connecté
     * @param token token de l'utilisateur connecté
     * @return UtilisateurRessouce utilisateur connecté
     */
    @GetMapping("/utilisateur/utilisateurConnecte/")
    public Object getUtilisateur(@RequestHeader(name = "token", defaultValue = "")String token){
        logger.debug(" utilisateur connecté :");
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);

        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        // on récupère l'utilisateur en base, si jamais il a changé
        return this.utilisateurService.getUtilisateurByID(utiConnecte.getIdUtilisateur(), utiConnecte);

    }

    /**
     * Route pour récupérer les données d'un utilisateur en particulier (ils doivent être dans le même organisme)
     * @param token utilisateur connecté
     * @param utilisateurId id de l'utilisateur demandé
     * @return UtilisarteurRessource
     */
    @GetMapping("utilisateur/{utilisateurId}")
    public Object getUtilisateurById (@RequestHeader(name = "token", defaultValue = "")String token, @PathVariable Long utilisateurId){

        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        return this.utilisateurService.getUtilisateurByID(utilisateurId, utiConnecte);
    }

    /**
     * Permet de modifier un utilisateur
     * @param newVtilisateur données de l'utilisateur
     * @param id id de l'utilisateur (qui ne changera pas)
     * @return UtilisateurRessource utilisateur enregistré en base
     */
    @PutMapping("/utilisateur/{id}")
    public Object replaceUtilisateur(@RequestHeader(name = "token", defaultValue = "")String token,
                                     @RequestBody UtilisateurResources newVtilisateur, @PathVariable Long id) throws NoSuchAlgorithmException {

        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        newVtilisateur.setIdUtilisateur(id);

        return this.utilisateurService.replaceUtilisateur(newVtilisateur, utiConnecte);
    }

    /**
     *
     * @param token
     * @param id
     * @param connexionRessources
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PutMapping("/utilisateur/paswword/{id}")
    public Object replacePassword(@RequestHeader(name = "token", defaultValue = "")String token, @PathVariable Long id, @RequestBody ConnexionRessources connexionRessources)  throws NoSuchAlgorithmException {
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }
        return this.utilisateurService.replacePassword(connexionRessources, utiConnecte, id);
    }

    /**
     * Permet d'archiver un utilisateur
     * @param utilisateurId id de l'utilisateur à archiver
     * @param token token de l'utilisateur connecté
     * @return UtilisateurRessource utilisateur archivé
     */
    @GetMapping("utilisateur/archivage/{utilisateurId}")
    public Object archivageUtilisateur(@PathVariable Long utilisateurId,  @RequestHeader(name = "token", defaultValue = "")String token){

        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

        return this.utilisateurService.archivageUtilisateur(utilisateurId, utiConnecte);
    }
    
    /**
     * Permet d'envoyer le mail d'identifiant
     * @param utilisateurId id de l'utilisateur
     * @param token token de l'utilisateur connecté
     * @return
     */
    @GetMapping("utilisateur/email/{utilisateurId}")
    public Object envoieMail(@PathVariable Long utilisateurId, @RequestHeader(name = "token", defaultValue = "")String token){

        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }
		
		Organisme org = this.utilisateurService.getOrganismeFromUtilisateur(utiConnecte);
		List<Utilisateur> utis = this.utilisateurService.getAdmin(org);
		
        EmailResources email = EmailService.createBody(utiConnecte, null, utis, false);
        EmailService.send(email);
        
        return "OK";
    }
    
	@GetMapping("/testmailuti")
	public void testmail() {
		System.out.println("Test mail !");
		
		UtilisateurResources utiConnecte = new UtilisateurResources();
		utiConnecte.setIdUtilisateur(1);
		utiConnecte.setIdOrganisme(1);
		utiConnecte.setMail("jo.leclerc@hotmail.fr");
		Organisme org = this.utilisateurService.getOrganismeFromUtilisateur(utiConnecte);
		
		List<Utilisateur> utis = this.utilisateurService.getAdmin(org);
		
		EmailResources email = EmailService.createBody(utiConnecte, null, utis, false);
		
		EmailService.send(email);

	}
    
}
