package com.ithive.beebeecarrest.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ithive.beebeecarrest.exceptions.VehiculeNotFoundException;

@ControllerAdvice
class VoitureAdvice {

	@ResponseBody
	@ExceptionHandler(VehiculeNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String voitureNotFoundHandler(VehiculeNotFoundException ex) {
		return ex.getMessage();
	}
}