package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.resources.*;
import com.ithive.beebeecarrest.securities.TokenRegister;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import com.ithive.beebeecarrest.repositories.OrganismeRepository;
import com.ithive.beebeecarrest.repositories.SiteRepository;
import com.ithive.beebeecarrest.repositories.VehiculeRepository;
import com.ithive.beebeecarrest.services.SiteService;

@RestController
@CrossOrigin
public class SiteController {

	private SiteService siteService;

	private static Logger logger = LogManager.getLogger(SiteController.class);
	
	public SiteController(SiteRepository repository, VehiculeRepository VEHrepository, OrganismeRepository orgRepository) {
		this.siteService = new SiteService(repository, orgRepository, VEHrepository);
	}

	/**
	 * Récupère l'ensemble des sites en fonction de l'organisme auquel l'utilisateur est connecté
	 * @param token token de l'utilisateur connecté
	 * @return List<SiteRessource>
	 */
	@GetMapping("/sites")
	public Object all(@RequestHeader(name = "token", defaultValue = "")String token) {

		logger.debug("sites de l'organisme");
		// Récupération de l'utilisateur connecté à travers le token
		UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
		if(utiConnecte == null){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
		}
		return this.siteService.getAllSite(utiConnecte);
	}

	/**
	 * Permet de créer un nouveau site (l'utilisateur connecté doit être un admin)
	 * @param newSite nouveau site
	 * @param token token de l'utilisateur connecté
	 * @return Site créé
	 */
	@PostMapping("/site/new")
	public Object newSite(@RequestHeader(name = "token", defaultValue = "")String token, @RequestBody SiteResources newSite) {

		// Récupération de l'utilisateur connecté à travers le token
		UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
		if(utiConnecte == null){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
		}

		return this.siteService.createBO(newSite, utiConnecte);
	}

	/**
	 * Permet de modifier un site (l'utilisateur connecté doit être un admin)
	 * @param newSite site a modifier
	 * @param id id du site
	 * @return SiteRessource site modifié
	 */
	@PutMapping("/site/{id}")
	public Object replaceSite(@RequestHeader(name = "token", defaultValue = "")String token,
							  @RequestBody SiteResources newSite, @PathVariable Long id) {

		// Récupération de l'utilisateur connecté à travers le token
		UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
		if(utiConnecte == null){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
		}

		newSite.setSitId(id);
		return  this.siteService.modification(newSite, utiConnecte);

	}

	/**
	 * Route pour récupérer un site
	 * @param token token de l'utilisateur connecté
	 * @param siteId id du site à récupérer
	 * @return SiteRessource
	 */
	@GetMapping("/site/{siteId}")
	public Object unSite(@RequestHeader(name = "token", defaultValue = "")String token, @PathVariable Long siteId) {

        // Récupération de l'utilisateur connecté à travers le token
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

		return this.siteService.getSite(siteId, utiConnecte);
	}

	/**
	 * Pour supprimer un site
	 * @param token token de l'utilisateur connecté
	 * @param siteId id du site
	 * @return code ok
	 */
	@DeleteMapping("/site/{siteId}")
	public Object deleteSite(@RequestHeader(name = "token", defaultValue = "")String token, @PathVariable Long siteId){

        // Récupération de l'utilisateur connecté à travers le token
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }

		return this.siteService.delete(siteId, utiConnecte);
	}
}
