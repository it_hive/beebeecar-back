package com.ithive.beebeecarrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ithive.beebeecarrest.model.Organisme;

public interface OrganismeRepository extends JpaRepository<Organisme, Long> {

}
