package com.ithive.beebeecarrest.exceptions;

public class SiteNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SiteNotFoundException(Long id) {
		super("Impossible de trouver le site " + id);
	}
}
