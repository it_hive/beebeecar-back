package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Set;


/**
 * The persistent class for the edm_etatdemande database table.
 * 
 */
@Entity
@Table(name="edm_etatdemande")
@NamedQuery(name="EtatDemande.findAll", query="SELECT e FROM EtatDemande e")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "edmId")
public class EtatDemande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EDM_ID", unique=true, nullable=false)
	private int edmId;

	@Column(length=40, name = "EDM_Libelle")
	private String libelle;

	//bi-directional many-to-one association to DemDemandelocation
	@OneToMany(mappedBy="edmEtatdemande")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<DemandeLocation> demDemandelocations;

	public EtatDemande() {
	}

	public int getEdmId() {
		return this.edmId;
	}

	public void setEdmId(int edmId) {
		this.edmId = edmId;
	}

	public String getEDM_Libelle() {
		return this.libelle;
	}

	public void setEDM_Libelle(String EDM_Libelle) {
		this.libelle = EDM_Libelle;
	}

	public Set<DemandeLocation> getDemDemandelocations() {
		return this.demDemandelocations;
	}

	public void setDemDemandelocations(Set<DemandeLocation> demDemandelocations) {
		this.demDemandelocations = demDemandelocations;
	}

	public DemandeLocation addDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().add(demDemandelocation);
		demDemandelocation.setEdmEtatdemande(this);

		return demDemandelocation;
	}

	public DemandeLocation removeDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().remove(demDemandelocation);
		demDemandelocation.setEdmEtatdemande(null);

		return demDemandelocation;
	}

}