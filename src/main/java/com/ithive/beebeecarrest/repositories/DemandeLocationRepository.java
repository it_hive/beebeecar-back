package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.DemandeLocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DemandeLocationRepository extends JpaRepository<DemandeLocation, Long> {

    List<DemandeLocation> findByOrderByDemDateDemandeAsc();


    List<DemandeLocation> findByOrderByDemDateDemandeDesc();

}
