package com.ithive.beebeecarrest.controllers;

import java.util.List;

import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.OrganismeResources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import com.ithive.beebeecarrest.securities.TokenRegister;
import com.ithive.beebeecarrest.services.OrganismeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import com.ithive.beebeecarrest.repositories.OrganismeRepository;

@RestController
@CrossOrigin
public class OrganismeController {

	private OrganismeService organismeService;

	private static Logger logger = LogManager.getLogger(OrganismeController.class);
	
	public OrganismeController(OrganismeRepository repository) {
		this.organismeService = new OrganismeService(repository);
	}

	/**
	 * retourne l'organisme de la personne connecté
	 * @return OrganismeResource
	 */
	@GetMapping("/organismes")
	public List<OrganismeResources> all() {
		logger.info("Mapping : /organismes");
		return this.organismeService.findAll();
	}

	@GetMapping("/organisme")
    public Object getOrganisme(@RequestHeader(name = "token", defaultValue = "")String token){
	    logger.debug("getOrganisme ");

        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token);
        if(utiConnecte == null){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);
        }
        return this.organismeService.getOrganismeByUtilisateur(utiConnecte);
    }

}
