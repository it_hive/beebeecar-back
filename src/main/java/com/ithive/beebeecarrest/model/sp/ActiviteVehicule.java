package com.ithive.beebeecarrest.model.sp;

import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

@Data
public class ActiviteVehicule {

	private Long vehId;

	private Long demId;

	private Byte demAllowCovoiturage;

	private Long utiId;

	private String utiNom;

	private String utiPrenom;

	private Date dtdDate;

	private String edmLibelle;

	public ActiviteVehicule() {
		super();
	}

	public ActiviteVehicule(Long vehId, Long demId, Byte demAllowCovoiturage, Long utiId, String utiNom,
			String utiPrenom, Date dtdDate, String edmLibelle) {
		super();
		this.vehId = vehId;
		this.demId = demId;
		this.demAllowCovoiturage = demAllowCovoiturage;
		this.utiId = utiId;
		this.utiNom = utiNom;
		this.utiPrenom = utiPrenom;
		this.dtdDate = dtdDate;
		this.edmLibelle = edmLibelle;
	}
	
}
