package com.ithive.beebeecarrest.resources.demandeLocation;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.Etape;
import com.ithive.beebeecarrest.resources.VehiculeResources;
import com.ithive.beebeecarrest.services.VehiculeService;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class NouvelleDemandeLocationResources {

    private Long demId;

    private String dateDepart;

    private String dateArrivee;

    private byte allowCovoiturage;

    private Long idVehicule;

    private Long idSite;

    private String commentaireLibre;

    private String commentaireCovoiturage;

    private List<EtapeResources> etapes;

    private String etatDemande;
    
    private String dateDemande;
    
    private VehiculeResources vehicule;
    
    private String nomSite;
    
    public NouvelleDemandeLocationResources(){
        super();
    }

    public NouvelleDemandeLocationResources(DemandeLocation demande){
        setDemId(demande.getDemId());
        setDateDepart(DateUtilities.convertUtilDateToString(demande.getDemDatedebut()));
        setDateArrivee(DateUtilities.convertUtilDateToString(demande.getDemDatefin()));
        setAllowCovoiturage(demande.getDEM_AllowCovoiturage());
        setCommentaireLibre(demande.getCommentaireLibre());
        setCommentaireCovoiturage(demande.getCommentaireCovoiturage());
        setEtatDemande(demande.getEdmEtatdemande().getEDM_Libelle());
        if(demande.getVehVehicule() != null) {
        	setIdVehicule(demande.getVehVehicule().getVehId());  
        	setVehicule(VehiculeService.BOToVO(demande.getVehVehicule()));
        } else {
        	setIdVehicule(null);        	
        }
        setIdSite(demande.getSitSite().getSitId());
        //Ajout des etapes
        List<EtapeResources> etapes = new ArrayList<>();
        for(Etape etape : demande.getEtpEtape().stream().collect(Collectors.toList())){
            etapes.add(new EtapeResources(etape));
        }
        setEtapes(etapes);
        setDateDemande(DateUtilities.convertUtilDateToString(demande.getDemDateDemande()));
        setNomSite(demande.getSitSite().getSIT_Nom());
    }

}
