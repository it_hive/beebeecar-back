package com.ithive.beebeecarrest.resources.Vehicule;

import lombok.Data;

@Data
public class GetVehiculesDispoResources {
    private Long idSite;

    private String DateDebut;

    private String DateFin;
}
