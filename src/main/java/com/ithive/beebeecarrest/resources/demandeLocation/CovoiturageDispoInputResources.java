package com.ithive.beebeecarrest.resources.demandeLocation;

import lombok.Data;

@Data
public class CovoiturageDispoInputResources {
    private String DateReference;
}
