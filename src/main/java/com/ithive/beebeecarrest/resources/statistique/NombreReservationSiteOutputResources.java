package com.ithive.beebeecarrest.resources.statistique;

import com.ithive.beebeecarrest.model.sp.NombreReservationParSite;
import lombok.Data;

@Data
public class NombreReservationSiteOutputResources {

    private Long sitId;

    private String sitNom;

    private Integer demandesN;

    private Integer demandesN1;

    private Integer demandesN2;

    private Integer demandesN3;

    private Integer demandesN4;

    private Integer demandesN5;

    private Integer demandesN6;

    private Integer demandesN7;

    private Integer demandesN8;

    private Integer demandesN9;

    private Integer demandesN10;

    private Integer demandesN11;

    public NombreReservationSiteOutputResources(NombreReservationParSite nbReservation){
        setSitId(nbReservation.getSitId());
        setSitNom(nbReservation.getSitNom());
        setDemandesN(nbReservation.getDemandesN());
        setDemandesN1(nbReservation.getDemandesN1());
        setDemandesN2(nbReservation.getDemandesN2());
        setDemandesN3(nbReservation.getDemandesN3());
        setDemandesN4(nbReservation.getDemandesN4());
        setDemandesN5(nbReservation.getDemandesN5());
        setDemandesN6(nbReservation.getDemandesN6());
        setDemandesN7(nbReservation.getDemandesN7());
        setDemandesN8(nbReservation.getDemandesN8());
        setDemandesN9(nbReservation.getDemandesN9());
        setDemandesN10(nbReservation.getDemandesN10());
        setDemandesN11(nbReservation.getDemandesN11());
    }
}
