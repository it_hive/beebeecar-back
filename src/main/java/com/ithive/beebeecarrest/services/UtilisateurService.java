package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.EtatDemande;
import com.ithive.beebeecarrest.model.Organisme;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.repositories.DemandeLocationRepository;
import com.ithive.beebeecarrest.repositories.EtatDemandeRepository;
import com.ithive.beebeecarrest.repositories.OrganismeRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.resources.ConnexionRessources;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import com.ithive.beebeecarrest.securities.TokenRegister;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class UtilisateurService {

    private static Logger logger = LogManager.getLogger(UtilisateurService.class);

    private UtilisateurRepository utilisateurRepository;
    private OrganismeRepository organismeRepository;
    private EtatDemandeRepository etatDemandeRepository;
    private DemandeLocationRepository demandeLocationRepository;

    public UtilisateurService(UtilisateurRepository p_repository, OrganismeRepository p_organismeRepository,
                              EtatDemandeRepository p_etatDemandeRepository, DemandeLocationRepository p_demandeLocationRepository){
        this.utilisateurRepository = p_repository;
        this.demandeLocationRepository = p_demandeLocationRepository;
        this.etatDemandeRepository = p_etatDemandeRepository;
        this.organismeRepository = p_organismeRepository;
    }

    public List<UtilisateurResources> BoToVos(List<Utilisateur> listBO){
        List<UtilisateurResources> listVO = new LinkedList<UtilisateurResources>();

        for(Utilisateur bo : listBO){
            listVO.add(boToVoSimple(bo));
        }

        return listVO;
    }

    public UtilisateurResources boToVoSimple(Utilisateur bo){
        UtilisateurResources vo = new UtilisateurResources();

        vo.setIdUtilisateur(bo.getUtiId());
        vo.setHasPermis(Byte.valueOf("1").equals(bo.getUTI_HasPermis()));
        vo.setAdmin(Byte.valueOf("1").equals(bo.getIsAdmin()));
        vo.setArchived(Byte.valueOf("1").equals(bo.getUTI_IsArchived()));
        vo.setMail(bo.getmail());
        vo.setNom(bo.getUTI_Nom());
        vo.setPrenom(bo.getUTI_Prenom());
        vo.setTelephone(bo.getUTI_Telephone());

        vo.setIdOrganisme(bo.getOrgOrganisme().getOrgId());
        vo.setNomOrganisme(bo.getOrgOrganisme().getORG_Nom());
        vo.setPhoto(bo.getUTI_PhotoUrl());

        return vo;

    }

    public Object createBO(UtilisateurResources utilisateur, UtilisateurResources utilisateurConnect) throws NoSuchAlgorithmException {

        logger.debug("création d'un utilisateur");

        // on vérifie si l'utilisateur connecté est un admin
        if(!utilisateurConnect.isAdmin()){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CREATION_ADMIN);
        }

        // vérification des données
        // format téléphone
        if(!utilisateur.getTelephone().matches("\\d{10}")){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CREATION_TELEPHONE);
        }
        // format mail
        if(!utilisateur.getMail().toUpperCase().matches("^^\\w+(.\\w+)*@(\\w+.)+\\w{2,4}$")){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CREATION_MAIL);
        }

        logger.debug("récupération de l'organisme");
        Long idOrganisme = utilisateurConnect.getIdOrganisme();
        logger.debug("Id de l'organisme recherché : " + idOrganisme);
        Organisme organisme = organismeRepository.findById(idOrganisme).get();
        logger.debug("organisme trouvé :" + organisme.getORG_Nom());

        // recherche s'il n'existe pas un utilisateur pour cet organisme
        List<Utilisateur> listeUtilisateurBD = this.utilisateurRepository.findAllByMailAndAndOrgOrganisme(utilisateur.getMail(), organisme);
        if((listeUtilisateurBD != null) && listeUtilisateurBD.size() != 0){
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CREATION_EXISTE);
        }

        Utilisateur bo = new Utilisateur();
        bo.setOrgOrganisme(organisme);
        bo.setUTI_IsArchived(Byte.valueOf("0"));
        bo = VoToBO(utilisateur, bo);
        bo.setUTI_MotDePasse(getHash(utilisateur.getMotPasse()));

        this.utilisateurRepository.save(bo);
        logger.info("utilisateur créé :" + bo.getUTI_Nom() + " " + bo.getUTI_Prenom()
                + " (" + bo.getUtiId() + ")");

        return boToVoSimple(bo);

    }

    private String getHash(String input) throws NoSuchAlgorithmException {

        // Static getInstance method is called with hashing MD5
        MessageDigest md = MessageDigest.getInstance("MD5");

        // digest() method is called to calculate message digest
        //  of an input digest() return array of byte
        byte[] messageDigest = md.digest(input.getBytes());

        // Convert byte array into signum representation
        BigInteger no = new BigInteger(1, messageDigest);

        // Convert message digest into hex value
        String hashtext = no.toString(16);
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }

    public Object connexionToken(ConnexionRessources connexion) throws NoSuchAlgorithmException {
        // on récupère la liste des utilisateus avec le mail (0 ou 1 personne)
        List<Utilisateur> utilisateurs = this.utilisateurRepository.findAllBymail(connexion.getMailUtilisateur());
        if(utilisateurs.size() == 0){
            // aucun utilisateur n'a été trouvé
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_INEXISTANT);
        }

        // on récupère l'utilisateur trouvé
        Utilisateur utilisateur = utilisateurs.get(0);

        if(utilisateur.getUTI_MotDePasse().equals(connexion.getMotDePasse()) || utilisateur.getUTI_MotDePasse().equals(getHash(connexion.getMotDePasse()))) {
            // le mot de passe est le bon
            UtilisateurResources utilisateurResources = boToVoSimple(utilisateur);
            //on vérifie que l'utilisateur n'est pas archivé, sinon il ne peut pas se connecter
            if (!utilisateurResources.isArchived()) {
                // on créer un nouveu token à partir de l'utilisateurRessource
                String token = TokenRegister.getInstance().addToken(utilisateurResources);

                // on créer l'objet TokenRessource à renvoyer
                TokenRessources tokenObject = new TokenRessources();
                tokenObject.setToken(token);
                return tokenObject;
            } else {
                // l'utilisateur est archivé, on envoie donc un message d'erreur
                return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CONNEXION_ARCHIVE);
            }
        } else {

            // l'utilisateur/ l'émail a bien été trouvé mais le mot de passe n'est pas le bon
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_CONNEXION);
        }
    }

    public List<UtilisateurResources> getUtilisateurByOrganisme(Long organismeId){

        List<Utilisateur> listBO = this.utilisateurRepository.findAllByOrgOrganisme_orgId(organismeId);
        logger.info("nombre d'utilsateur trouvé :" + listBO.size());

        return  BoToVos(listBO);
    }

    public Object getUtilisateurByID(Long utilisateurID, UtilisateurResources utiConnect){
        Optional<Utilisateur> opt = this.utilisateurRepository.findById(utilisateurID);

        if(opt.isPresent()){

            //vérifier que l'utilisateur demandé et l'utilisateur connecté font parti du même organisme
            if(opt.get().getOrgOrganisme().getOrgId() != utiConnect.getIdOrganisme()){
                return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_ACCES);
            }

            return boToVoSimple(opt.get());
        } else {
            logger.error("Pas d'utilisateur trouvé pour l'id :" + utilisateurID);
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_INEXISTANT);
        }
    }

    public Object replaceUtilisateur(UtilisateurResources utilisateurRessource, UtilisateurResources utiConnect) throws NoSuchAlgorithmException {

        Optional<Utilisateur> optional = this.utilisateurRepository.findById(utilisateurRessource.getIdUtilisateur());

        if(optional.isPresent()){
            Utilisateur bo = optional.get();

            // vérification, l'utilisateur a modifié est celui connecté ou l'utilisateur connecté est un admin de l'organisme
            if(!(bo.getUtiId() == utiConnect.getIdUtilisateur())
                    && !(utiConnect.isAdmin() && (utiConnect.getIdOrganisme() == bo.getOrgOrganisme().getOrgId()))){
                return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_MODIFICATION);
            }

            bo = VoToBO(utilisateurRessource, bo);

            this.utilisateurRepository.save(bo);
        } else {
            logger.error("L'utilisateur (" + utilisateurRessource.getIdUtilisateur() + " - " + utilisateurRessource.getNom()
                    + " " + utilisateurRessource.getPrenom() + ") n'existe pas dans la base de données");
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_INEXISTANT);
        }

        return utilisateurRessource;

    }

    public Utilisateur VoToBO (UtilisateurResources utilisateurRessource, Utilisateur bo){
        bo.setUTI_Nom(utilisateurRessource.getNom());
        bo.setUTI_Prenom(utilisateurRessource.getPrenom());
        bo.setuti_mail(utilisateurRessource.getMail());
        bo.setUTI_HasPermis(utilisateurRessource.isHasPermis()? Byte.valueOf("1") : Byte.valueOf("0"));
        bo.setIsAdmin(utilisateurRessource.isAdmin() ? Byte.valueOf("1") : Byte.valueOf("0"));
        bo.setUTI_Telephone(utilisateurRessource.getTelephone());
        bo.setUTI_PhotoUrl(utilisateurRessource.getPhoto());

        return bo;
    }

    public Object archivageUtilisateur(Long utilisateurID, UtilisateurResources utiConnect){

        EtatDemande etatDemande = this.etatDemandeRepository.findFirstByLibelleEquals("Annulé car archivage");
        logger.info("etatdemande trouvé :" + etatDemande.getEdmId() + " - " + etatDemande.getEDM_Libelle() );

        Optional<Utilisateur> optional = this.utilisateurRepository.findById(utilisateurID);
        if(optional.isPresent()){
            Utilisateur bo = optional.get();

            // vérification des droits pour l'archivage, l'utilisateur connecté doit être un admin de l'organisme
            if(!(utiConnect.isAdmin() && (utiConnect.getIdOrganisme() == bo.getOrgOrganisme().getOrgId()))){
                return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_MODIFICATION);
            }

            // recherche des clés
            if(bo.getCleClevehicules().size() != 0){
                // message d'erreur, l'utilisateur a encore une clé
                logger.error("L'utilisateur " + (bo.getUtiId()));
                return MessageErreur.createMessageErreur(MessageErreur.CODE_DEMANDE_ARCHIVAGE_CLE);
            }

            // recherche des demandes
            // si au moins une demande validé est en cours, alors l'archivage est interdit
            List<DemandeLocation> listeDemandeAnnule = new LinkedList<DemandeLocation>();
            Date dateJour = new Date();

            for(DemandeLocation demande : bo.getDemDemandelocations()){
                if(demande.getEdmEtatdemande().getEDM_Libelle().equals("En cours de validation")){
                    listeDemandeAnnule.add(demande);

                } else if(demande.getEdmEtatdemande().getEDM_Libelle().equals("Validé")){
                    // voir les dates
                    // si date de début après date du jour (dans le futur)
                    //  --> annulation de la demande
                    if(demande.getDemDatedebut().after(dateJour)){
                        listeDemandeAnnule.add(demande);
                    }
                    // sinon si date de fin après date du jour (demande en cours)
                    //  --> message d'erreur
                    else if(demande.getDemDatefin().after(dateJour)){
                        return MessageErreur.createMessageErreur(MessageErreur.CODE_DEMANDE_ARCHIVAGE_DEMANDE);
                    }
                }
            }

            for(DemandeLocation demande : listeDemandeAnnule){
                logger.info("La demande (" + demande.getDemId() + ") est annulé à cause de l'archivage ");
                demande.setEdmEtatdemande(etatDemande);
                this.demandeLocationRepository.save(demande);
            }

            logger.info("L'utilisateur (" + bo.getUtiId() + " - " + bo.getUTI_Nom() + " " + bo.getUTI_Prenom()
                    + ") est archivé");
            bo.setUTI_IsArchived(new Byte("1"));
            this.utilisateurRepository.save(bo);

            //return boToVoSimple(bo);

        } else {
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_INEXISTANT);
        }

        return MessageErreur.createMessageErreur(MessageErreur.CODE_OK);
    }

    public List<UtilisateurResources> getUtilisateurByOrganisme(UtilisateurResources utilisateur){
        return BoToVos(this.utilisateurRepository.findAllByOrgOrganisme_orgId(utilisateur.getIdOrganisme()));
    }

    /// Fonction permettant de récupérer l'utilisateur lié au token
    public static Utilisateur GetUtilisateurByToken(TokenRessources token, UtilisateurRepository repository){

        // Récupération de l'utilisateur connecté à travers le token
        UtilisateurResources utiConnecte = TokenRegister.getInstance().verification(token.getToken());
        if(utiConnecte == null){
            return null;
        }
        Utilisateur utilisateur = repository.findFirstByMail(utiConnecte.getMail());

        return utilisateur;
    }
    
    // Récupère l'organisme d'un utilisateur
    public Organisme getOrganismeFromUtilisateur(UtilisateurResources utilisateur) {
    	return this.organismeRepository.findById(utilisateur.getIdOrganisme()).get();
    }
    
    // Récupère la liste des admins
    public List<Utilisateur> getAdmin(Organisme organisme) {

        boolean vIn = true;
		byte isAdmin = (byte)(vIn?1:0);
    	
    	return this.utilisateurRepository.findAllByIsAdminAndOrgOrganisme(isAdmin, organisme);
    }
    
 // Récupère la liste des admins
    public Utilisateur getUti(Long id) {
    	return this.utilisateurRepository.findById(id).get();
    }

    public Object replacePassword(ConnexionRessources connexion, UtilisateurResources utiConnect, Long idUtilisateur) throws NoSuchAlgorithmException {

        Optional<Utilisateur> optional = this.utilisateurRepository.findById(idUtilisateur);

        if(optional.isPresent()){
            Utilisateur bo = optional.get();

            // vérification, l'utilisateur a modifié est celui connecté ou l'utilisateur connecté est un admin de l'organisme
            if(!(bo.getUtiId() == utiConnect.getIdUtilisateur())
                    && !(utiConnect.isAdmin() && (utiConnect.getIdOrganisme() == bo.getOrgOrganisme().getOrgId()))){
                return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_MODIFICATION);
            }


            // remplacement du nouveau mot de passe si celui-ci n'est pas vide dans la ressource
            if(!connexion.getMotDePasse().equals("")) {
                bo.setUTI_MotDePasse(getHash(connexion.getMotDePasse()));
            }


            this.utilisateurRepository.save(bo);
        } else {
            logger.error("L'utilisateur est inexistant");
            return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_INEXISTANT);
        }

        return MessageErreur.createMessageErreur(MessageErreur.CODE_OK);

    }
}