package com.ithive.beebeecarrest.model.sp;

import lombok.Data;

@Data
public class TauxOccupationVehicule {

    public Long vehId;

    public String vehImmatriculation;

    public Float tauxoccupationn;
    public Float tauxoccupationn1;
    public Float tauxoccupationn2;
    public Float tauxoccupationn3;
    public Float tauxoccupationn4;
    public Float tauxoccupationn5;
    public Float tauxoccupationn6;
    public Float tauxoccupationn7;
    public Float tauxoccupationn8;
    public Float tauxoccupationn9;
    public Float tauxoccupationn10;
    public Float tauxoccupationn11;

    public TauxOccupationVehicule(Long vehId, String vehImmatriculation, Float tauxoccupationn, Float tauxoccupationn1, Float tauxoccupationn2, Float tauxoccupationn3, Float tauxoccupationn4, Float tauxoccupationn5, Float tauxoccupationn6, Float tauxoccupationn7, Float tauxoccupationn8, Float tauxoccupationn9, Float tauxoccupationn10, Float tauxoccupationn11) {
        this.vehId = vehId;
        this.vehImmatriculation = vehImmatriculation;
        this.tauxoccupationn = tauxoccupationn;
        this.tauxoccupationn1 = tauxoccupationn1;
        this.tauxoccupationn2 = tauxoccupationn2;
        this.tauxoccupationn3 = tauxoccupationn3;
        this.tauxoccupationn4 = tauxoccupationn4;
        this.tauxoccupationn5 = tauxoccupationn5;
        this.tauxoccupationn6 = tauxoccupationn6;
        this.tauxoccupationn7 = tauxoccupationn7;
        this.tauxoccupationn8 = tauxoccupationn8;
        this.tauxoccupationn9 = tauxoccupationn9;
        this.tauxoccupationn10 = tauxoccupationn10;
        this.tauxoccupationn11 = tauxoccupationn11;
    }
}
