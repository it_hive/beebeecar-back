package com.ithive.beebeecarrest.exceptions;

import com.ithive.beebeecarrest.resources.MessageErreur;

public class VehiculeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private MessageErreur messageErreur;
	
	public VehiculeNotFoundException(Long id) {
		super("Impossible de trouver la voiture #" + id);
	}
	
	
	
//	public VehiculeNotFoundException() {
//		this.messageErreur.setCode(messageErreur.CODE_SITE_VEHICULE_EXISTANT);
//	}
	
}
