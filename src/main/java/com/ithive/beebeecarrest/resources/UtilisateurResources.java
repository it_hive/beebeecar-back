package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class UtilisateurResources {

    private long idUtilisateur;
    private boolean hasPermis;
    private boolean admin;
    private String mail;
    private String nom;
    private String prenom;
    private String telephone;
    private long idOrganisme;
    private String nomOrganisme;
    private boolean archived;
    private String photo;
    private String motPasse;

    /*
    * clé voiture, commentaire des demandes, demandes de location
    * */
}
