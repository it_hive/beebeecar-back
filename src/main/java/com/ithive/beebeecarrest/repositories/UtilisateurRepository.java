package com.ithive.beebeecarrest.repositories;

import java.util.List;
import java.util.Set;

import com.ithive.beebeecarrest.model.Organisme;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ithive.beebeecarrest.model.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
//    @Query(" select u from Utilisateur u " +
//            " where u.username = ?1")
//    public Optional<Utilisateur> findUserWithName(String username);
	
	List<Utilisateur> findAllByOrgOrganisme_orgId(Long orgId);
	List<Utilisateur> findAllBymail(String mail);
	List<Utilisateur> findAllByMailAndAndOrgOrganisme(String mail, Organisme organisme);
	Utilisateur findFirstByMail(String mail);
	List<Utilisateur> findAllByIsAdminAndOrgOrganisme(byte isAdmin, Organisme organisme);
}