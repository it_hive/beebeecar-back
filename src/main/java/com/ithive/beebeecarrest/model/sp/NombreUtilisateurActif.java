package com.ithive.beebeecarrest.model.sp;

import lombok.Data;

@Data
public class NombreUtilisateurActif {

    private Integer utilisateurActifsN;

    private Integer utilisateurActifsN1;

    private Integer utilisateurActifsN2;

    private Integer utilisateurActifsN3;

    private Integer utilisateurActifsN4;

    private Integer utilisateurActifsN5;

    private Integer utilisateurActifsN6;

    private Integer utilisateurActifsN7;

    private Integer utilisateurActifsN8;

    private Integer utilisateurActifsN9;

    private Integer utilisateurActifsN10;

    private Integer utilisateurActifsN11;

    public NombreUtilisateurActif(Integer utilisateurActifsN, Integer utilisateurActifsN1, Integer utilisateurActifsN2, Integer utilisateurActifsN3, Integer utilisateurActifsN4, Integer utilisateurActifsN5, Integer utilisateurActifsN6, Integer utilisateurActifsN7, Integer utilisateurActifsN8, Integer utilisateurActifsN9, Integer utilisateurActifsN10, Integer utilisateurActifsN11) {
        this.utilisateurActifsN = utilisateurActifsN;
        this.utilisateurActifsN1 = utilisateurActifsN1;
        this.utilisateurActifsN2 = utilisateurActifsN2;
        this.utilisateurActifsN3 = utilisateurActifsN3;
        this.utilisateurActifsN4 = utilisateurActifsN4;
        this.utilisateurActifsN5 = utilisateurActifsN5;
        this.utilisateurActifsN6 = utilisateurActifsN6;
        this.utilisateurActifsN7 = utilisateurActifsN7;
        this.utilisateurActifsN8 = utilisateurActifsN8;
        this.utilisateurActifsN9 = utilisateurActifsN9;
        this.utilisateurActifsN10 = utilisateurActifsN10;
        this.utilisateurActifsN11 = utilisateurActifsN11;
    }
}
