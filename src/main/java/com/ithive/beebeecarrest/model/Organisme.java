package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Set;


/**
 * The persistent class for the org_organisme database table.
 * 
 */
@Entity
@Table(name="org_organisme")
@NamedQuery(name="Organisme.findAll", query="SELECT o FROM Organisme o")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "orgId")
public class Organisme implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ORG_ID", unique=true, nullable=false)
	private Long orgId;

	@Column(length=100)
	private String ORG_Nom;

	//bi-directional many-to-one association to SitSite
	@OneToMany(mappedBy="orgOrganisme", fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Site> sitSites;

	//bi-directional many-to-one association to UtiUtilisateur
	@OneToMany(mappedBy="orgOrganisme", fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Utilisateur> utiUtilisateurs;

	public Organisme() {
	}

	public Long getOrgId() {
		return this.orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getORG_Nom() {
		return this.ORG_Nom;
	}

	public void setORG_Nom(String ORG_Nom) {
		this.ORG_Nom = ORG_Nom;
	}

	public Set<Site> getSitSites() {
		return this.sitSites;
	}

	public void setSitSites(Set<Site> sitSites) {
		this.sitSites = sitSites;
	}

	public Site addSitSite(Site sitSite) {
		getSitSites().add(sitSite);
		sitSite.setOrgOrganisme(this);

		return sitSite;
	}

	public Site removeSitSite(Site sitSite) {
		getSitSites().remove(sitSite);
		sitSite.setOrgOrganisme(null);

		return sitSite;
	}

	public Set<Utilisateur> getUtiUtilisateurs() {
		return this.utiUtilisateurs;
	}

	public void setUtiUtilisateurs(Set<Utilisateur> utiUtilisateurs) {
		this.utiUtilisateurs = utiUtilisateurs;
	}

	public Utilisateur addUtiUtilisateur(Utilisateur utiUtilisateur) {
		getUtiUtilisateurs().add(utiUtilisateur);
		utiUtilisateur.setOrgOrganisme(this);

		return utiUtilisateur;
	}

	public Utilisateur removeUtiUtilisateur(Utilisateur utiUtilisateur) {
		getUtiUtilisateurs().remove(utiUtilisateur);
		utiUtilisateur.setOrgOrganisme(null);

		return utiUtilisateur;
	}

}