package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.model.Organisme;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.repositories.*;
import com.ithive.beebeecarrest.resources.DemandeLocationResources;
import com.ithive.beebeecarrest.resources.EmailResources;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import com.ithive.beebeecarrest.resources.demandeLocation.CovoiturageDispoInputResources;
import com.ithive.beebeecarrest.resources.demandeLocation.NouvelleDemandeLocationResources;
import com.ithive.beebeecarrest.resources.utilisateur.ValidationDemandeInputResources;
import com.ithive.beebeecarrest.services.DemandeLocationService;
import com.ithive.beebeecarrest.services.EmailService;
import com.ithive.beebeecarrest.services.UtilisateurService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class DemandeLocationController {

	private final DemandeLocationRepository demRepository;
	private final SiteRepository siteRepo;
	private final VehiculeRepository vehRepo;
	private final UtilisateurRepository utiRepo;
	private final ModeleRepository modeleRepository;
	private final EtatDemandeRepository edmRepository;
	private final DetailsDemandeRepository detailRepository;
	private final EtapeRepository etapeRepository;

	private UtilisateurService utilisateurService;

	private static Logger logger = LogManager.getLogger(DemandeLocationController.class);

	public DemandeLocationController(DemandeLocationRepository demRepository, SiteRepository siteRepo,
			VehiculeRepository vehRepo, UtilisateurRepository utiRepo, ModeleRepository modeleRepository,
			EtatDemandeRepository edmRepository, DetailsDemandeRepository detailRepository,
			EtapeRepository etapeRepository, OrganismeRepository orgRepo, DemandeLocationRepository demLocRepo) {
		this.demRepository = demRepository;
		this.siteRepo = siteRepo;
		this.vehRepo = vehRepo;
		this.utiRepo = utiRepo;
		this.modeleRepository = modeleRepository;
		this.edmRepository = edmRepository;
		this.detailRepository = detailRepository;
		this.etapeRepository = etapeRepository;
		this.utilisateurService = new UtilisateurService(utiRepo, orgRepo, edmRepository, demLocRepo);
	}

	// Aggregate root

	// Route permettant de créer une nouvelle demande
	@PostMapping("/demandeLocation/new/{userToken}")
	public Object newDemande(@RequestBody NouvelleDemandeLocationResources resource, @PathVariable String userToken) {
		logger.info(resource.toString());

		// On reconstruit le token avec la variable donnée en commentaire
		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.CreateDemandeLocation(resource, token, demRepository, edmRepository, vehRepo,
				utiRepo, detailRepository, siteRepo, etapeRepository);
	}

	// Contrôleur retournant la liste des demande pour un utilisateur
	// Si il est admin, il récupère toutes les demande de son organisme
	// Si il ne l'est pas, il ne récupère que ses demandes
	// Si l'état est renseigné, on filtre les demandes par état
	// Sinon, on ne filtre pas par état
	@GetMapping("/demandeLocation/{userToken}/{etat}/{asc}")
	public List<DemandeLocationResources> getDemandesForUserByEtat(@PathVariable String userToken,
			@PathVariable String etat, @PathVariable boolean asc) {

		// On reconstruit le token avec la variable donnée en commentaire
		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.GetDemandesForUserByEtatDemande(token, etat, asc, edmRepository, demRepository,
				utiRepo);
	}

	/*
	 * @GetMapping("/demandeLocation/{userToken}") public
	 * List<DemandeLocationResources> getDemandesForUserByEtat(@PathVariable String
	 * userToken){
	 * 
	 * return getDemandesForUserByEtat(userToken, null); }
	 */

	// Route permettant de valider une demande de location
	@PutMapping("/demandeLocation/validate/{userToken}")
	public Object validateDemandeLocation(@RequestBody ValidationDemandeInputResources body,
			@PathVariable String userToken) {

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.UpdateEtatDemandeLocation(body, "Valide", token, edmRepository, demRepository,
				utiRepo, vehRepo);
	}

	// Route permettant de valider une demande de location
	@PutMapping("/demandeLocation/refuse/{userToken}")
	public Object refuseDemandeLocation(@RequestBody ValidationDemandeInputResources body,
			@PathVariable String userToken) {

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.UpdateEtatDemandeLocation(body, "Refuse", token, edmRepository, demRepository,
				utiRepo, vehRepo);
	}

	// Route permettant de valider une demande de location
	@PutMapping("/demandeLocation/cancel/{userToken}")
	public Object cancelDemandeLocation(@RequestBody ValidationDemandeInputResources body,
			@PathVariable String userToken) {

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.UpdateEtatDemandeLocation(body, "Annule", token, edmRepository, demRepository,
				utiRepo, vehRepo);
	}

	// Route permettant de récupérer une demandeLocationResources
	@GetMapping("/demandeLocation/get/{idDemande}")
	public Object getDemandeLocation(@PathVariable Long idDemande) {

		return DemandeLocationService.GetDemandeLocationById(idDemande, demRepository);
	}

	// Route permettant de mettre à jour une demande de location
	@PutMapping("/demandeLocation/update/{userToken}")
	public Object updateDemandeLocation(@RequestBody NouvelleDemandeLocationResources resource,
			@PathVariable String userToken) {

		// On reconstruit le token avec la variable donnée en commentaire
		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.UpdateDemandeLocation(resource, token, demRepository, edmRepository, vehRepo,
				utiRepo, detailRepository, siteRepo, etapeRepository);
	}

	// Fonction retournant la liste des covoiturages pour une date donnée
	@PostMapping("/demandeLocation/covoiturages/{userToken}")
	public Object getCovoiturageDisponibles(@RequestBody CovoiturageDispoInputResources resources,
			@PathVariable String userToken) {

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);
		return DemandeLocationService.GetCovoiturageDispos(resources.getDateReference(), token, demRepository, utiRepo);
	}

	// Route permettant de renvoyer les demandes validées commençant aujourd'hui
	@GetMapping("/demandeLocatio/getTodayValidated/{userToken}")
	public Object getDemandesValideesDuJour(@PathVariable String userToken) {

		TokenRessources token = new TokenRessources();
		token.setToken(userToken);

		return DemandeLocationService.getDemandesValideesDuJour(token, demRepository, utiRepo);
	}

	/**
	 * Permet d'envoyer le mail d'identifiant
	 * 
	 * @param utilisateurId id de l'utilisateur
	 * @param  token de l'utilisateur connecté
	 * @return
	 */
	@GetMapping("demandeLocation/email/{utilisateurId}/{demandeLocationId}")
	public Object envoieMail(@PathVariable Long utilisateurId, @PathVariable Long demandeLocationId) {

		Utilisateur uti = this.utilisateurService.getUti(utilisateurId);
		UtilisateurResources utiConnecte = this.utilisateurService.boToVoSimple(uti);

		NouvelleDemandeLocationResources demLoc = (NouvelleDemandeLocationResources) DemandeLocationService.GetDemandeLocationById(demandeLocationId, demRepository);

		Organisme org = this.utilisateurService.getOrganismeFromUtilisateur(utiConnecte);
		List<Utilisateur> utis = this.utilisateurService.getAdmin(org);

		EmailResources email = EmailService.createBody(utiConnecte, demLoc, utis, false);
		EmailService.send(email);

		return "OK";
	}

	/**
	 * Permet d'envoyer le mail d'identifiant
	 * 
	 * @param utilisateurId id de l'utilisateur
	 * @param token de l'utilisateur connecté
	 * @return
	 */
	@GetMapping("demandeLocationStatut/email/{utilisateurId}/{demandeLocationId}")
	public Object envoieMailStatut(@PathVariable Long utilisateurId, @PathVariable Long demandeLocationId) {

		Utilisateur uti = this.utilisateurService.getUti(utilisateurId);
		UtilisateurResources utiConnecte = this.utilisateurService.boToVoSimple(uti);

		NouvelleDemandeLocationResources demLoc = (NouvelleDemandeLocationResources) DemandeLocationService.GetDemandeLocationById(demandeLocationId, demRepository);
//		DemandeLocationResources demLoc = null;
//		DemandeLocationResources demLoc = DemandeLocationService.GetDemandeLocationResourcesById(demandeLocationId,
//				demRepository);
		
//		NouvelleDemandeLocationResources demLoc = (NouvelleDemandeLocationResources) DemandeLocationService.GetDemandeLocationById(demandeLocationId,demRepository);
		
//		System.out.println(demLoc);
		
		System.out.println("mailStatut");
		
		Organisme org = this.utilisateurService.getOrganismeFromUtilisateur(utiConnecte);
		List<Utilisateur> utis = this.utilisateurService.getAdmin(org);

		EmailResources email = EmailService.createBody(utiConnecte, demLoc, utis, true);
		EmailService.send(email);

		return "OK";
	}

}
