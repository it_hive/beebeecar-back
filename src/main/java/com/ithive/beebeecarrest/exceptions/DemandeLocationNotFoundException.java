package com.ithive.beebeecarrest.exceptions;

public class DemandeLocationNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DemandeLocationNotFoundException(Long id) {
		super("Impossible de trouver la demande de location #" + id);
	}
}
