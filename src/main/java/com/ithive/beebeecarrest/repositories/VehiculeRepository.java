package com.ithive.beebeecarrest.repositories;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.ithive.beebeecarrest.model.Vehicule;

public interface VehiculeRepository extends JpaRepository<Vehicule, Long> {
//public interface VehiculeRepository extends PagingAndSortingRepository<Vehicule, Long> {

	List<Vehicule> findAllBySitSite_sitId(Long sitId);
//	List<Vehicule> findAllBySitSite_sitId(Long sitId, Pageable page);
	
	List<Vehicule> findAllBySitSite_sitIdIsIn(List<Long> allSitesIds);
	
}
