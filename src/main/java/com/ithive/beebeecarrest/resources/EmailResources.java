package com.ithive.beebeecarrest.resources;

import java.util.List;

import lombok.Data;

@Data
public class EmailResources {

	private List<String> mailTo;
	
	private List<String> mailToCC;

	private String mailFrom;

	private String mailBody;

	private String mailSubject;
	
	public EmailResources() {
		super();
	}
	
}
