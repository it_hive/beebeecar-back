package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.model.*;
import com.ithive.beebeecarrest.repositories.*;
import com.ithive.beebeecarrest.resources.DemandeLocationResources;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.demandeLocation.CovoiturageDisposResources;
import com.ithive.beebeecarrest.resources.demandeLocation.EtapeResources;
import com.ithive.beebeecarrest.resources.demandeLocation.NouvelleDemandeLocationResources;
import com.ithive.beebeecarrest.resources.utilisateur.ValidationDemandeInputResources;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class DemandeLocationService {

    private static Logger logger = LogManager.getLogger(DemandeLocationService.class);

    public static List<DemandeLocationResources> BotoVos(List<DemandeLocation> listBO){
        List<DemandeLocationResources> listVO = new LinkedList<DemandeLocationResources>();
        for (DemandeLocation bo : listBO) {
            listVO.add(BOToVO(bo));
        }
        return listVO;
    }

    public static DemandeLocationResources BOToVO (DemandeLocation bo){
    	DemandeLocationResources vo = new DemandeLocationResources();

    	vo.setDemId(bo.getDemId());
    	vo.setAllowCovoiturage(bo.getDEM_AllowCovoiturage());
    	vo.setDateDebut(DateUtilities.convertUtilDateToString(bo.getDemDatedebut()));
    	vo.setDateDemande(DateUtilities.convertUtilDateToString(bo.getDemDateDemande()));
    	vo.setDateFin(DateUtilities.convertUtilDateToString(bo.getDemDatefin()));
		vo.setEtatDemande(bo.getEdmEtatdemande().getEDM_Libelle());
    	vo.setNomUtilisateur(bo.getUtiUtilisateur().getUTI_Nom());
    	vo.setPrenomUtilisateur(bo.getUtiUtilisateur().getUTI_Prenom());
    	vo.setIdUtilisateur(bo.getUtiUtilisateur().getUtiId());

    	vo.setVehicule(VehiculeService.BOToVO(bo.getVehVehicule()));

        return vo;
    }

    public static DemandeLocation VOToBO(
    		DemandeLocationResources vo, 
			SiteRepository siteRepo, 
			VehiculeRepository vehRepo, 
			UtilisateurRepository utiRepo,
			ModeleRepository modeleRepository) {
    	
    	DemandeLocation bo = new DemandeLocation();
		
    	try {
			bo.setDemId(vo.getDemId());
			bo.setDEM_AllowCovoiturage(vo.getAllowCovoiturage());
			bo.setDemDatedebut(DateUtilities.convertStringToUtilDate(vo.getDateDebut()));
			bo.setDemDateDemande(DateUtilities.convertStringToUtilDate(vo.getDateDemande()));
			bo.setUtiUtilisateur(utiRepo.getOne(vo.getIdUtilisateur()));
			bo.setDemDatefin(DateUtilities.convertStringToUtilDate(vo.getDateFin()));
			bo.setVehVehicule(VehiculeService.VOToBO(vo.getVehicule(), siteRepo, modeleRepository));
	//		
	//		bo.setPhvPhotovehicules(phvPhotovehicules);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bo;
	}
    
    
    public static List<DemandeLocationResources> getAll(DemandeLocationRepository repository){

    	List<DemandeLocation> demandes = repository.findAll();
    	return BotoVos(demandes);

	}

	// Fonction permettant d'ajouter une demande de location.
	// Créé les détails de demandes liées à la demande.
	// TODO : Virez moi ces repository de la signature!!!
	public static Object CreateDemandeLocation(
			NouvelleDemandeLocationResources resources,
			TokenRessources userToken,
			DemandeLocationRepository demandeRepo,
			EtatDemandeRepository etatdemRepo,
			VehiculeRepository vehiculeRepository,
			UtilisateurRepository utilisateurRepository,
			DetailsDemandeRepository detailRepo,
			SiteRepository siteRepo,
			EtapeRepository etapeRepository) {
		DemandeLocation demande = new DemandeLocation();
		try {

			// Récupération de l'utilisateur connecté à travers le token

			Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(userToken, utilisateurRepository);

			// On set le site de la demande
			Optional<Site> site = siteRepo.findById(resources.getIdSite());
			if(site == null){
				throw new Exception("Le site n'a pas pu être récupéré en base de données");
			}

			//TODO : après réflexion, ça aurait été mieux de passer par une énum que par une table de paramétrage
			EtatDemande etatDemande = etatdemRepo.findFirstByLibelleEquals("EnCoursDeValidation");
			// Vérification que l'entité etatDemande a été récupéré
			if(etatDemande == null)
				throw new Exception("L'entité EtatDemande n'as pas été récupéré");


			//Convertion des strings date départ et arrivée en Date
			Date start = DateUtilities.convertStringToUtilDate(resources.getDateDepart());
			Date end = DateUtilities.convertStringToUtilDate(resources.getDateArrivee());

			// On met la date de la demande à la date du jour
			demande.setDemDateDemande(java.util.Calendar.getInstance().getTime());

			demande.setDemDatedebut(start);
			demande.setDemDatefin(end);
			demande.setEdmEtatdemande(etatDemande);
			demande.setSitSite(site.get());
			demande.setUtiUtilisateur(utilisateur);
			demande.setDEM_AllowCovoiturage(resources.getAllowCovoiturage());
			demande.setCommentaireLibre(resources.getCommentaireLibre());
			demande.setCommentaireCovoiturage(resources.getCommentaireCovoiturage());



			// Ajout du véhicule si renseigné
			if(resources.getIdVehicule() != 0){
				Vehicule vehicule = vehiculeRepository.getOne(resources.getIdVehicule());
				demande.setVehVehicule(vehicule);
			}

			// On ajoute la demande en base
			demandeRepo.save(demande);

			// On ajoute les étapes du trajet en base
			if(resources.getEtapes() != null){
				for(EtapeResources etapeRes : resources.getEtapes()){
					Etape etape = new Etape();

					etape.setAdresse(etapeRes.getAdresse());
					etape.setDemDemande(demande);
					etape.setOrdre(etapeRes.getOrdre());

					etapeRepository.save(etape);
				}
			}

			// On créé les details liés à la demande ajoutée
			Date current = start;

			// Pour chaque jour entre les deux dates :
			while (current.before(end) || current.equals(end)){
				DetailDemande detail = new DetailDemande();
				detail.setDTD_Date(current);
				detail.setDemDemandelocation(demande);

				// On ajoute en base un nouveau détail
				detailRepo.save(detail);

				// On augmente la date current de 1 jour
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(current);
				calendar.add(Calendar.DATE, 1);
				current = calendar.getTime();
			}

			// On retourne la ressource
			return new DemandeLocationResources(demande, false);

		}catch (Exception e){
			logger.error(e);
			return  MessageErreur.creaMessageErreur(e.getMessage());
		}
	}

	// Fonction permettant de récupérer les demandes d'un utilsiateur, par son état
	// Si l'état n'est pas renseigné, on récupère tout;
	public static List<DemandeLocationResources> GetDemandesForUserByEtatDemande(
			TokenRessources userToken,
			String etat,
			boolean asc,
			EtatDemandeRepository etatDemandeRepository,
			DemandeLocationRepository demandeLocationRepository,
			UtilisateurRepository utilisateurRepository){
		try {

			// Récupération de l'utilisateur connecté à travers le token
			Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(userToken, utilisateurRepository);
			boolean isAdmin = utilisateur.getIsAdmin() == 1;

			//Récupération de l'état de demande "En cours de validation" ascendant ou descendant
			List<DemandeLocation> demandes = asc ?
					demandeLocationRepository.findByOrderByDemDateDemandeAsc()
					: demandeLocationRepository.findByOrderByDemDateDemandeDesc();

			// Si l'utilisateur n'est pas admin :
			if(!isAdmin){
				// Il ne récupère que ces demandes
				demandes = demandes.stream()
						.filter((DemandeLocation) -> DemandeLocation.getUtiUtilisateur().getUtiId() == utilisateur.getUtiId())
						.collect(Collectors.toList());
			}else{
				// Si il est admin :
				// Il récupère toutes les demandes de son organisme
				demandes = demandes.stream()
						.filter((DemandeLocation) ->
								DemandeLocation.getUtiUtilisateur().getOrgOrganisme().getOrgId()
										== utilisateur.getOrgOrganisme().getOrgId())
						.collect(Collectors.toList());
			}


			// Si un état est renseigné, on ne récupère que ceux liées à l'état
			EtatDemande etatDemande = etatDemandeRepository.findFirstByLibelleEquals(etat);
			demandes = demandes.stream()
					.filter((DemandeLocation) -> DemandeLocation.getEdmEtatdemande().getEdmId() == etatDemande.getEdmId())
					.collect(Collectors.toList());

			List<DemandeLocationResources> resources = new ArrayList<>();

			for(DemandeLocation demande : demandes){
				DemandeLocationResources dem = new DemandeLocationResources(demande, isAdmin);
				resources.add(dem);
			}

			return resources;
		}catch (Exception e){
			logger.error(e);
			throw e;
		}
	}

	// Fonction permettant de modifier l'état d'un demande de location
	public static Object UpdateEtatDemandeLocation(
			ValidationDemandeInputResources body,
			String etatDemande,
			TokenRessources token,
			EtatDemandeRepository etatRepository,
			DemandeLocationRepository demandeRepository,
			UtilisateurRepository utilisateurRepository,
			VehiculeRepository vehiculeRepository
	){
    	try{
    		// On vérifie que l'utilisateur est bien connecté
    		Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
    		if(utilisateur == null)
    			return MessageErreur.createMessageErreur(MessageErreur.CODE_TOKEN_NON_CONNECTE);

			// On récupère la demande
			Optional<DemandeLocation> demande = demandeRepository.findById(body.getIdDemande());
			if(demande == null)
				throw new Exception("La demande n'a pas été trouvée en base de données");

			EtatDemande etat = etatRepository.findFirstByLibelleEquals(etatDemande);
			if(etat == null)
				throw new Exception("L'état n'a pas été trouvée en base de données");

			switch (etatDemande){
				case "Valide":
					//Récupération du véhicule
					Optional<Vehicule> vehicule = vehiculeRepository.findById(body.getIdVehicule());
					if(!vehicule.isPresent()){
						throw new Exception("Impossible de trouver le véhicule");
					}

					// On vérifie les droits d'adminsitration
					if(utilisateur.getIsAdmin() != 1)
						return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_MODIFICATION);
					demande.get().setUtiValideur(utilisateur);
					demande.get().setVehVehicule(vehicule.get());
					break;
				case "Refuse":

					// On vérifie les droits d'adminsitration
					if(utilisateur.getIsAdmin() != 1)
						return MessageErreur.createMessageErreur(MessageErreur.CODE_UTILISATEUR_DROIT_MODIFICATION);
					demande.get().setUtiValideur(utilisateur);
					break;
			}

			// On attribue le nouvel état à la demande
			demande.get().setEdmEtatdemande(etat);

			// Sauvegarde des modifications
			demandeRepository.save(demande.get());

			return MessageErreur.createMessageErreur(MessageErreur.CODE_OK);
		}catch (Exception e){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
		}
	}

	// Fonction permettant de récupérer un objet NouvelleDemandeLocationResources rempli à partir de l'id de la demande
	public static Object GetDemandeLocationById(
			Long idDemande,
			DemandeLocationRepository demRepository
	){
		try {
			DemandeLocation demandeLocation = demRepository.getOne(idDemande);
			NouvelleDemandeLocationResources resources = new NouvelleDemandeLocationResources(demandeLocation);

			return resources;
		}catch (Exception e){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
		}
	}
	
	// Fonction permettant de récupérer un objet NouvelleDemandeLocationResources rempli à partir de l'id de la demande
	public static DemandeLocationResources GetDemandeLocationResourcesById(
			Long idDemande,
			DemandeLocationRepository demRepository
	){
		DemandeLocation demandeLocation = demRepository.findById(idDemande).get();
		System.out.println("BUG !");
		DemandeLocationResources resources = BOToVO(demandeLocation);

		return resources;
	}

	// Fonction permettant de mettre à jour une demande de location
	public static Object UpdateDemandeLocation(
			NouvelleDemandeLocationResources resources,
			TokenRessources userToken,
			DemandeLocationRepository demandeRepo,
			EtatDemandeRepository etatdemRepo,
			VehiculeRepository vehiculeRepository,
			UtilisateurRepository utilisateurRepository,
			DetailsDemandeRepository detailRepo,
			SiteRepository siteRepo,
			EtapeRepository etapeRepository
	){
		try {
			// Récupération de la demande depuis la BDD
			Optional<DemandeLocation> demandeOpt = demandeRepo.findById(resources.getDemId());
			if(!demandeOpt.isPresent())
				throw new Exception("La demande n'as pas pu être récupérée en base de donnée");

			DemandeLocation demande = demandeOpt.get();

			// Récupération de l'utilisateur connecté à travers le token
			Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(userToken, utilisateurRepository);

			// On set le site de la demande
			Optional<Site> site = siteRepo.findById(resources.getIdSite());
			if(site == null){
				throw new Exception("Le site n'a pas pu être récupéré en base de données");
			}

			//TODO : après réflexion, ça aurait été mieux de passer par une énum que par une table de paramétrage
			EtatDemande etatDemande = etatdemRepo.findFirstByLibelleEquals("EnCoursDeValidation");
			// Vérification que l'entité etatDemande a été récupéré
			if(etatDemande == null)
				throw new Exception("L'entité EtatDemande n'as pas été récupéré");


			//Convertion des strings date départ et arrivée en Date
			Date start = DateUtilities.convertStringToUtilDate(resources.getDateDepart());
			Date end = DateUtilities.convertStringToUtilDate(resources.getDateArrivee());

			// On met la date de la demande à la date du jour
			demande.setDemDateDemande(java.util.Calendar.getInstance().getTime());

			demande.setDemDatedebut(start);
			demande.setDemDatefin(end);
			demande.setEdmEtatdemande(etatDemande);
			demande.setSitSite(site.get());
			demande.setUtiUtilisateur(utilisateur);
			demande.setDEM_AllowCovoiturage(resources.getAllowCovoiturage());
			demande.setCommentaireLibre(resources.getCommentaireLibre());
			demande.setCommentaireCovoiturage(resources.getCommentaireCovoiturage());
			demande.setUtiValideur(null);


			// Ajout du véhicule si renseigné
			if(resources.getIdVehicule() != 0){
				Vehicule vehicule = vehiculeRepository.getOne(resources.getIdVehicule());
				demande.setVehVehicule(vehicule);
			}

			// On ajoute la demande en base
			demandeRepo.save(demande);

			// On supprime les anciennes étapes
			List<Etape> etapes = etapeRepository.findAllByDemDemandelocationOrderByOrdreAsc(demande);
			for(Etape etp : etapes){
				etapeRepository.delete(etp);
			}
//			etapeRepository.deleteAllByDemDemandelocation(demande);

			// On ajoute les étapes du trajet en base
			if(resources.getEtapes() != null){
				for(EtapeResources etapeRes : resources.getEtapes()){
					Etape etape = new Etape();

					etape.setAdresse(etapeRes.getAdresse());
					etape.setDemDemande(demande);
					etape.setOrdre(etapeRes.getOrdre());

					etapeRepository.save(etape);
				}
			}


			//On supprime tous les détails existant
			List<DetailDemande> details = detailRepo.getAllByDemDemandelocation(demande);
			for(DetailDemande detail : details){
				detailRepo.delete(detail);
			}

			// On créé les details liés à la demande ajoutée
			Date current = start;

			// Pour chaque jour entre les deux dates :
			while (current.before(end) || current.equals(end)){
				DetailDemande detail = new DetailDemande();
				detail.setDTD_Date(current);
				detail.setDemDemandelocation(demande);

				// On ajoute en base un nouveau détail
				detailRepo.save(detail);

				// On augmente la date current de 1 jour
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(current);
				calendar.add(Calendar.DATE, 1);
				current = calendar.getTime();
			}

			return demande;

		}catch (Exception e){
			logger.error(e);
			return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
		}
	}

	// Récupère la liste des covoiturages disponibles pour un date donnée
	public static Object GetCovoiturageDispos(
			String dateRef,
			TokenRessources userToken,
			DemandeLocationRepository demandeLocationRepository,
			UtilisateurRepository utilisateurRepository
	){
    	try{

    		Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(userToken, utilisateurRepository);
			Date date = DateUtilities.convertStringToUtilDate(dateRef);

			List<DemandeLocation> demandeLocations = demandeLocationRepository.findByOrderByDemDateDemandeAsc();

			// On récupère les demandes de de l'ortganisme de l'utilisateur
			demandeLocations.stream().filter((demande) ->
					demande.getUtiUtilisateur().getOrgOrganisme().getOrgId() == utilisateur.getOrgOrganisme().getOrgId())
					.collect(Collectors.toList());

			// On récupère la liste des demandes acceptant le covoiturages et dont la date de référence est comprise dans le covoiturage
			demandeLocations = demandeLocations.stream().filter((demande) ->
					demande.getDEM_AllowCovoiturage() == 1 &&
					("Valide").equals(demande.getEdmEtatdemande().getEDM_Libelle()) &&
//					date.after(demande.getDemDatedebut()) &&
					(date.before(demande.getDemDatefin()) || DateUtilities.isSameDay(date, demande.getDemDatefin())))
					.collect(Collectors.toList());

			List<CovoiturageDisposResources> resources = new ArrayList<>();

			for (DemandeLocation demande : demandeLocations){
				resources.add(new CovoiturageDisposResources(demande));
			}

			return resources;
		}catch (Exception e){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
		}
	}

	// Fonction retournant les demandes validées du jour de l'organisme de l'utilisateur
	public static Object getDemandesValideesDuJour(
			TokenRessources token,
			DemandeLocationRepository demandeRepository,
			UtilisateurRepository utilisateurRepository
	){
    	try {
    		// Récupération de l'utilisateur
			Utilisateur utilisateur = UtilisateurService.GetUtilisateurByToken(token, utilisateurRepository);
			boolean isAdmin = utilisateur.getIsAdmin() == 1;

			Date current = Calendar.getInstance().getTime();


			// Récupération et filtrage des demandes
			List<DemandeLocation> demandes = demandeRepository.findAll().stream()
					.filter((demande) ->
							demande.getUtiUtilisateur().getOrgOrganisme().getOrgId() == utilisateur.getOrgOrganisme().getOrgId()
							&& DateUtilities.isBetweenDate(current, demande.getDemDatedebut(), demande.getDemDatefin())
							&& ("Valide").equals(demande.getEdmEtatdemande().getEDM_Libelle())
					)
					.collect(Collectors.toList());

			List<DemandeLocationResources> resources = new ArrayList<>();
			for (DemandeLocation demande : demandes){
				resources.add(new DemandeLocationResources(demande, isAdmin));
			}

			return resources;
		}catch (Exception e){
			return MessageErreur.createMessageErreur(MessageErreur.CODE_KO);
		}
	}

}
