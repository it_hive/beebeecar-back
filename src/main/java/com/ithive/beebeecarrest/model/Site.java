package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;


/**
 * The persistent class for the sit_site database table.
 * 
 */
@Entity
@Table(name="sit_site")
//@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NamedQuery(name="Site.findAll", query="SELECT s FROM Site s")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "sitId")
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SIT_ID", unique=true, nullable=false)
	@EqualsAndHashCode.Include
	private Long sitId;

	@Column(nullable=false, length=100)
	private String SIT_Adresse;

	@Column(name="sit_codepostal", nullable=false, length=5)
	private String SIT_CodePostal;

	@Column(nullable=false, length=100)
	private String SIT_Nom;

	@Column(nullable=false, length=100)
	private String SIT_Ville;

	//bi-directional many-to-one association to OrgOrganisme
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="SIT_ORG_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Organisme orgOrganisme;

	//bi-directional many-to-one association to VehVehicule
	@OneToMany(mappedBy="sitSite")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Vehicule> vehVehicules;

	public Site() {
	}

	public Long getSitId() {
		return this.sitId;
	}

	public void setSitId(Long sitId) {
		this.sitId = sitId;
	}

	public String getSIT_Adresse() {
		return this.SIT_Adresse;
	}

	public void setSIT_Adresse(String SIT_Adresse) {
		this.SIT_Adresse = SIT_Adresse;
	}

	public String getSIT_CodePostal() {
		return this.SIT_CodePostal;
	}

	public void setSIT_CodePostal(String SIT_CodePostal) {
		this.SIT_CodePostal = SIT_CodePostal;
	}

	public String getSIT_Nom() {
		return this.SIT_Nom;
	}

	public void setSIT_Nom(String SIT_Nom) {
		this.SIT_Nom = SIT_Nom;
	}

	public String getSIT_Ville() {
		return this.SIT_Ville;
	}

	public void setSIT_Ville(String SIT_Ville) {
		this.SIT_Ville = SIT_Ville;
	}

	public Organisme getOrgOrganisme() {
		return this.orgOrganisme;
	}

	public void setOrgOrganisme(Organisme orgOrganisme) {
		this.orgOrganisme = orgOrganisme;
	}

	public Set<Vehicule> getVehVehicules() {
		return this.vehVehicules;
	}

	public void setVehVehicules(Set<Vehicule> vehVehicules) {
		this.vehVehicules = vehVehicules;
	}

	public Vehicule addVehVehicule(Vehicule vehVehicule) {
		getVehVehicules().add(vehVehicule);
		vehVehicule.setSitSite(this);

		return vehVehicule;
	}

	public Vehicule removeVehVehicule(Vehicule vehVehicule) {
		getVehVehicules().remove(vehVehicule);
		vehVehicule.setSitSite(null);

		return vehVehicule;
	}

}