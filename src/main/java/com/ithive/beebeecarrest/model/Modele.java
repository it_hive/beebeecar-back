package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;


/**
 * The persistent class for the sit_site database table.
 * 
 */
@Entity
@Table(name="mod_modele")
@NamedQuery(name="Modele.findAll", query="SELECT m FROM Modele m")
public class Modele implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MOD_ID", unique=true, nullable=false)
	@EqualsAndHashCode.Include
	private Long modId;

	@Column(name="MOD_LIBELLE", nullable=false, length=100)
	private String modLibelle;

	//bi-directional many-to-one association to OrgOrganisme
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="MOD_MAR_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Marque marMarque;

	public Modele() {
		
	}

	public Long getModId() {
		return modId;
	}

	public void setModId(Long modId) {
		this.modId = modId;
	}

	public String getModLibelle() {
		return modLibelle;
	}

	public void setModLibelle(String modLibelle) {
		this.modLibelle = modLibelle;
	}

	public Marque getMarMarque() {
		return marMarque;
	}

	public void setMarMarque(Marque marMarque) {
		this.marMarque = marMarque;
	}

}