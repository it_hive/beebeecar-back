package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.ToString;


/**
 * The persistent class for the eng_entretiengarage database table.
 *  Eventuellement retirer le toString
 */
@ToString
@Entity
@Table(name="hkm_historiquekilometrage")
@NamedQuery(name="HistoriqueKilometrage.findAll", query="SELECT h FROM HistoriqueKilometrage h")
public class HistoriqueKilometrage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HKM_ID", unique=true, nullable=false)
	private Long hkmId;

	//bi-directional many-to-one association to VehVehicule
	@ManyToOne
	@JoinColumn(name="HKM_VEH_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Vehicule vehVehicule;
	
	@Column(name="HKM_KILOMETRAGE", nullable=false)
	private Integer hkmKilometrage;
	
	@Column(name="HKM_DATE", unique=true, nullable=false)
	private Date hkmDate;

	public HistoriqueKilometrage() {
	}
	
	public Vehicule getVehVehicule() {
		return this.vehVehicule;
	}

	public void setVehVehicule(Vehicule vehVehicule) {
		this.vehVehicule = vehVehicule;
	}

	public Long getHkmId() {
		return hkmId;
	}

	public void setHkmId(Long hkmId) {
		this.hkmId = hkmId;
	}

	public Integer getHkmKilometrage() {
		return hkmKilometrage;
	}

	public void setHkmKilometrage(Integer hkmKilometrage) {
		this.hkmKilometrage = hkmKilometrage;
	}

	public Date getHkmDate() {
		return hkmDate;
	}

	public void setHkmDate(Date hkmDate) {
		this.hkmDate = hkmDate;
	}
	
}