package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the cle_clevehicule database table.
 * 
 */
@Entity
@Table(name="cle_clevehicule")
@NamedQuery(name="CleVehicule.findAll", query="SELECT c FROM CleVehicule c")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "cleId")
public class CleVehicule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CLE_ID", unique=true, nullable=false)
	private int cleId;

	@Column(length=200)
	private String CLE_Emplacement;

	//bi-directional many-to-one association to UtiUtilisateur
	@ManyToOne
	@JoinColumn(name="CLE_UTI_ID")
	@JsonIdentityReference(alwaysAsId = true)
	private Utilisateur utiUtilisateur;

	//bi-directional many-to-one association to VehVehicule
	@ManyToOne
	@JoinColumn(name="CLE_VEH_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Vehicule vehVehicule;

	public CleVehicule() {
	}

	public int getCleId() {
		return this.cleId;
	}

	public void setCleId(int cleId) {
		this.cleId = cleId;
	}

	public String getCLE_Emplacement() {
		return this.CLE_Emplacement;
	}

	public void setCLE_Emplacement(String CLE_Emplacement) {
		this.CLE_Emplacement = CLE_Emplacement;
	}

	public Utilisateur getUtiUtilisateur() {
		return this.utiUtilisateur;
	}

	public void setUtiUtilisateur(Utilisateur utiUtilisateur) {
		this.utiUtilisateur = utiUtilisateur;
	}

	public Vehicule getVehVehicule() {
		return this.vehVehicule;
	}

	public void setVehVehicule(Vehicule vehVehicule) {
		this.vehVehicule = vehVehicule;
	}

}