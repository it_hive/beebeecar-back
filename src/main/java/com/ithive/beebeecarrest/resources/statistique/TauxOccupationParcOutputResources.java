package com.ithive.beebeecarrest.resources.statistique;

import com.ithive.beebeecarrest.model.sp.TauxOccupationVehicule;
import lombok.Data;

@Data
public class TauxOccupationParcOutputResources {

    private Long vehId;

    private String vehImmatriculation;

    private Float tauxOccupationN;
    private Float tauxOccupationN1;
    private Float tauxOccupationN2;
    private Float tauxOccupationN3;
    private Float tauxOccupationN4;
    private Float tauxOccupationN5;
    private Float tauxOccupationN6;
    private Float tauxOccupationN7;
    private Float tauxOccupationN8;
    private Float tauxOccupationN9;
    private Float tauxOccupationN10;
    private Float tauxOccupationN11;

    public TauxOccupationParcOutputResources(TauxOccupationVehicule taux){
        setVehId(taux.getVehId());
        setVehImmatriculation(taux.getVehImmatriculation());
        setTauxOccupationN(taux.getTauxoccupationn());
        setTauxOccupationN1(taux.getTauxoccupationn1());
        setTauxOccupationN2(taux.getTauxoccupationn2());
        setTauxOccupationN3(taux.getTauxoccupationn3());
        setTauxOccupationN4(taux.getTauxoccupationn4());
        setTauxOccupationN5(taux.getTauxoccupationn5());
        setTauxOccupationN6(taux.getTauxoccupationn6());
        setTauxOccupationN7(taux.getTauxoccupationn7());
        setTauxOccupationN8(taux.getTauxoccupationn8());
        setTauxOccupationN9(taux.getTauxoccupationn9());
        setTauxOccupationN10(taux.getTauxoccupationn10());
        setTauxOccupationN11(taux.getTauxoccupationn11());
    }
}
