package com.ithive.beebeecarrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ithive.beebeecarrest.model.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
	
}
