package com.ithive.beebeecarrest.exceptions;

public class UtilisateurNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UtilisateurNotFoundException(Long id) {
		super("Impossible de trouver l'utilisateur #" + id);
	}
}
