package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityReference;

import java.util.Date;


/**
 * The persistent class for the com_commentairedemande database table.
 * 
 */
@Entity
@Table(name="com_commentairedemande")
@NamedQuery(name="CommentaireDemande.findAll", query="SELECT c FROM CommentaireDemande c")
public class CommentaireDemande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COM_ID", unique=true, nullable=false)
	private int comId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date COM_Date;

	@Column(nullable=false, length=500)
	private String COM_Text;

	//bi-directional many-to-one association to DemDemandelocation
	@ManyToOne
	@JoinColumn(name="COM_DEM_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private DemandeLocation demDemandelocation;

	//bi-directional many-to-one association to UtiUtilisateur
	@ManyToOne
	@JoinColumn(name="COM_UTI_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Utilisateur utiUtilisateur;

	public CommentaireDemande() {
	}

	public int getComId() {
		return this.comId;
	}

	public void setComId(int comId) {
		this.comId = comId;
	}

	public Date getCOM_Date() {
		return this.COM_Date;
	}

	public void setCOM_Date(Date COM_Date) {
		this.COM_Date = COM_Date;
	}

	public String getCOM_Text() {
		return this.COM_Text;
	}

	public void setCOM_Text(String COM_Text) {
		this.COM_Text = COM_Text;
	}

	public DemandeLocation getDemDemandelocation() {
		return this.demDemandelocation;
	}

	public void setDemDemandelocation(DemandeLocation demDemandelocation) {
		this.demDemandelocation = demDemandelocation;
	}

	public Utilisateur getUtiUtilisateur() {
		return this.utiUtilisateur;
	}

	public void setUtiUtilisateur(Utilisateur utiUtilisateur) {
		this.utiUtilisateur = utiUtilisateur;
	}

}