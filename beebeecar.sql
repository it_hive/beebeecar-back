create table edm_etatdemande
(
    EDM_ID      int auto_increment
        primary key,
    EDM_Libelle varchar(40) null
)
    charset = latin1;

create table mar_marque
(
    MAR_ID      int auto_increment
        primary key,
    MAR_Libelle varchar(100) not null
);

create table mod_modele
(
    MOD_ID      int auto_increment
        primary key,
    MOD_MAR_ID  int          not null,
    MOD_Libelle varchar(100) not null,
    constraint mod_modele_ibfk_1
        foreign key (MOD_MAR_ID) references mar_marque (MAR_ID)
);

create table org_organisme
(
    ORG_ID  int auto_increment
        primary key,
    ORG_Nom varchar(100) null
)
    charset = latin1;

create table sit_site
(
    SIT_ID         int auto_increment
        primary key,
    SIT_ORG_ID     int          not null,
    SIT_Nom        varchar(100) not null,
    SIT_Ville      varchar(100) not null,
    SIT_Adresse    varchar(100) not null,
    SIT_CodePostal varchar(5)   not null,
    constraint sit_site_ibfk_1
        foreign key (SIT_ORG_ID) references org_organisme (ORG_ID)
)
    charset = latin1;

create index SIT_ORG_ID
    on sit_site (SIT_ORG_ID);

create table uti_utilisateur
(
    UTI_ID         int auto_increment
        primary key,
    UTI_ORG_ID     int              not null,
    UTI_Mail       varchar(100)     not null,
    UTI_Nom        varchar(100)     not null,
    UTI_Prenom     varchar(100)     not null,
    UTI_MotDePasse varchar(1000)    not null,
    UTI_Telephone  varchar(20)      not null,
    UTI_PhotoUrl   varchar(400)     null,
    UTI_IsAdmin    bit              not null,
    UTI_HasPermis  bit              not null,
    UTI_Archived   bit default b'0' not null,
    constraint uti_utilisateur_ibfk_2
        foreign key (UTI_ORG_ID) references org_organisme (ORG_ID)
)
    charset = latin1;

create index UTI_ORG_ID
    on uti_utilisateur (UTI_ORG_ID);

create table veh_vehicule
(
    VEH_ID                int auto_increment
        primary key,
    VEH_SIT_ID            int          not null,
    VEH_Immatriculation   varchar(40)  not null,
    VEH_MOD_ID            int          not null,
    VEH_Kilometrage       int          not null,
    VEH_DateMiseEnService datetime     not null,
    VEH_DateFinService    datetime     null,
    VEH_PhotoUrl          varchar(400) null,
    VEH_Details           varchar(400) not null,
    VEH_Couleur           varchar(10)  null,
    constraint veh_vehicule_ibfk_1
        foreign key (VEH_SIT_ID) references sit_site (SIT_ID),
    constraint veh_vehicule_ibfk_2
        foreign key (VEH_MOD_ID) references mod_modele (MOD_ID)
)
    charset = latin1;

create table cle_clevehicule
(
    CLE_ID          int auto_increment
        primary key,
    CLE_VEH_ID      int          not null,
    CLE_UTI_ID      int          null,
    CLE_Emplacement varchar(200) null,
    constraint cle_clevehicule_ibfk_1
        foreign key (CLE_VEH_ID) references veh_vehicule (VEH_ID),
    constraint cle_clevehicule_ibfk_2
        foreign key (CLE_UTI_ID) references uti_utilisateur (UTI_ID)
)
    charset = latin1;

create index CLE_UTI_ID
    on cle_clevehicule (CLE_UTI_ID);

create index CLE_VEH_ID
    on cle_clevehicule (CLE_VEH_ID);

create table dem_demandelocation
(
    DEM_ID                     int auto_increment
        primary key,
    DEM_UTI_ID                 int           not null,
    DEM_VEH_ID                 int           null,
    DEM_EDM_ID                 int           not null,
    DEM_SIT_ID                 int           not null,
    DEM_DateDemande            datetime      not null,
    DEM_DateDebut              datetime      not null,
    DEM_DateFin                datetime      null,
    DEM_AllowCovoiturage       bit           not null,
    DEM_UTI_ValideurId         int           null,
    DEM_CommentaireLibre       varchar(1000) null,
    DEM_CommentaireCovoiturage varchar(1000) null,
    constraint dem_demandelocation_ibfk_1
        foreign key (DEM_UTI_ID) references uti_utilisateur (UTI_ID),
    constraint dem_demandelocation_ibfk_2
        foreign key (DEM_VEH_ID) references veh_vehicule (VEH_ID),
    constraint dem_demandelocation_ibfk_3
        foreign key (DEM_EDM_ID) references edm_etatdemande (EDM_ID),
    constraint dem_demandelocation_ibfk_4
        foreign key (DEM_UTI_ValideurId) references uti_utilisateur (UTI_ID),
    constraint dem_demandelocation_sit_site
        foreign key (DEM_SIT_ID) references sit_site (SIT_ID)
)
    charset = latin1;

create table com_commentairedemande
(
    COM_ID     int auto_increment
        primary key,
    COM_DEM_ID int          not null,
    COM_UTI_ID int          not null,
    COM_Date   datetime     not null,
    COM_Text   varchar(500) not null,
    constraint com_commentairedemande_ibfk_1
        foreign key (COM_DEM_ID) references dem_demandelocation (DEM_ID),
    constraint com_commentairedemande_ibfk_2
        foreign key (COM_UTI_ID) references uti_utilisateur (UTI_ID)
)
    charset = latin1;

create index COM_DEM_ID
    on com_commentairedemande (COM_DEM_ID);

create index COM_UTI_ID
    on com_commentairedemande (COM_UTI_ID);

create index DEM_EDM_ID
    on dem_demandelocation (DEM_EDM_ID);

create index DEM_SIT_ID
    on dem_demandelocation (DEM_SIT_ID);

create index DEM_UTI_ID
    on dem_demandelocation (DEM_UTI_ID);

create index DEM_UTI_ValideurId
    on dem_demandelocation (DEM_UTI_ValideurId);

create index DEM_VEH_ID
    on dem_demandelocation (DEM_VEH_ID);

create table dtd_detaildemande
(
    DTD_ID     int auto_increment
        primary key,
    DTD_DEM_ID int      not null,
    DTD_Date   datetime not null,
    constraint dtd_detaildemande_ibfk_1
        foreign key (DTD_DEM_ID) references dem_demandelocation (DEM_ID)
)
    charset = latin1;

create index DTD_DEM_ID
    on dtd_detaildemande (DTD_DEM_ID);

create table eng_entretiengarage
(
    ENG_ID     int auto_increment
        primary key,
    ENG_VEH_ID int not null,
    constraint eng_entretiengarage_ibfk_1
        foreign key (ENG_VEH_ID) references veh_vehicule (VEH_ID)
)
    charset = latin1;

create index ENG_VEH_ID
    on eng_entretiengarage (ENG_VEH_ID);

create table etp_etape
(
    ETP_ID      int auto_increment
        primary key,
    ETP_DEM_ID  int          not null,
    ETP_Adresse varchar(200) not null,
    ETP_Ordre   int          not null,
    constraint FK__dem_demandelocation
        foreign key (ETP_DEM_ID) references dem_demandelocation (DEM_ID)
);

create index ETP_DEM_ID
    on etp_etape (ETP_DEM_ID);

create table hkm_historiquekilometrage
(
    HKM_ID          int auto_increment
        primary key,
    HKM_VEH_ID      int      not null,
    HKM_Kilometrage int      not null,
    HKM_Date        datetime not null,
    constraint hkm_historiqueKilometrage_ibfk_1
        foreign key (HKM_VEH_ID) references veh_vehicule (VEH_ID)
);

create index VEH_MOD_ID
    on veh_vehicule (VEH_MOD_ID);

create index VEH_SIT_ID
    on veh_vehicule (VEH_SIT_ID);

create
    definer = ithive@`%` function fn_GetKilometrageVehiculeForMonth(idVehicule int, dateRef datetime) returns int
BEGIN

    RETURN (
        SELECT (
                       (
                           SELECT HKM_Kilometrage
                           FROM hkm_historiquekilometrage
                           WHERE HKM_Date between dateRef AND DATE_ADD(DATE_ADD(dateRef, INTERVAL 1 MONTH), INTERVAL -1 DAY)
                             AND HKM_VEH_ID = idVehicule
                           ORDER BY HKM_Kilometrage desc
                           LIMIT 1
                       ) - (
                           SELECT IFNULL(
                                          (SELECT HKM_Kilometrage
                                           FROM hkm_historiquekilometrage
                                           WHERE HKM_Date between DATE_ADD(dateRef, INTERVAL -1 MONTH) AND DATE_ADD(dateRef, INTERVAL -1 DAY)
                                             AND HKM_VEH_ID = idVehicule
                                           ORDER BY HKM_Kilometrage desc
                                           LIMIT 1)
                                      ,
                                          (SELECT HKM_Kilometrage
                                           FROM hkm_historiquekilometrage
                                           WHERE HKM_Date between dateRef AND DATE_ADD(DATE_ADD(dateRef, INTERVAL 1 MONTH), INTERVAL -1 DAY)
                                             AND HKM_VEH_ID = idVehicule
                                           ORDER BY HKM_Kilometrage ASC
                                           LIMIT 1)
                                      )
                       )
                   )
    );

END;

create
    definer = ithive@`%` function fn_getNombreReservationMoisPourSite(idSite int, dateRef datetime) returns int
BEGIN

    SET @dateDeb := date_add(date_add(LAST_DAY(dateRef), interval 1 DAY), interval -1 MONTH);
    SET @dateFin := LAST_DAY(@dateDeb);

    RETURN (
        SELECT COUNT(*) as 'Total'
        FROM dem_demandelocation dem
        WHERE 1 = 1
          AND dem.DEM_SIT_ID = idSite
          AND dem.DEM_DateDemande BETWEEN @dateDeb AND @dateFin
    );

END;

create
    definer = ithive@`%` function fn_getNombreReservationMoisPourSiteAndEtat(idSite int, dateRef datetime, idEdm int) returns int
BEGIN

    SET @dateDeb := date_add(date_add(LAST_DAY(dateRef), interval 1 DAY), interval -1 MONTH);
    SET @dateFin := LAST_DAY(@dateDeb);

    RETURN (
        SELECT COUNT(*) as 'Total'
        FROM dem_demandelocation dem
                 INNER JOIN edm_etatdemande edm on edm.EDM_ID = dem.DEM_EDM_ID
        WHERE 1 = 1
          AND dem.DEM_SIT_ID = idSite
          AND edm.EDM_ID = idEdm
          AND dem.DEM_DateDemande BETWEEN @dateDeb AND @dateFin
    );

END;

create
    definer = ithive@`%` function fn_getNombreUtilisateurDemandeurForMonth(idOrganisme int, dateRef datetime) returns int
BEGIN

    SET @dateDeb := date_add(date_add(LAST_DAY(dateRef), interval 1 DAY), interval -1 MONTH);
    SET @dateFin := LAST_DAY(dateRef);


    RETURN (
        SELECT COUNT(DISTINCT (dem.DEM_UTI_ID))
        FROM dem_demandelocation dem
                 INNER JOIN uti_utilisateur uti on dem.DEM_UTI_ID = uti.UTI_ID
        WHERE 1 = 1
          AND uti.UTI_ORG_ID = idOrganisme
          AND dem.DEM_DateDemande BETWEEN @dateDeb AND @dateFin
    );

END;

create
    definer = ithive@`%` function fn_getTauxOccupationMoisForSite(idSite int, dateRef datetime) returns decimal(5, 2)
BEGIN

    SET @S := DATE_ADD(dateRef, INTERVAL -1 DAY);
    SET @E := DATE_ADD(DATE_ADD(dateRef, INTERVAL 1 MONTH), INTERVAL -1 DAY);

    RETURN (
        SELECT CAST(ROUND(((COUNT(dtd.DTD_ID) /
                            ((5 * (DATEDIFF(@E, @S) DIV 7) + MID('0123444401233334012222340111123400012345001234550',
                                                                 7 * WEEKDAY(@S) + WEEKDAY(@E) + 1, 1))
                                * (SELECT COUNT(veh1.VEH_ID)
                                   FROM veh_vehicule veh1
                                            INNER JOIN sit_site sit1 on sit1.SIT_ID = veh1.VEH_SIT_ID
                                   WHERE sit1.SIT_ID = idSite))
                               )
            * 100), 2) AS DECIMAL(5, 2))
        FROM veh_vehicule veh
                 INNER JOIN sit_site sit on sit.SIT_ID = veh.VEH_SIT_ID
                 LEFT JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh.VEH_ID
                 LEFT JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
        WHERE 1 = 1
          AND (
                dtd.DTD_Date BETWEEN dateRef AND @E
                OR
                dtd.DTD_Date is null
            )
          AND sit.SIT_ID = idSite
    );

END;

create
    definer = ithive@`%` function fn_getTauxOccupationMoisForVehicule(idVehicule int, dateDebut datetime, dateFin datetime) returns decimal(5, 2)
BEGIN


    RETURN (
        SELECT ROUND(((COUNT(dtd.DTD_ID) /
                       (5 * (DATEDIFF(dateFin, dateDebut) DIV 7) +
                        MID('0123444401233334012222340111123400012345001234550',
                            7 * WEEKDAY(dateDebut) + WEEKDAY(dateFin) + 1, 1)))
            * 100), 2) as 'TauxOccupation'
        FROM veh_vehicule veh
                 LEFT JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh.VEH_ID
                 LEFT JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
        WHERE 1 = 1
          AND (
                dtd.DTD_Date BETWEEN dateDebut AND dateFin
                OR
                dtd.DTD_Date is null
            )
          AND veh.VEH_ID = idVehicule
    );

END;

create
    definer = ithive@`%` function fn_getTotalVehiculeDisponibleForDate(idOrganisme int, dateRef datetime) returns int
BEGIN


    RETURN (
        SELECT COUNT(veh.VEH_ID)
        FROM veh_vehicule veh
                 INNER JOIN sit_site sit on veh.VEH_SIT_ID = sit.SIT_ID
        WHERE veh.VEH_ID NOT IN (
            SELECT veh1.VEH_ID
            FROM veh_vehicule veh1
                     LEFT JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh1.VEH_ID
                     LEFT JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
            WHERE 1 = 1
              AND dtd.DTD_Date = dateRef
        )
          AND sit.SIT_ORG_ID = idOrganisme
    );

END;

create
    definer = ithive@`%` function fn_getTotalVehiculeIndisponibleForDate(idOrganisme int, dateRef datetime) returns int
BEGIN


    RETURN (
        SELECT COUNT(veh.VEH_ID)
        FROM veh_vehicule veh
                 INNER JOIN sit_site sit on veh.VEH_SIT_ID = sit.SIT_ID
        WHERE veh.VEH_ID IN (
            SELECT veh1.VEH_ID
            FROM veh_vehicule veh1
                     LEFT JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh1.VEH_ID
                     LEFT JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
            WHERE 1 = 1
              AND dtd.DTD_Date = dateRef
        )
          AND sit.SIT_ORG_ID = idOrganisme
    );

END;

create
    definer = ithive@`%` procedure sp_GetActiviteVehiculeOrganismeForPeriode(IN IdOrganisme int, IN dateDebut date, IN dateFin date)
    comment 'Procédure récupérant l''activité des véhicules d''un organisme pour une période donnée'
BEGIN

    SELECT VEH_ID,
           DEM_ID,
           DEM_AllowCovoiturage,
           UTI_ID,
           UTI_Nom,
           UTI_Prenom,
           DTD_Date,
           EDM_Libelle
    FROM veh_vehicule veh
             INNER JOIN sit_site sit on sit.SIT_ID = veh.VEH_SIT_ID
             INNER JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh.VEH_ID
             INNER JOIN edm_etatdemande edm on edm.EDM_ID = dem.DEM_EDM_ID
             INNER JOIN uti_utilisateur uti on uti.UTI_ID = dem.DEM_UTI_ID
             INNER JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
    WHERE 1 = 1
      AND EDM_Libelle like 'Valide'
      AND SIT_ORG_ID = IdOrganisme
      AND DTD_Date BETWEEN dateDebut AND dateFin;

END;

create
    definer = ithive@`%` procedure sp_GetActiviteVehiculeOrganismeForPeriodeTest(IN IdOrganisme int, IN dateDebut date, IN dateFin date)
BEGIN

    SELECT VEH_ID,
           VEH_DateMiseEnService,
           DEM_ID,
           DEM_AllowCovoiturage,
           UTI_ID,
           UTI_Nom,
           UTI_Prenom,
           DTD_ID,
           DTD_Date,
           EDM_ID,
           EDM_Libelle
    FROM veh_vehicule veh
             INNER JOIN sit_site sit on sit.SIT_ID = veh.VEH_SIT_ID
             INNER JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh.VEH_ID
             INNER JOIN edm_etatdemande edm on edm.EDM_ID = dem.DEM_EDM_ID
             INNER JOIN uti_utilisateur uti on uti.UTI_ID = dem.DEM_UTI_ID
             INNER JOIN dtd_detaildemande dtd on dtd.DTD_DEM_ID = dem.DEM_ID
    WHERE 1 = 1
      AND EDM_Libelle like 'Valide'
      AND SIT_ORG_ID = IdOrganisme
      AND DTD_Date BETWEEN dateDebut AND dateFin;

END;

create
    definer = ithive@`%` procedure sp_GetEtatParc(IN IdOrganisme int, IN DateRef datetime)
BEGIN

    SELECT `fn_getTotalVehiculeDisponibleForDate`(IdOrganisme, DateRef)   as 'vehiculeDispo',
           `fn_getTotalVehiculeIndisponibleForDate`(IdOrganisme, DateRef) as 'vehiculeIndispo';

END;

create
    definer = ithive@`%` procedure sp_GetNombreDemandeParMoisParSite(IN IdOrganisme int, IN DateRef datetime)
BEGIN


    SET @dateRefN1 := DATE_ADD(DateRef, INTERVAL -1 MONTH);
    SET @dateRefN2 := DATE_ADD(DateRef, INTERVAL -2 MONTH);
    SET @dateRefN3 := DATE_ADD(DateRef, INTERVAL -3 MONTH);
    SET @dateRefN4 := DATE_ADD(DateRef, INTERVAL -4 MONTH);
    SET @dateRefN5 := DATE_ADD(DateRef, INTERVAL -5 MONTH);
    SET @dateRefN6 := DATE_ADD(DateRef, INTERVAL -6 MONTH);
    SET @dateRefN7 := DATE_ADD(DateRef, INTERVAL -7 MONTH);
    SET @dateRefN8 := DATE_ADD(DateRef, INTERVAL -8 MONTH);
    SET @dateRefN9 := DATE_ADD(DateRef, INTERVAL -9 MONTH);
    SET @dateRefN10 := DATE_ADD(DateRef, INTERVAL -10 MONTH);
    SET @dateRefN11 := DATE_ADD(DateRef, INTERVAL -11 MONTH);

    SELECT sit.SIT_ID,
           sit.SIT_Nom,
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, DateRef)     as 'DemandesN',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN1)  as 'DemandesN1',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN2)  as 'DemandesN2',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN3)  as 'DemandesN3',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN4)  as 'DemandesN4',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN5)  as 'DemandesN5',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN6)  as 'DemandesN6',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN7)  as 'DemandesN7',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN8)  as 'DemandesN8',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN9)  as 'DemandesN9',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN10) as 'DemandesN10',
           fn_getNombreReservationMoisPourSite(sit.SIT_ID, @dateRefN11) as 'DemandesN11'
    FROM sit_site sit
    WHERE sit.SIT_ORG_ID = IdOrganisme;

END;

create
    definer = ithive@`%` procedure sp_GetNombreDemandeParMoisParSiteAndEtat(IN IdOrganisme int, IN DateRef datetime)
BEGIN
    SET @dateRefN1 := DATE_ADD(DateRef, INTERVAL -1 MONTH);
    SET @dateRefN2 := DATE_ADD(DateRef, INTERVAL -2 MONTH);
    SET @dateRefN3 := DATE_ADD(DateRef, INTERVAL -3 MONTH);
    SET @dateRefN4 := DATE_ADD(DateRef, INTERVAL -4 MONTH);
    SET @dateRefN5 := DATE_ADD(DateRef, INTERVAL -5 MONTH);
    SET @dateRefN6 := DATE_ADD(DateRef, INTERVAL -6 MONTH);
    SET @dateRefN7 := DATE_ADD(DateRef, INTERVAL -7 MONTH);
    SET @dateRefN8 := DATE_ADD(DateRef, INTERVAL -8 MONTH);
    SET @dateRefN9 := DATE_ADD(DateRef, INTERVAL -9 MONTH);
    SET @dateRefN10 := DATE_ADD(DateRef, INTERVAL -10 MONTH);
    SET @dateRefN11 := DATE_ADD(DateRef, INTERVAL -11 MONTH);

    SELECT sit.SIT_ID,
           sit.SIT_Nom,
           edm.EDM_ID,
           edm.EDM_Libelle,
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, DateRef, edm.EDM_ID)     as 'DemandesN',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN1, edm.EDM_ID)  as 'DemandesN1',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN2, edm.EDM_ID)  as 'DemandesN2',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN3, edm.EDM_ID)  as 'DemandesN3',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN4, edm.EDM_ID)  as 'DemandesN4',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN5, edm.EDM_ID)  as 'DemandesN5',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN6, edm.EDM_ID)  as 'DemandesN6',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN7, edm.EDM_ID)  as 'DemandesN7',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN8, edm.EDM_ID)  as 'DemandesN8',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN9, edm.EDM_ID)  as 'DemandesN9',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN10, edm.EDM_ID) as 'DemandesN10',
           fn_getNombreReservationMoisPourSiteAndEtat(sit.SIT_ID, @dateRefN11, edm.EDM_ID) as 'DemandesN11'
    FROM sit_site sit
             CROSS JOIN edm_etatdemande edm
    WHERE sit.SIT_ORG_ID = IdOrganisme;

END;

create
    definer = ithive@`%` procedure sp_GetNombreUtilisateurActifsMonthByMonth(IN IdOrganisme int, IN DateRef datetime)
BEGIN


    SET @dateRefN1 := DATE_ADD(DateRef, INTERVAL -1 MONTH);
    SET @dateRefN2 := DATE_ADD(DateRef, INTERVAL -2 MONTH);
    SET @dateRefN3 := DATE_ADD(DateRef, INTERVAL -3 MONTH);
    SET @dateRefN4 := DATE_ADD(DateRef, INTERVAL -4 MONTH);
    SET @dateRefN5 := DATE_ADD(DateRef, INTERVAL -5 MONTH);
    SET @dateRefN6 := DATE_ADD(DateRef, INTERVAL -6 MONTH);
    SET @dateRefN7 := DATE_ADD(DateRef, INTERVAL -7 MONTH);
    SET @dateRefN8 := DATE_ADD(DateRef, INTERVAL -8 MONTH);
    SET @dateRefN9 := DATE_ADD(DateRef, INTERVAL -9 MONTH);
    SET @dateRefN10 := DATE_ADD(DateRef, INTERVAL -10 MONTH);
    SET @dateRefN11 := DATE_ADD(DateRef, INTERVAL -11 MONTH);

    SELECT `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, DateRef)     as 'UtilisateurActifsN'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN1)  as 'UtilisateurActifsN-1'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN2)  as 'UtilisateurActifsN-2'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN3)  as 'UtilisateurActifsN-3'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN4)  as 'UtilisateurActifsN-4'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN5)  as 'UtilisateurActifsN-5'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN6)  as 'UtilisateurActifsN-6'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN7)  as 'UtilisateurActifsN-7'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN8)  as 'UtilisateurActifsN-8'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN9)  as 'UtilisateurActifsN-9'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN10) as 'UtilisateurActifsN-10'
         , `fn_getNombreUtilisateurDemandeurForMonth`(IdOrganisme, @dateRefN11) as 'UtilisateurActifsN-11';

END;

create
    definer = ithive@`%` procedure sp_GetTauxOccupationSiteByMonth(IN IdOrganisme int, IN DateRef datetime)
BEGIN


    SET @dateRefN1 := DATE_ADD(dateRef, INTERVAL -1 MONTH);
    SET @dateRefN2 := DATE_ADD(dateRef, INTERVAL -2 MONTH);
    SET @dateRefN3 := DATE_ADD(dateRef, INTERVAL -3 MONTH);
    SET @dateRefN4 := DATE_ADD(dateRef, INTERVAL -4 MONTH);
    SET @dateRefN5 := DATE_ADD(dateRef, INTERVAL -5 MONTH);
    SET @dateRefN6 := DATE_ADD(dateRef, INTERVAL -6 MONTH);
    SET @dateRefN7 := DATE_ADD(dateRef, INTERVAL -7 MONTH);
    SET @dateRefN8 := DATE_ADD(dateRef, INTERVAL -8 MONTH);
    SET @dateRefN9 := DATE_ADD(dateRef, INTERVAL -9 MONTH);
    SET @dateRefN10 := DATE_ADD(dateRef, INTERVAL -10 MONTH);
    SET @dateRefN11 := DATE_ADD(dateRef, INTERVAL -11 MONTH);

    SELECT sit.SIT_ID
         , sit.SIT_Nom
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, dateRef), 0)     as 'TauxOccupationN'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN1), 0)  as 'TauxOccupationN-1'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN2), 0)  as 'TauxOccupationN-2'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN3), 0)  as 'TauxOccupationN-3'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN4), 0)  as 'TauxOccupationN-4'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN5), 0)  as 'TauxOccupationN-5'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN6), 0)  as 'TauxOccupationN-6'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN7), 0)  as 'TauxOccupationN-7'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN8), 0)  as 'TauxOccupationN-8'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN9), 0)  as 'TauxOccupationN-9'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN10), 0) as 'TauxOccupationN-10'
         , IFNULL(`fn_getTauxOccupationMoisForSite`(sit.SIT_ID, @dateRefN11), 0) as 'TauxOccupationN-11'
    FROM sit_site sit
    WHERE 1 = 1
      AND SIT_ORG_ID = IdOrganisme;

END;

create
    definer = ithive@`%` procedure sp_GetTauxOccupationVehiculeByMonth(IN IdOrganisme int, IN DateRef datetime)
BEGIN

    SET @dateRefN1 := DATE_ADD(dateRef, INTERVAL -1 MONTH);
    SET @dateRefN2 := DATE_ADD(dateRef, INTERVAL -2 MONTH);
    SET @dateRefN3 := DATE_ADD(dateRef, INTERVAL -3 MONTH);
    SET @dateRefN4 := DATE_ADD(dateRef, INTERVAL -4 MONTH);
    SET @dateRefN5 := DATE_ADD(dateRef, INTERVAL -5 MONTH);
    SET @dateRefN6 := DATE_ADD(dateRef, INTERVAL -6 MONTH);
    SET @dateRefN7 := DATE_ADD(dateRef, INTERVAL -7 MONTH);
    SET @dateRefN8 := DATE_ADD(dateRef, INTERVAL -8 MONTH);
    SET @dateRefN9 := DATE_ADD(dateRef, INTERVAL -9 MONTH);
    SET @dateRefN10 := DATE_ADD(dateRef, INTERVAL -10 MONTH);
    SET @dateRefN11 := DATE_ADD(dateRef, INTERVAL -11 MONTH);

    SELECT veh.VEH_ID
         , veh.VEH_Immatriculation
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, dateRef, LAST_DAY(dateRef))         as 'TauxOccupationN'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN1, LAST_DAY(@dateRefN1))   as 'TauxOccupationN-1'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN2, LAST_DAY(@dateRefN2))   as 'TauxOccupationN-2'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN3, LAST_DAY(@dateRefN3))   as 'TauxOccupationN-3'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN4, LAST_DAY(@dateRefN4))   as 'TauxOccupationN-4'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN5, LAST_DAY(@dateRefN5))   as 'TauxOccupationN-5'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN6, LAST_DAY(@dateRefN6))   as 'TauxOccupationN-6'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN7, LAST_DAY(@dateRefN7))   as 'TauxOccupationN-7'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN8, LAST_DAY(@dateRefN8))   as 'TauxOccupationN-8'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN9, LAST_DAY(@dateRefN9))   as 'TauxOccupationN-9'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN10, LAST_DAY(@dateRefN10)) as 'TauxOccupationN-10'
         , `fn_getTauxOccupationMoisForVehicule`(veh.VEH_ID, @dateRefN11, LAST_DAY(@dateRefN11)) as 'TauxOccupationN-11'
    FROM veh_vehicule veh
             INNER JOIN sit_site sit on sit.SIT_ID = veh.VEH_SIT_ID
    WHERE 1 = 1
      AND sit.SIT_ORG_ID = IdOrganisme;

END;

create
    definer = ithive@`%` procedure sp_GetVehiculeDispoForPeriode(IN IdOrganisme int, IN DateDebut date, IN DateFin date)
    comment 'Récupère la liste des véhicules disponibles sur une période'
BEGIN

    SELECT veh.*
    FROM veh_vehicule veh
    WHERE veh.VEH_ID NOT IN (
        SELECT DISTINCT veh.VEH_ID
                        --		*
        FROM veh_vehicule veh
                 INNER JOIN dem_demandelocation dem on dem.DEM_VEH_ID = veh.VEH_ID
                 INNER JOIN edm_etatdemande edm on edm.EDM_ID = dem.DEM_EDM_ID
                 INNER JOIN sit_site sit on sit.SIT_ID = veh.VEH_SIT_ID
        WHERE 1 = 1
          AND sit.SIT_ORG_ID = IdOrganisme
          AND edm.EDM_Libelle LIKE 'Valide'
          AND dem.DEM_DateDebut > DateDebut
          AND dem.DEM_DateFin < DateFin
    )
      AND VEH_DateMiseEnService < DateDebut
      AND (VEH_DateFinService IS NULL
        OR VEH_DateFinService > DateFin);


END;

create
    definer = ithive@`%` procedure sp_GetVehiculeOrganismeActifForPeriode(IN IdOrganisme int, IN DateDebut datetime, IN DateFin datetime)
    comment 'Récupère la liste des véhicules actifs sur une période, pour un organisme'
BEGIN

    SELECT veh.*
    FROM veh_vehicule veh
             INNER JOIN sit_site sit ON sit.SIT_ID = veh.VEH_SIT_ID
    WHERE 1 = 1
      AND SIT_ORG_ID = IdOrganisme
      AND VEH_DateMiseEnService < DateDebut
      AND (VEH_DateFinService IS NULL
        OR VEH_DateFinService > DateFin);


END;

