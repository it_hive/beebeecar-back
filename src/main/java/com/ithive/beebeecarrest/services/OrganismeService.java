package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.model.Organisme;
import com.ithive.beebeecarrest.repositories.OrganismeRepository;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.OrganismeResources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class OrganismeService {

    private static Logger logger = LogManager.getLogger(OrganismeService.class);

    private OrganismeRepository organismeRepository;

    public OrganismeService(OrganismeRepository organismeRepository){
        this.organismeRepository = organismeRepository;
    }

    private OrganismeResources BoToVo(Organisme bo){
        OrganismeResources vo = new OrganismeResources();

        vo.setIdOrganisme(bo.getOrgId());
        vo.setNom(bo.getORG_Nom());

        return  vo;
    }

    private List<OrganismeResources> BoToVos(List<Organisme> listeBO){
        List<OrganismeResources> listVO = new LinkedList<OrganismeResources>();
        for(Organisme bo : listeBO){
            listVO.add(BoToVo(bo));
        }
        return listVO;
    }

    public List<OrganismeResources> findAll(){
        logger.info("find all - organisme Services");
        return BoToVos(this.organismeRepository.findAll());
    }

    public  Object getOrganismeByUtilisateur(UtilisateurResources utilisateurResources){
        Optional<Organisme> option =  this.organismeRepository.findById(utilisateurResources.getIdOrganisme());
        if(option.isPresent()){
            return this.BoToVo(option.get());
        } else {
            return MessageErreur.createMessageErreur(MessageErreur.CODE_ORGANISME_INEXISTANT);
        }
    }
}
