package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class ConnexionRessources {

    private String mailUtilisateur;
    private String motDePasse;
}
