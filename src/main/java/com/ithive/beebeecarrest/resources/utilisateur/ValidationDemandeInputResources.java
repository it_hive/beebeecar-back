package com.ithive.beebeecarrest.resources.utilisateur;

import lombok.Data;

@Data
public class ValidationDemandeInputResources {

    private Long idDemande;

    private Long idVehicule;
}
