package com.ithive.beebeecarrest.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ithive.beebeecarrest.exceptions.ModeleNotFoundException;
import com.ithive.beebeecarrest.exceptions.SiteNotFoundException;
import com.ithive.beebeecarrest.model.Site;
import com.ithive.beebeecarrest.model.Modele;
import com.ithive.beebeecarrest.repositories.MarqueRepository;
import com.ithive.beebeecarrest.repositories.ModeleRepository;
import com.ithive.beebeecarrest.repositories.SiteRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.repositories.VehiculeRepository;
import com.ithive.beebeecarrest.resources.ModeleResources;

public class ModeleService {

	private static Logger logger = LogManager.getLogger(ModeleService.class);

	public static List<ModeleResources> BotoVos(List<Modele> listBO) {
		List<ModeleResources> listVO = new LinkedList<ModeleResources>();
		for (Modele bo : listBO) {
			listVO.add(BOToVO(bo));
		}
		return listVO;
	}

	public static Modele VOToBO(
			ModeleResources vo) {
		Modele bo = new Modele();
		
		bo.setModId(vo.getModId());
		bo.setModLibelle(vo.getLibelle());

		return bo;
	}
	
	public static ModeleResources BOToVO(Modele bo) {
		ModeleResources vo = new ModeleResources();

		vo.setModId(bo.getModId());
		vo.setLibelle(bo.getModLibelle());

		return vo;
	}
	
}
