package com.ithive.beebeecarrest.resources;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.Etape;
import com.ithive.beebeecarrest.resources.demandeLocation.EtapeResources;
import com.ithive.beebeecarrest.services.VehiculeService;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class DemandeLocationResources {


	private Long demId;

	private byte allowCovoiturage;

	private String dateDebut;

	private String dateDemande;

	private String dateFin;

	private Long etatId;

	private String etatDemande;

	private VehiculeResources vehicule;

	private List<EtapeResources> etapes;

	private String nomUtilisateur;

	private String prenomUtilisateur;

	private Long idUtilisateur;

	private Long idSite;

	private String nomSite;

	private Boolean canEdit;

	private Boolean canCancel;

	private Boolean canValidate;

	private String commentaire;

	private String commentaireCovoit;

	public DemandeLocationResources() {
		super();
	}

	// Constructeur permettant de construire la ressource à partir du business object DemandeLocation
	public DemandeLocationResources(DemandeLocation demande, boolean isAdmin) {
		setAllowCovoiturage(demande.getDEM_AllowCovoiturage());
		setDateDebut(DateUtilities.convertUtilDateToString(demande.getDemDatedebut()));
		setDateFin(DateUtilities.convertUtilDateToString(demande.getDemDatefin()));
		setDateDemande(DateUtilities.convertUtilDateToString(demande.getDemDateDemande()));
		setDemId(demande.getDemId());
		setEtatDemande(demande.getEdmEtatdemande().getEDM_Libelle());
		setEtatId((long) demande.getEdmEtatdemande().getEdmId());
		if(demande.getVehVehicule() != null){
			setVehicule(VehiculeService.BOToVO(demande.getVehVehicule()));
		}
		setIdUtilisateur(demande.getUtiUtilisateur().getUtiId());
		setNomUtilisateur(demande.getUtiUtilisateur().getUTI_Nom());
		setPrenomUtilisateur(demande.getUtiUtilisateur().getUTI_Prenom());
		setCommentaire(demande.getCommentaireLibre());
		setCommentaireCovoit(demande.getCommentaireCovoiturage());
		//Ajout des etapes
		List<EtapeResources> etapes = new ArrayList<>();
		if(demande.getEtpEtape() != null){
			for(Etape etape : demande.getEtpEtape().stream().collect(Collectors.toList())){
				etapes.add(new EtapeResources(etape));
			}
			setEtapes(etapes);
		}
		setIdSite(demande.getSitSite().getSitId());
		setNomSite(demande.getSitSite().getSIT_Nom());

		// On vérifie l'état de la demande
		switch (demande.getEdmEtatdemande().getEDM_Libelle()){
			// Demande en cours de validation
			case "EnCoursDeValidation" :
				// Si l'utilisateur n'est pas admin
				// Alors il peut editer et annuler
				// Mais ne peut pas valider/refuser
				setCanEdit(!isAdmin);
				setCanCancel(!isAdmin);
				setCanValidate(isAdmin);
				break;
			// Si la demande est validée
			case "Valide" :
				// La demande ne peut être validée ou refusée
				setCanValidate(false);
				// Si le premier jour de la demande est passé
				if(demande.getDemDatedebut().before(new Date())){
					// Seul l'admin peut annuler la demande
					setCanCancel(isAdmin);
					setCanEdit(false);
				}else{
					// Si le premier jour de la demande n'est pas passé
					// Seul l'utilisateur peut annuler la demande
					// L'utilisateur peut modifier sa demande
					setCanCancel(!isAdmin);
					setCanEdit(!isAdmin);
				}
				break;
			// Si demande ni en cours, ni validée :
			default :
				// On ne peut pas modifier/ annuler / valider / refuser
				setCanEdit(false);
				setCanCancel(false);
				setCanValidate(false);
				break;
		}
	}
}
