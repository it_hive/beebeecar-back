package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class MessageErreur {

    //  code ok
    public static int CODE_OK = 0;

    // code ko générique
    public static int CODE_KO = 1;

    // DEMANDE
    // Une clé est rattaché à l'utilisateur, l'archivage n'est pas possible
    public static int CODE_DEMANDE_ARCHIVAGE_CLE = 101;
    // Il existe au moins une demande validé qui est en cours
    public  static  int CODE_DEMANDE_ARCHIVAGE_DEMANDE = 102;
    // Un erreur est survenue lors de la création d'une demande
    public  static  int CODE_DEMANDE_AJOUT_KO = 103;

    // UTILISATEUR
    // L'utilisateur n'existe pas dans la BD
    public static int CODE_UTILISATEUR_INEXISTANT = 201;
    // L'utilisateur n'existe pas ou les identifiants ne sont pas correctes
    public  static int CODE_UTILISATEUR_CONNEXION = 202;
    // Création d'un utilisateur, le numéro de téléphone n'est pas au bon format
    public static int CODE_UTILISATEUR_CREATION_TELEPHONE = 203;
    // Création d'un utilisateur, le mail n'est pas au bon format
    public static int CODE_UTILISATEUR_CREATION_MAIL = 204;
    // création d'un utilisateur, un utilisateur avec le même mail et le même organisme existe déjà
    public static int CODE_UTILISATEUR_CREATION_EXISTE = 205;
    // Les identifiants de l'utilisateur sont bon, mais il est archivé
    public static int CODE_UTILISATEUR_CONNEXION_ARCHIVE = 206;
    // L'utilisateur connecté n'est pas un admin, il n'a donc pas le droit de créer un nouvel utilisateur
    public static  int CODE_UTILISATEUR_CREATION_ADMIN = 207;
    // L'utilisateur connecté n'a pas le droit d'accéder à ces données
    public static  int CODE_UTILISATEUR_DROIT_ACCES = 208;
    // L'utilisateur n'a pas le droit de modifier ces modifications
    public static  int CODE_UTILISATEUR_DROIT_MODIFICATION = 209;

    // SITE
    // Le site n'existe pas
    public static int CODE_SITE_INEXISTANT = 301;
    // Le site contient des véhicules et ne peut pas être supprimé
    public static int CODE_SITE_VEHICULE_EXISTANT = 302;
    // L'utilisateur connecté n'est pas un admin, il n'a donc pas le droit de créer un nouveau site
    public static  int CODE_SITE_CREATION_ADMIN = 207;
    // L'utilisateur connecté n'a pas le droit d'acceder à ces infos
    public static  int CODE_SITE_DROIT_ACCES = 208;

    // VEHICULE
    // Le vehicule n'existe pas 
	public static int CODE_VEHICULE_INEXISTANT = 401;
    // Une erreur inconue est survenie
    public static int CODE_VEHICULE_ERREUR = 402;

	// TOKEN
    // L'utilisateur n'est pas connecté
    public static int CODE_TOKEN_NON_CONNECTE = 501;

    // ORGANISME
    // L'organisme n'existe pas
    public static int CODE_ORGANISME_INEXISTANT = 601;

    public static MessageErreur createMessageErreur(int code){
        return new MessageErreur(code);
    }

    public static MessageErreur creaMessageErreur(String message){
        return new MessageErreur(message);
    }

    private MessageErreur(int code){
        this.code = code;
        this.message = "";
    }

    private MessageErreur(String message){
        this.code = MessageErreur.CODE_KO;
        this.message = message;
    }

    private int code;

    private String message;

}
