package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.EqualsAndHashCode;


/**
 * The persistent class for the sit_site database table.
 * 
 */
@Entity
@Table(name="mar_marque")
@NamedQuery(name="Marque.findAll", query="SELECT m FROM Marque m")
public class Marque implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MAR_ID", unique=true, nullable=false)
	@EqualsAndHashCode.Include
	private Long marId;

	@Column(name="MAR_LIBELLE", nullable=false, length=100)
	private String marLibelle;

	public Marque() {
		
	}

	public Long getMarId() {
		return marId;
	}

	public void setMarId(Long marId) {
		this.marId = marId;
	}

	public String getMarLibelle() {
		return marLibelle;
	}

	public void setMarLibelle(String marLibelle) {
		this.marLibelle = marLibelle;
	}

	
}