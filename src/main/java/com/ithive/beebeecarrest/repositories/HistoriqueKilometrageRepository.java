package com.ithive.beebeecarrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ithive.beebeecarrest.model.HistoriqueKilometrage;

public interface HistoriqueKilometrageRepository extends JpaRepository<HistoriqueKilometrage, Long> {
	
}
