//package com.ithive.beebeecarrest.model;
//
//import java.io.Serializable;
//import javax.persistence.*;
//
//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
//import com.fasterxml.jackson.annotation.JsonIdentityReference;
//import com.fasterxml.jackson.annotation.JsonManagedReference;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;
//
//
///**
// * The persistent class for the phv_photovehicule database table.
// * 
// */
//@Entity
//@Table(name="phv_photovehicule")
//@NamedQuery(name="PhotoVehicule.findAll", query="SELECT p FROM PhotoVehicule p")
////@JsonIdentityInfo(
////		  generator = ObjectIdGenerators.PropertyGenerator.class, 
////		  property = "phvId")
//public class PhotoVehicule implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(name="PHV_ID", unique=true, nullable=false)
//	private int phvId;
//
//	@Column(length=400, name = "PHV_Photo", nullable=false)
//	private String PHV_Photo;
//
//	//bi-directional many-to-one association to VehVehicule
//	@ManyToOne
//	@JoinColumn(name="PHV_VEH_ID", nullable=false)
//	@JsonIdentityReference(alwaysAsId = true)
//	private Vehicule vehVehicule;
//
//	public PhotoVehicule() {
//	}
//
//	public int getPhvId() {
//		return this.phvId;
//	}
//
//	public void setPhvId(int phvId) {
//		this.phvId = phvId;
//	}
//
//	public String getPHV_Photo() {
//		return this.PHV_Photo;
//	}
//
//	public void setPHV_Photo(String PHV_Photo) {
//		this.PHV_Photo = PHV_Photo;
//	}
//
//	public Vehicule getVehVehicule() {
//		return this.vehVehicule;
//	}
//
//	public void setVehVehicule(Vehicule vehVehicule) {
//		this.vehVehicule = vehVehicule;
//	}
//
//}