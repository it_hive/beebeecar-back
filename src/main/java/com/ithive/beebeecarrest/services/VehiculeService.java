package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.exceptions.ModeleNotFoundException;
import com.ithive.beebeecarrest.exceptions.SiteNotFoundException;
import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.model.Vehicule;
import com.ithive.beebeecarrest.repositories.ModeleRepository;
import com.ithive.beebeecarrest.repositories.SiteRepository;
import com.ithive.beebeecarrest.repositories.StoredProcedureRepository;
import com.ithive.beebeecarrest.repositories.VehiculeRepository;
import com.ithive.beebeecarrest.resources.MessageErreur;
import com.ithive.beebeecarrest.resources.Vehicule.GetVehiculesDispoResources;
import com.ithive.beebeecarrest.resources.VehiculeResources;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class VehiculeService {

	private static Logger logger = LogManager.getLogger(VehiculeService.class);

	public static List<VehiculeResources> BotoVos(List<Vehicule> listBO) {
		List<VehiculeResources> listVO = new LinkedList<VehiculeResources>();
		for (Vehicule bo : listBO) {
			listVO.add(BOToVO(bo));
		}
		return listVO;
	}

	public static Vehicule VOToBO(
			VehiculeResources vo, 
			SiteRepository siteRepo, 
			ModeleRepository modeleRepository) {
		Vehicule bo = new Vehicule();
		try {
			bo.setVehId(vo.getId_vehicule());
			bo.setVEH_DateFinService(DateUtilities.convertStringToUtilDate(vo.getDateFinService()));
			bo.setVEH_DateMiseEnService(DateUtilities.convertStringToUtilDate(vo.getDateMiseEnService()));
			bo.setVEH_Immatriculation(vo.getImmatriculation());
			bo.setVEH_Kilometrage(vo.getKilometrage());
			bo.setModModele(modeleRepository.findById(vo.getIdModele())
					.orElseThrow(() -> new ModeleNotFoundException(vo.getIdModele())));
	//		bo.setVEH_Marque(vo.getMarque());
	//		bo.setVEH_Modele(vo.getModele());
			bo.setSitSite(siteRepo.findById(vo.getIdSite())
					.orElseThrow(() -> new SiteNotFoundException(vo.getIdSite())));
			bo.setVEH_Details(vo.getDetails());
			bo.setVEH_PhotoUrl(vo.getPhoto());
			bo.setVEH_Couleur(vo.getCouleur());
	//		bo.setPhvPhotovehicules(phvPhotovehicules);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bo;
	}
	
	public static VehiculeResources BOToVO(Vehicule bo) {
		VehiculeResources vo = new VehiculeResources();

		vo.setId_vehicule(bo.getVehId());
		vo.setDateFinService(DateUtilities.convertUtilDateToString(bo.getVEH_DateFinService()));
		vo.setDateMiseEnService(DateUtilities.convertUtilDateToString(bo.getVEH_DateMiseEnService()));
		vo.setImmatriculation(bo.getVEH_Immatriculation());
		vo.setKilometrage(bo.getVEH_Kilometrage());
		vo.setIdMarque(bo.getModModele().getMarMarque().getMarId());
		vo.setMarque(bo.getModModele().getMarMarque().getMarLibelle());
		vo.setIdModele(bo.getModModele().getModId());
		vo.setModele(bo.getModModele().getModLibelle());
		vo.setIdSite(bo.getSitSite().getSitId());
		vo.setNomSite(bo.getSitSite().getSIT_Nom());
		vo.setDetails(bo.getVEH_Details());
		vo.setPhoto(bo.getVEH_PhotoUrl());
		vo.setCouleur(bo.getVEH_Couleur());
//		vo.setPhotos(photos);

		return vo;
	}

//	private static Vehicule VoToBO (VehiculeResources vehiculeRessource, Vehicule bo, SiteRepository siteRepository){
//		bo.setVEH_Immatriculation(vehiculeRessource.getImmatriculation());
//		bo.setVEH_DateFinService(vehiculeRessource.getDateFinService());
//		bo.setVEH_DateMiseEnService(vehiculeRessource.getDateMiseEnService());
//		bo.setVEH_Kilometrage(vehiculeRessource.getKilometrage());
////		bo.setVEH_Marque(vehiculeRessource.getMarque());
////		bo.setVEH_Modele(vehiculeRessource.getModele());
//		bo.setSitSite(siteRepository.findById(vehiculeRessource.getIdSite())
//				.orElseThrow(() -> new SiteNotFoundException(vehiculeRessource.getIdSite())));
//
//        return bo;
//    }
	
	public static VehiculeResources createBO(
			VehiculeRepository vehiculeRepo, 
			SiteRepository siteRepo, 
			ModeleRepository modeleRepository,
			VehiculeResources vo) {

		logger.debug("création véhicule");
		Vehicule bo = VOToBO(vo, siteRepo, modeleRepository);
//		Vehicule bo = new Vehicule();
		// récupération du site

//		Long idSite = vo.getIdSite().longValue();
//		Site site = siteRepo.findById(idSite).get();
//		logger.info(" site trouvé :" + site.getSIT_Nom() + "(" + site.getSitId() + ")");
//
//		bo.setSitSite(site);
//		bo.setModModele(modeleRepository.findById(vo.getIdModele())
//				.orElseThrow(() -> new ModeleNotFoundException(vo.getIdModele())));
//		bo.setVEH_Immatriculation(vo.getImmatriculation());
//		bo.setVEH_DateMiseEnService(vo.getDateMiseEnService());
//		bo.setVEH_Kilometrage(vo.getKilometrage());

		vehiculeRepo.save(bo);

		return BOToVO(bo);
	}

	
	
	public static VehiculeResources replaceVehicule(
			VehiculeRepository repository,
			SiteRepository siteRepository,
			VehiculeResources vehiculeRessource,
			ModeleRepository modeleRepository) {

		Optional<Vehicule> optional = repository.findById(vehiculeRessource.getId_vehicule());

		if (optional.isPresent()) {
			Vehicule bo = optional.get();
			bo = VOToBO(vehiculeRessource, siteRepository, modeleRepository);
			repository.save(bo);
		} else {
			vehiculeRessource = createBO(repository, siteRepository, modeleRepository, vehiculeRessource);
		}

		return vehiculeRessource;

	}

	// Fonction permettant de récupérer la liste des véhicules disponibles pour une période et un organisme donné
	public static Object GetVehiculesForPeriode(
			GetVehiculesDispoResources resources,
			Utilisateur utilisateur,
			StoredProcedureRepository storedProcedureRepository){

		try {

			Date debut = DateUtilities.convertStringToUtilDate(resources.getDateDebut());
			Date fin = DateUtilities.convertStringToUtilDate(resources.getDateFin());
			//Exécution de la procédure stockée récupérant les véhicules disponibles
			List<Vehicule> vehicules = storedProcedureRepository.getVehiculesDispoForPeriode(utilisateur.getOrgOrganisme().getOrgId(),debut,fin);
			vehicules = vehicules.stream()
					.filter((vehicule) -> vehicule.getSitSite().getSitId() == resources.getIdSite())
					.collect(Collectors.toList());

			// Renvoie des resources
			return BotoVos(vehicules);
		} catch (ParseException e) {
			logger.error(e);
			return MessageErreur.createMessageErreur(MessageErreur.CODE_VEHICULE_ERREUR);
		}


	}
}
