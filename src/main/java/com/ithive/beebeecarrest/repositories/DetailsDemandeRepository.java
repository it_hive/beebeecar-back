package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.DetailDemande;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DetailsDemandeRepository extends JpaRepository<DetailDemande, Long> {

    List<DetailDemande> getAllByDemDemandelocation(DemandeLocation demande);
}
