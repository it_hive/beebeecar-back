package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;


/**
 * The persistent class for the dtd_detaildemande database table.
 * 
 */
@Entity
@Table(name="dtd_detaildemande")
@NamedQuery(name="DetailDemande.findAll", query="SELECT d FROM DetailDemande d")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "dtdId")
public class DetailDemande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DTD_ID", unique=true, nullable=false)
	private int dtdId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date DTD_Date;

	//bi-directional many-to-one association to DemDemandelocation
	@ManyToOne
	@JoinColumn(name="DTD_DEM_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private DemandeLocation demDemandelocation;

	public DetailDemande() {
	}

	public int getDtdId() {
		return this.dtdId;
	}

	public void setDtdId(int dtdId) {
		this.dtdId = dtdId;
	}

	public Date getDTD_Date() {
		return this.DTD_Date;
	}

	public void setDTD_Date(Date DTD_Date) {
		this.DTD_Date = DTD_Date;
	}

	public DemandeLocation getDemDemandelocation() {
		return this.demDemandelocation;
	}

	public void setDemDemandelocation(DemandeLocation demDemandelocation) {
		this.demDemandelocation = demDemandelocation;
	}

}