package com.ithive.beebeecarrest.controllers;

import com.ithive.beebeecarrest.repositories.StoredProcedureRepository;
import com.ithive.beebeecarrest.repositories.UtilisateurRepository;
import com.ithive.beebeecarrest.resources.TokenRessources;
import com.ithive.beebeecarrest.resources.statistique.TauxOccupationInputResources;
import com.ithive.beebeecarrest.services.StatistiqueService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class StatistiquesController {


    private final UtilisateurRepository repositoryUtilisateur;
    private final StoredProcedureRepository spv;

    private static Logger logger = LogManager.getLogger(StatistiquesController.class);

    public StatistiquesController(
            UtilisateurRepository repositoryUtilisateur,
            StoredProcedureRepository spv
    ){
        this.repositoryUtilisateur = repositoryUtilisateur;
        this.spv = spv;
    }


    //Route permettant de récupérer le taux d'occupation des véhicules, mois par mois
    @PostMapping("/statistiques/vehicule/tauxoccupation/{userToken}")
    public Object GetTauxOccupationVehicules(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetTauxOccupationVehicule(resources, token, repositoryUtilisateur, spv);
    }

    // Route permettant de récupérer le taux d'occupation des sites, mois par mois
    @PostMapping("/statistiques/site/tauxoccupation/{userToken}")
    public Object GetTauxOccupationSite(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetTauxOccupationSite(resources, token, repositoryUtilisateur, spv);
    }

    // Route retournant l'état du parc
    @PostMapping("/statistiques/etatParc/{userToken}")
    public Object GetEtatParc(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetEtatParc(resources, token, spv, repositoryUtilisateur);
    }

    // Route retournant le nombre de réservation par site
    @PostMapping("/statistiques/site/nombreReservations/{userToken}")
    public Object GetNombreReservationSite(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetNombreReservationsParSiteParMois(resources, token, spv, repositoryUtilisateur);
    }

    // Route retournant le nombre de réservation par site et par état
    @PostMapping("/statistiques/site/nombreReservationsByEtat/{userToken}")
    public Object GetNombreReservationSiteAndEtat(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetNombreReservationsParSiteAndEtatParMois(resources, token, spv, repositoryUtilisateur);
    }


    @PostMapping("/statistiques/utilisateurs/nombreReservations/{userToken}")
    public Object GetNombreUtilisateurActifsMoisParMois(@RequestBody TauxOccupationInputResources resources, @PathVariable String userToken){

        TokenRessources token = new TokenRessources();
        token.setToken(userToken);

        return StatistiqueService.GetNombreUtilisateurActif(resources, token, spv, repositoryUtilisateur);
    }
}
