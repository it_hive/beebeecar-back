package com.ithive.beebeecarrest.exceptions;

public class ModeleNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ModeleNotFoundException(Long id) {
		super("Impossible de trouver le modele #" + id);
	}
}
