package com.ithive.beebeecarrest.resources;

import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;

@Data
public class ActiviteVehiculeOutputResources {

	private VehiculeResources vehicule;

	private DemandeLocationResources demandeLocation;

	private UtilisateurResources utilisateur;

	private String dtdDate;

	private String edmLibelle;
	
}
