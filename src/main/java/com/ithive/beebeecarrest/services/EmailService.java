package com.ithive.beebeecarrest.services;

import com.ithive.beebeecarrest.model.Utilisateur;
import com.ithive.beebeecarrest.resources.DemandeLocationResources;
import com.ithive.beebeecarrest.resources.EmailResources;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import com.ithive.beebeecarrest.resources.demandeLocation.NouvelleDemandeLocationResources;
import com.sun.mail.smtp.SMTPTransport;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class EmailService {

	@Autowired
	static
    ResourceLoader resourceLoader;
	
	private static final String MAJ_STATUS = "<!doctype html>\r\n" + 
			"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\r\n" + 
			"\r\n" + 
			"<head>\r\n" + 
			"    <title> BeeBeeCar </title>\r\n" + 
			"    <!--[if !mso]><!-- -->\r\n" + 
			"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n" + 
			"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        #outlook a {\r\n" + 
			"            padding: 0;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ReadMsgBody {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass * {\r\n" + 
			"            line-height: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        body {\r\n" + 
			"            margin: 0;\r\n" + 
			"            padding: 0;\r\n" + 
			"            -webkit-text-size-adjust: 100%;\r\n" + 
			"            -ms-text-size-adjust: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        table,\r\n" + 
			"        td {\r\n" + 
			"            border-collapse: collapse;\r\n" + 
			"            mso-table-lspace: 0pt;\r\n" + 
			"            mso-table-rspace: 0pt;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        img {\r\n" + 
			"            border: 0;\r\n" + 
			"            height: auto;\r\n" + 
			"            line-height: 100%;\r\n" + 
			"            outline: none;\r\n" + 
			"            text-decoration: none;\r\n" + 
			"            -ms-interpolation-mode: bicubic;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        p {\r\n" + 
			"            display: block;\r\n" + 
			"            margin: 13px 0;\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            @-ms-viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"            @viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <!--[if mso]>\r\n" + 
			"    <xml>\r\n" + 
			"        <o:OfficeDocumentSettings>\r\n" + 
			"            <o:AllowPNG/>\r\n" + 
			"            <o:PixelsPerInch>96</o:PixelsPerInch>\r\n" + 
			"        </o:OfficeDocumentSettings>\r\n" + 
			"    </xml>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if lte mso 11]>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        .outlook-group-fix { width:100% !important; }\r\n" + 
			"    </style>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500\" rel=\"stylesheet\" type=\"text/css\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @import url(https://fonts.googleapis.com/css?family=Roboto:300,500);\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (min-width:480px) {\r\n" + 
			"            .mj-column-per-100 {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"                max-width: 100%;\r\n" + 
			"            }\r\n" + 
			"            .mj-column-per-50 {\r\n" + 
			"                width: 50% !important;\r\n" + 
			"                max-width: 50%;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            table.full-width-mobile {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"            }\r\n" + 
			"            td.full-width-mobile {\r\n" + 
			"                width: auto !important;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"</head>\r\n" + 
			"\r\n" + 
			"<body>\r\n" + 
			"<div style=\"\">\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:20px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:collapse;border-spacing:0px;\">\r\n" + 
			"                                        <tbody>\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"width:200px;\"> <a href=\"https://beebeecar.tk\" target=\"_blank\">\r\n" + 
			"\r\n" + 
			"                                                <img\r\n" + 
			"                                                        height=\"auto\" src=\"https://beebeecar.tk/LogoNoir_email.png\" style=\"border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;\" width=\"200\"\r\n" + 
			"                                                />\r\n" + 
			"\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                        </tbody>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:300px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-50 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <p style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:100%;\"> </p>\r\n" + 
			"                                    <!--[if mso | IE]>\r\n" + 
			"                                    <table\r\n" + 
			"                                            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:250px;\" role=\"presentation\" width=\"250px\"\r\n" + 
			"                                    >\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"height:0;line-height:0;\">\r\n" + 
			"                                                &nbsp;\r\n" + 
			"                                            </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                    <![endif]-->\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:24px;font-weight:500;line-height:24px;text-align:center;color:#FFC038;\"> #status_demande de votre demande sur BeeBeeCar </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:10px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Votre demande effectué sur BeeBeeCar le #date_demande vient d'être #status_demande_v par un administrateur. Retrouvez ci-dessous le détail de votre demande. </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de la demande :</strong> #date_demande </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de début :</strong> #date_debut </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de fin :</strong> #date_fin </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Site de départ :</strong> #site_depart </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"<!--                             <tr> -->\r\n" + 
			"<!--                                 <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\"> -->\r\n" + 
			"<!--                                     <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Véhicule :</strong> #véhicule </div> -->\r\n" + 
			"<!--                                 </td> -->\r\n" + 
			"<!--                             </tr> -->\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Autorise le trajet au covoiturage :</strong> #covoit </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" vertical-align=\"middle\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:separate;line-height:100%;\">\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td align=\"center\" bgcolor=\"#FFC038\" role=\"presentation\" style=\"border:none;border-radius:3px;cursor:auto;padding:10px 25px;text-align:center;background:#FFC038;\" valign=\"middle\"> <a href=\"https://beebeecar.tk\" style=\"background:#FFC038;color:white;font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;\" target=\"_blank\">\r\n" + 
			"                                                Accéder à mon espace personnel\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:14px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Message envoyé automatiquement depuis <a href=\"https://beebeecar.tk\">BeeBeeCar.tk</a> </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"    <![endif]-->\r\n" + 
			"</div>\r\n" + 
			"</body>\r\n" + 
			"\r\n" + 
			"</html>";
	
	private static final String NEW_DEMANDE = "<!doctype html>\r\n" + 
			"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\r\n" + 
			"\r\n" + 
			"<head>\r\n" + 
			"    <title> BeeBeeCar </title>\r\n" + 
			"    <!--[if !mso]><!-- -->\r\n" + 
			"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n" + 
			"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        #outlook a {\r\n" + 
			"            padding: 0;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ReadMsgBody {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass * {\r\n" + 
			"            line-height: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        body {\r\n" + 
			"            margin: 0;\r\n" + 
			"            padding: 0;\r\n" + 
			"            -webkit-text-size-adjust: 100%;\r\n" + 
			"            -ms-text-size-adjust: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        table,\r\n" + 
			"        td {\r\n" + 
			"            border-collapse: collapse;\r\n" + 
			"            mso-table-lspace: 0pt;\r\n" + 
			"            mso-table-rspace: 0pt;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        img {\r\n" + 
			"            border: 0;\r\n" + 
			"            height: auto;\r\n" + 
			"            line-height: 100%;\r\n" + 
			"            outline: none;\r\n" + 
			"            text-decoration: none;\r\n" + 
			"            -ms-interpolation-mode: bicubic;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        p {\r\n" + 
			"            display: block;\r\n" + 
			"            margin: 13px 0;\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            @-ms-viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"            @viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <!--[if mso]>\r\n" + 
			"    <xml>\r\n" + 
			"        <o:OfficeDocumentSettings>\r\n" + 
			"            <o:AllowPNG/>\r\n" + 
			"            <o:PixelsPerInch>96</o:PixelsPerInch>\r\n" + 
			"        </o:OfficeDocumentSettings>\r\n" + 
			"    </xml>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if lte mso 11]>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        .outlook-group-fix { width:100% !important; }\r\n" + 
			"    </style>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500\" rel=\"stylesheet\" type=\"text/css\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @import url(https://fonts.googleapis.com/css?family=Roboto:300,500);\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (min-width:480px) {\r\n" + 
			"            .mj-column-per-100 {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"                max-width: 100%;\r\n" + 
			"            }\r\n" + 
			"            .mj-column-per-50 {\r\n" + 
			"                width: 50% !important;\r\n" + 
			"                max-width: 50%;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            table.full-width-mobile {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"            }\r\n" + 
			"            td.full-width-mobile {\r\n" + 
			"                width: auto !important;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"</head>\r\n" + 
			"\r\n" + 
			"<body>\r\n" + 
			"<div style=\"\">\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:20px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:collapse;border-spacing:0px;\">\r\n" + 
			"                                        <tbody>\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"width:200px;\"> <a href=\"https://beebeecar.tk\" target=\"_blank\">\r\n" + 
			"\r\n" + 
			"                                                <img\r\n" + 
			"                                                        height=\"auto\" src=\"https://beebeecar.tk/LogoNoir_email.png\" style=\"border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;\" width=\"200\"\r\n" + 
			"                                                />\r\n" + 
			"\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                        </tbody>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:300px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-50 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <p style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:100%;\"> </p>\r\n" + 
			"                                    <!--[if mso | IE]>\r\n" + 
			"                                    <table\r\n" + 
			"                                            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:250px;\" role=\"presentation\" width=\"250px\"\r\n" + 
			"                                    >\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"height:0;line-height:0;\">\r\n" + 
			"                                                &nbsp;\r\n" + 
			"                                            </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                    <![endif]-->\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:24px;font-weight:500;line-height:24px;text-align:center;color:#FFC038;\"> Votre demande sur BeeBeeCar </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:10px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Votre demande sur BeeBeeCar a bien été prise en compte, retrouvez ci-dessous le détail de votre demande. </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de la demande :</strong> #date_demande </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de début :</strong> #date_debut </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Date de fin :</strong> #date_fin </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Site de départ :</strong> #site_depart </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"<!--                             <tr> -->\r\n" + 
			"<!--                                 <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\"> -->\r\n" + 
			"<!--                                     <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Véhicule :</strong> #véhicule </div> -->\r\n" + 
			"<!--                                 </td> -->\r\n" + 
			"<!--                             </tr> -->\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Proposer le trajet au covoiturage :</strong> #covoit </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" vertical-align=\"middle\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:separate;line-height:100%;\">\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td align=\"center\" bgcolor=\"#FFC038\" role=\"presentation\" style=\"border:none;border-radius:3px;cursor:auto;padding:10px 25px;text-align:center;background:#FFC038;\" valign=\"middle\"> <a href=\"https://beebeecar.tk\" style=\"background:#FFC038;color:white;font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;\" target=\"_blank\">\r\n" + 
			"                                                Accéder à mon espace personnel\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:14px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Message envoyé automatiquement depuis <a href=\"https://beebeecar.tk\">BeeBeeCar.tk</a> </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"    <![endif]-->\r\n" + 
			"</div>\r\n" + 
			"</body>\r\n" + 
			"\r\n" + 
			"</html>";
	
	private static final String USER = "<!doctype html>\r\n" + 
			"<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\r\n" + 
			"\r\n" + 
			"<head>\r\n" + 
			"    <title> BeeBeeCar </title>\r\n" + 
			"    <!--[if !mso]><!-- -->\r\n" + 
			"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n" + 
			"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        #outlook a {\r\n" + 
			"            padding: 0;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ReadMsgBody {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass {\r\n" + 
			"            width: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        .ExternalClass * {\r\n" + 
			"            line-height: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        body {\r\n" + 
			"            margin: 0;\r\n" + 
			"            padding: 0;\r\n" + 
			"            -webkit-text-size-adjust: 100%;\r\n" + 
			"            -ms-text-size-adjust: 100%;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        table,\r\n" + 
			"        td {\r\n" + 
			"            border-collapse: collapse;\r\n" + 
			"            mso-table-lspace: 0pt;\r\n" + 
			"            mso-table-rspace: 0pt;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        img {\r\n" + 
			"            border: 0;\r\n" + 
			"            height: auto;\r\n" + 
			"            line-height: 100%;\r\n" + 
			"            outline: none;\r\n" + 
			"            text-decoration: none;\r\n" + 
			"            -ms-interpolation-mode: bicubic;\r\n" + 
			"        }\r\n" + 
			"\r\n" + 
			"        p {\r\n" + 
			"            display: block;\r\n" + 
			"            margin: 13px 0;\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            @-ms-viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"            @viewport {\r\n" + 
			"                width: 320px;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <!--[if mso]>\r\n" + 
			"    <xml>\r\n" + 
			"        <o:OfficeDocumentSettings>\r\n" + 
			"            <o:AllowPNG/>\r\n" + 
			"            <o:PixelsPerInch>96</o:PixelsPerInch>\r\n" + 
			"        </o:OfficeDocumentSettings>\r\n" + 
			"    </xml>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if lte mso 11]>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        .outlook-group-fix { width:100% !important; }\r\n" + 
			"    </style>\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <!--[if !mso]><!-->\r\n" + 
			"    <link href=\"https://fonts.googleapis.com/css?family=Roboto:300,500\" rel=\"stylesheet\" type=\"text/css\">\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @import url(https://fonts.googleapis.com/css?family=Roboto:300,500);\r\n" + 
			"    </style>\r\n" + 
			"    <!--<![endif]-->\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (min-width:480px) {\r\n" + 
			"            .mj-column-per-100 {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"                max-width: 100%;\r\n" + 
			"            }\r\n" + 
			"            .mj-column-per-50 {\r\n" + 
			"                width: 50% !important;\r\n" + 
			"                max-width: 50%;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"    <style type=\"text/css\">\r\n" + 
			"        @media only screen and (max-width:480px) {\r\n" + 
			"            table.full-width-mobile {\r\n" + 
			"                width: 100% !important;\r\n" + 
			"            }\r\n" + 
			"            td.full-width-mobile {\r\n" + 
			"                width: auto !important;\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    </style>\r\n" + 
			"</head>\r\n" + 
			"\r\n" + 
			"<body>\r\n" + 
			"<div style=\"\">\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:20px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:collapse;border-spacing:0px;\">\r\n" + 
			"                                        <tbody>\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"width:200px;\"> <a href=\"https://beebeecar.tk\" target=\"_blank\">\r\n" + 
			"\r\n" + 
			"                                                <img\r\n" + 
			"                                                        height=\"auto\" src=\"https://beebeecar.tk/LogoNoir_email.png\" style=\"border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;\" width=\"200\"\r\n" + 
			"                                                />\r\n" + 
			"\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                        </tbody>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:300px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-50 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <p style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:100%;\"> </p>\r\n" + 
			"                                    <!--[if mso | IE]>\r\n" + 
			"                                    <table\r\n" + 
			"                                            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-top:solid 2px #212121;font-size:1;margin:0px auto;width:250px;\" role=\"presentation\" width=\"250px\"\r\n" + 
			"                                    >\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td style=\"height:0;line-height:0;\">\r\n" + 
			"                                                &nbsp;\r\n" + 
			"                                            </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                    <![endif]-->\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:24px;font-weight:500;line-height:24px;text-align:center;color:#FFC038;\"> Création de votre compte sur BeeBeeCar </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:10px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Votre compte BeeBeeCar vient d'être crée, retrouvez ci-dessous vos informations de connexion. </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Identifiant :</strong> #email_utilisateur </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> <strong>Mot de passe :</strong> Il vous sera transmis par votre administrateur </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:30px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" vertical-align=\"middle\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:separate;line-height:100%;\">\r\n" + 
			"                                        <tr>\r\n" + 
			"                                            <td align=\"center\" bgcolor=\"#FFC038\" role=\"presentation\" style=\"border:none;border-radius:3px;cursor:auto;padding:10px 25px;text-align:center;background:#FFC038;\" valign=\"middle\"> <a href=\"https://beebeecar.tk\" style=\"background:#FFC038;color:white;font-family:Roboto, Helvetica, sans-serif;font-size:18px;font-weight:normal;line-height:120%;Margin:0;text-decoration:none;text-transform:none;\" target=\"_blank\">\r\n" + 
			"                                                Accéder à mon espace personnel\r\n" + 
			"                                            </a> </td>\r\n" + 
			"                                        </tr>\r\n" + 
			"                                    </table>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"\r\n" + 
			"    <table\r\n" + 
			"            align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"\" style=\"width:600px;\" width=\"600\"\r\n" + 
			"    >\r\n" + 
			"        <tr>\r\n" + 
			"            <td style=\"line-height:0px;font-size:0px;mso-line-height-rule:exactly;\">\r\n" + 
			"    <![endif]-->\r\n" + 
			"    <div style=\"Margin:0px auto;max-width:600px;\">\r\n" + 
			"        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\r\n" + 
			"            <tbody>\r\n" + 
			"            <tr>\r\n" + 
			"                <td style=\"direction:ltr;font-size:0px;padding:0px;padding-top:0px;text-align:center;vertical-align:top;\">\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    <table role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n" + 
			"\r\n" + 
			"                        <tr>\r\n" + 
			"\r\n" + 
			"                            <td\r\n" + 
			"                                    class=\"\" style=\"vertical-align:top;width:600px;\"\r\n" + 
			"                            >\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                    <div class=\"mj-column-per-100 outlook-group-fix\" style=\"font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\r\n" + 
			"                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"vertical-align:top;\" width=\"100%\">\r\n" + 
			"                            <tr>\r\n" + 
			"                                <td align=\"center\" style=\"font-size:0px;padding:10px 25px;word-break:break-word;\">\r\n" + 
			"                                    <div style=\"font-family:Roboto, Helvetica, sans-serif;font-size:14px;font-weight:300;line-height:24px;text-align:center;color:#212121;\"> Message envoyé automatiquement depuis <a href=\"https://beebeecar.tk\">BeeBeeCar.tk</a> </div>\r\n" + 
			"                                </td>\r\n" + 
			"                            </tr>\r\n" + 
			"                        </table>\r\n" + 
			"                    </div>\r\n" + 
			"                    <!--[if mso | IE]>\r\n" + 
			"                    </td>\r\n" + 
			"\r\n" + 
			"                    </tr>\r\n" + 
			"\r\n" + 
			"                    </table>\r\n" + 
			"                    <![endif]-->\r\n" + 
			"                </td>\r\n" + 
			"            </tr>\r\n" + 
			"            </tbody>\r\n" + 
			"        </table>\r\n" + 
			"    </div>\r\n" + 
			"    <!--[if mso | IE]>\r\n" + 
			"    </td>\r\n" + 
			"    </tr>\r\n" + 
			"    </table>\r\n" + 
			"    <![endif]-->\r\n" + 
			"</div>\r\n" + 
			"</body>\r\n" + 
			"\r\n" + 
			"</html>";
	
	private static final String SMTP_SERVER = "smtp.gmail.com";
	private static final String USERNAME = "beebeecar.sas@gmail.com";
	private static final String PASSWORD = "ITHive2019!";

	private static final String EMAIL_FROM = "beebeecar.sas@gmail.com";

	private static final String EMAIL_UTI_TEXT = "Bonjour #nompre, <br/>"
			+ "<p>Voici les identifiants de votre compte : <br/>" + "<ul>" + "<li>Identifiant : <b> #email </b></li>"
//    		+ "<li>Mot de Passe : <b> #pass </b></li>"
			+ "</ul></p>";

	private static final String EMAIL_DEMLOC_TEXT = "<p>Bonjour #nompre, <br/>"
			+ "Votre demande de location pour le #date a été <b>#valid</b> !</p>";

	public static void send(EmailResources email) {

		Integer cpt = 0;
		Integer cptCC = 0;
		String listMail = "";
		String listMailCC = "";

		for (String to : email.getMailTo()) {
			cpt++;
			if (cpt != email.getMailTo().size()) {
				listMail += to + ", ";
			} else {
				listMail += to;
			}
		}

		for (String toCC : email.getMailToCC()) {
			cptCC++;
			if (cpt != email.getMailToCC().size()) {
				listMailCC += toCC + ", ";
			} else {
				listMailCC += toCC;
			}
		}

		Properties prop = System.getProperties();
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		// prop.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getInstance(prop, null);
		Message msg = new MimeMessage(session);

		try {

			msg.setFrom(new InternetAddress(email.getMailFrom()));

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(listMail, false));

			msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(listMailCC, false));

			msg.setSubject(email.getMailSubject());

			// TEXT email
			msg.setText(email.getMailBody());

			// HTML email
			msg.setDataHandler(new DataHandler(new HTMLDataSource(email.getMailBody())));

			SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

			// Connection
			t.connect(SMTP_SERVER, 465, USERNAME, PASSWORD); // 465 / 587

			// Envoi du mail
			t.sendMessage(msg, msg.getAllRecipients());

			System.out.println("Response: " + t.getLastServerResponse());

			t.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	public static EmailResources createBody(UtilisateurResources user, NouvelleDemandeLocationResources demLoc,
			List<Utilisateur> admins, boolean isStatut) {

		// Initialisation de variable temporaire
		List<String> mailTo = new ArrayList<String>();
		List<String> mailToCC = new ArrayList<String>();

		String mailBody = "";

		// Initialisation de l'Email
		EmailResources newEmail = new EmailResources();
		newEmail.setMailFrom(EMAIL_FROM);

		// Construction de l'eMail
		if (demLoc == null) {
			
			mailBody = readTemplate("U");
			// Initialisation des valeurs
			mailTo.add(user.getMail());
//    		mailTo.add("jo.leclerc@hotmail.fr");
			newEmail.setMailSubject("BeebeeCar - Modification du compte utilisateur");
			newEmail.setMailTo(mailTo);
			if (admins != null) {
				for (Utilisateur uti : admins) {
					System.out.println(uti.getmail());
					mailToCC.add(uti.getmail());
				}
			}
//			mailBody = EMAIL_UTI_TEXT;

//			mailBody = mailBody.replace("#nompre", user.getPrenom() + " " + user.getNom());
//    		mailBody = mailBody.replace("#nompre", "Jonathan LECLERC");
			mailBody = mailBody.replace("#email_utilisateur", user.getMail());
			// mailBody.replace("#pass", user.get);

			// Récupérer le template création utilisateur

		} else {
			
			// Initialisation des valeurs
			mailTo.add(user.getMail());
//    		mailTo.add("jo.leclerc@hotmail.fr");
			
			newEmail.setMailTo(mailTo);
			if (admins != null) {
				for (Utilisateur uti : admins) {
					System.out.println(uti.getmail());
					mailToCC.add(uti.getmail());
				}
			}

			
			if (isStatut) {
				newEmail.setMailSubject("BeebeeCar - Statut de votre demande");
				mailBody = readTemplate("S");
				
				String validation_n = "";
				String validation_v = "";
				if (demLoc.getEtatDemande().equals("Valide")) {
					validation_n = "Validation";
					validation_v = "validée";
				} else if (demLoc.getEtatDemande().equals("Refuse")) {
					validation_v = "refusée";
					validation_n = "Refus";
				}
				mailBody = mailBody.replace("#status_demande_v", validation_v);
				mailBody = mailBody.replace("#status_demande", validation_n);
				 
			}else {
				newEmail.setMailSubject("BeebeeCar - Confirmation de votre demande");
				// Récupérer le template confirmation mail
				mailBody = readTemplate("N");

			}
			// Remplacement des placeholders
			mailBody = mailBody.replace("#nompre", user.getPrenom() + " " + user.getNom());
			mailBody = mailBody.replace("#date_demande", demLoc.getDateDemande());
			mailBody = mailBody.replace("#date_debut", demLoc.getDateDepart());
			mailBody = mailBody.replace("#date_fin", demLoc.getDateArrivee());
			mailBody = mailBody.replace("#site_depart", demLoc.getNomSite());
//			mailBody = mailBody.replace("#véhicule", demLoc.getVehicule().getMarque() + " " + demLoc.getVehicule().getModele() + " - " + demLoc.getVehicule().getImmatriculation());
			
			String sAllowCovoit = "";
			int iAllowCovoit = demLoc.getAllowCovoiturage();
			
			if (iAllowCovoit == 1) {
				sAllowCovoit = "Oui";
			}else {
				sAllowCovoit = "Non";
			}
			mailBody = mailBody.replace("#covoit", sAllowCovoit);
			
			

		}
		newEmail.setMailBody(mailBody);
		newEmail.setMailToCC(mailToCC);

		return newEmail;
	}

	static class HTMLDataSource implements DataSource {

		private String html;

		public HTMLDataSource(String htmlString) {
			html = htmlString;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			if (html == null)
				throw new IOException("html message is null!");
			return new ByteArrayInputStream(html.getBytes());
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			throw new IOException("This DataHandler cannot write HTML");
		}

		@Override
		public String getContentType() {
			return "text/html";
		}

		@Override
		public String getName() {
			return "HTMLDataSource";
		}
	}

	private static String readTemplate(String type) {

		String locFile = "";
		String template = "";
		String line = "";

		if (type.equals("U")) {
			locFile += "user.html";
			template = USER;
		} else if (type.equals("N")) {
			locFile += "new_demande.html";
			template = NEW_DEMANDE;
		} else if (type.equals("S")) {
			locFile += "maj_status.html";
			template = MAJ_STATUS;
		}
		
/*
		try {
			
			File file = new ClassPathResource(locFile).getFile();
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while ( (line = reader.readLine()) != null) {
				template += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(template);
*/
		return template;
	}

}