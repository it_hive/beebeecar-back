package com.ithive.beebeecarrest.securities;

import com.auth0.jwt.JWT;
import com.ithive.beebeecarrest.resources.UtilisateurResources;
import org.apache.catalina.User;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class TokenRegister {

    private static TokenRegister instance = null;

    private Map<String, UserToken> mapToken;

    private TokenRegister(){

        this.mapToken = new HashMap<>();
    }

    public static TokenRegister getInstance(){
        if( instance == null){
            instance = new TokenRegister();
        }
        return instance;
    }

    /**
     * vérifie que l'utilisateur est connecté avec un token et
     *  que sa date d'expiration n'est pas passé
     * @param token
     * @return Utilisateur connecté ou null
     */
    public UtilisateurResources verification(String token){

        UserToken user = this.mapToken.get(token);
        if(user == null){
            // le token n'est pas connu
            return null;
        }

        Date dateNow = new Date(System.currentTimeMillis());

        if(dateNow.after(user.getDateLimite())){
            // le token est périmé
            //on supprime le token
            this.mapToken.remove(token);
            return null;
        }

        return user.getUtilisateurResources();
    }

    /**
     * Ajoute un nouveau token
     * @param utilisateurResources
     * @return token
     */
    public String addToken(UtilisateurResources utilisateurResources){

        UserToken newUserToken = null;
        String token = "";

        // vérification si l'utilisateur n'est pas déjà enregistré
        for(Map.Entry<String, UserToken> entry : this.mapToken.entrySet()){
            if(entry.getValue().getUtilisateurResources().getIdUtilisateur() == utilisateurResources.getIdUtilisateur()){
                newUserToken = entry.getValue();
                token = entry.getKey();
                break;
            }
        }

        if(newUserToken != null){
            // si l'utilisateur existe déjà, on le supprime
            this.mapToken.remove(token);
        }

        // création de l'utilisateur token
        newUserToken = new UserToken(new Date(System.currentTimeMillis() + TokenConstantes.SECONDES), utilisateurResources);

        // création du token
        token = JWT.create()
                .withSubject(utilisateurResources.getMail() + "_" + utilisateurResources.getNomOrganisme())
                .withExpiresAt(new Date(System.currentTimeMillis() + TokenConstantes.SECONDES))
                .sign(HMAC512(TokenConstantes.SECRET.getBytes()));

        // ajout de l'utilisateur dans la map des tokens
        this.mapToken.put(token, newUserToken);

        return token;

    }

    public void removeToken(String token){
        this.mapToken.remove(token);
    }


}
