package com.ithive.beebeecarrest.resources.demandeLocation;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.Etape;
import com.ithive.beebeecarrest.resources.VehiculeResources;
import com.ithive.beebeecarrest.services.VehiculeService;
import com.ithive.beebeecarrest.utilities.DateUtilities;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class CovoiturageDisposResources {

    private Long demId;

    private byte allowCovoiturage;

    private String dateDebut;

    private String dateDemande;

    private String dateFin;

    private Long etatId;

    private String etatDemande;

    private VehiculeResources vehicule;

    private List<EtapeResources> etapes;

    private String nomUtilisateur;

    private String prenomUtilisateur;

    private Long idUtilisateur;

    private String mailUtilisateur;

    private String numeroTelephone;

    private String nomSite;

    public CovoiturageDisposResources() {
        super();
    }

    // Constructeur permettant de construire la ressource à partir du business object DemandeLocation
    public CovoiturageDisposResources(DemandeLocation demande) {
        setAllowCovoiturage(demande.getDEM_AllowCovoiturage());
        setDateDebut(DateUtilities.convertUtilDateToString(demande.getDemDatedebut()));
        setDateFin(DateUtilities.convertUtilDateToString(demande.getDemDatefin()));
        setDateDemande(DateUtilities.convertUtilDateToString(demande.getDemDateDemande()));
        setDemId(demande.getDemId());
        setEtatDemande(demande.getEdmEtatdemande().getEDM_Libelle());
        setEtatId((long) demande.getEdmEtatdemande().getEdmId());
        if(demande.getVehVehicule() != null){
            setVehicule(VehiculeService.BOToVO(demande.getVehVehicule()));
        }
        setIdUtilisateur(demande.getUtiUtilisateur().getUtiId());
        setNomUtilisateur(demande.getUtiUtilisateur().getUTI_Nom());
        setPrenomUtilisateur(demande.getUtiUtilisateur().getUTI_Prenom());
        setMailUtilisateur(demande.getUtiUtilisateur().getmail());
        setNumeroTelephone(demande.getUtiUtilisateur().getUTI_Telephone());
        setNomSite(demande.getSitSite().getSIT_Nom());
        //Ajout des etapes
        List<EtapeResources> etapes = new ArrayList<>();
        for(Etape etape : demande.getEtpEtape().stream().collect(Collectors.toList())){
            etapes.add(new EtapeResources(etape));
        }
        setEtapes(etapes);
    }
}
