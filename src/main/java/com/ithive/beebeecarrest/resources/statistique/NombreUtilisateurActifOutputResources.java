package com.ithive.beebeecarrest.resources.statistique;

import com.ithive.beebeecarrest.model.sp.NombreUtilisateurActif;
import lombok.Data;

@Data
public class NombreUtilisateurActifOutputResources {

    private Integer utilisateurActifsN;

    private Integer utilisateurActifsN1;

    private Integer utilisateurActifsN2;

    private Integer utilisateurActifsN3;

    private Integer utilisateurActifsN4;

    private Integer utilisateurActifsN5;

    private Integer utilisateurActifsN6;

    private Integer utilisateurActifsN7;

    private Integer utilisateurActifsN8;

    private Integer utilisateurActifsN9;

    private Integer utilisateurActifsN10;

    private Integer utilisateurActifsN11;

    public NombreUtilisateurActifOutputResources(NombreUtilisateurActif nbUtilisateur){
        setUtilisateurActifsN(nbUtilisateur.getUtilisateurActifsN());
        setUtilisateurActifsN1(nbUtilisateur.getUtilisateurActifsN1());
        setUtilisateurActifsN2(nbUtilisateur.getUtilisateurActifsN2());
        setUtilisateurActifsN3(nbUtilisateur.getUtilisateurActifsN3());
        setUtilisateurActifsN4(nbUtilisateur.getUtilisateurActifsN4());
        setUtilisateurActifsN5(nbUtilisateur.getUtilisateurActifsN5());
        setUtilisateurActifsN6(nbUtilisateur.getUtilisateurActifsN6());
        setUtilisateurActifsN7(nbUtilisateur.getUtilisateurActifsN7());
        setUtilisateurActifsN8(nbUtilisateur.getUtilisateurActifsN8());
        setUtilisateurActifsN9(nbUtilisateur.getUtilisateurActifsN9());
        setUtilisateurActifsN10(nbUtilisateur.getUtilisateurActifsN10());
        setUtilisateurActifsN11(nbUtilisateur.getUtilisateurActifsN11());
    }
}
