package com.ithive.beebeecarrest.model.sp;

import lombok.Data;

@Data
public class TauxOccupationSite {

    private Long sitId;

    private String sitNom;

    private Float tauxoccupationn;
    private Float tauxoccupationn1;
    private Float tauxoccupationn2;
    private Float tauxoccupationn3;
    private Float tauxoccupationn4;
    private Float tauxoccupationn5;
    private Float tauxoccupationn6;
    private Float tauxoccupationn7;
    private Float tauxoccupationn8;
    private Float tauxoccupationn9;
    private Float tauxoccupationn10;
    private Float tauxoccupationn11;

    public TauxOccupationSite(Long sitId, String sitNom, Float tauxoccupationn, Float tauxoccupationn1, Float tauxoccupationn2, Float tauxoccupationn3, Float tauxoccupationn4, Float tauxoccupationn5, Float tauxoccupationn6, Float tauxoccupationn7, Float tauxoccupationn8, Float tauxoccupationn9, Float tauxoccupationn10, Float tauxoccupationn11) {
        this.sitId = sitId;
        this.sitNom = sitNom;
        this.tauxoccupationn = tauxoccupationn;
        this.tauxoccupationn1 = tauxoccupationn1;
        this.tauxoccupationn2 = tauxoccupationn2;
        this.tauxoccupationn3 = tauxoccupationn3;
        this.tauxoccupationn4 = tauxoccupationn4;
        this.tauxoccupationn5 = tauxoccupationn5;
        this.tauxoccupationn6 = tauxoccupationn6;
        this.tauxoccupationn7 = tauxoccupationn7;
        this.tauxoccupationn8 = tauxoccupationn8;
        this.tauxoccupationn9 = tauxoccupationn9;
        this.tauxoccupationn10 = tauxoccupationn10;
        this.tauxoccupationn11 = tauxoccupationn11;
    }
}
