package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ithive.beebeecarrest.model.sp.ActiviteVehicule;

import lombok.Data;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the veh_vehicule database table.
 * 
 */
@Entity
@Table(name = "veh_vehicule")
@NamedQuery(name = "Vehicule.findAll", query = "SELECT v FROM Vehicule v")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "vehId")
//@NamedStoredProcedureQueries({
//		@NamedStoredProcedureQuery(name = "sp_GetActiviteVehiculeOrganismeForPeriode", procedureName = "sp_GetActiviteVehiculeOrganismeForPeriode", resultClasses = {
//				ActiviteVehicule.class }, parameters = {
//						@StoredProcedureParameter(name = "idOrganisme", type = Integer.class, mode = ParameterMode.IN),
//						@StoredProcedureParameter(name = "dateDebut", type = Date.class, mode = ParameterMode.IN),
//						@StoredProcedureParameter(name = "dateFin", type = Date.class, mode = ParameterMode.IN), }) })
//@SqlResultSetMapping(name = "ActiviteVehiculeResult", 
//		entities = {
//        @EntityResult(
//                entityClass = Vehicule.class,
//                fields = {
//                    @FieldResult(name = "vehId", column = "veh_id"),
//        			@FieldResult(name = "VEH_DateMiseEnService", column = "VEH_DATEMISEENSERVICE")}),
//        @EntityResult(
//                entityClass = DemandeLocation.class,
//                fields = {
//                    @FieldResult(name = "demId", column = "dem_id"),
//                    @FieldResult(name = "DEM_AllowCovoiturage", column = "DEM_AllowCovoiturage")}),
//        @EntityResult(
//                entityClass = Utilisateur.class,
//                fields = {
//                    @FieldResult(name = "utiId", column = "uti_id"),
//                    @FieldResult(name = "UTI_Nom", column = "UTI_Nom"),
//                    @FieldResult(name = "UTI_Prenom", column = "UTI_Prenom")
//                    }),
//        @EntityResult(
//                entityClass = DetailDemande.class,
//                fields = {
//            		@FieldResult(name = "dtdId", column = "dtd_id"),
//                    @FieldResult(name = "DTD_Date", column = "DTD_Date")
//                    }),
//        @EntityResult(
//                entityClass = EtatDemande.class,
//                fields = {
//            		@FieldResult(name = "edmId", column = "edm_id"),
//                    @FieldResult(name = "EDM_Libelle", column = "EDM_Libelle")
//                    }),
//        
//        })
public class Vehicule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "VEH_ID", unique = true, nullable = false)
	private long vehId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VEH_DATEFINSERVICE")
	private Date VEH_DateFinService;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VEH_DATEMISEENSERVICE", nullable = false)
	private Date VEH_DateMiseEnService;

	@Column(nullable = false, length = 40)
	private String VEH_Immatriculation;

	@Column(nullable = false)
	private int VEH_Kilometrage;
	
	@Column(length = 400)
	private String VEH_Details;
	
	@Column(nullable=true, length=400, name = "VEH_PHOTOURL")
	private String VEH_PhotoUrl;
	
	@Column(nullable=true, length=10, name = "VEH_COULEUR")
	private String VEH_Couleur;
	
//	@Column(nullable = false, length = 40)
//	private String VEH_Marque;
//
//	@Column(nullable = false, length = 60)
//	private String VEH_Modele;

	// bi-directional many-to-one association to CleClevehicule
	@OneToMany(mappedBy = "vehVehicule", fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Set<CleVehicule> cleClevehicules;

	// bi-directional many-to-one association to DemDemandelocation
	@OneToMany(mappedBy = "vehVehicule", fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Set<DemandeLocation> demDemandelocations;

	// bi-directional many-to-one association to EngEntretiengarage
	@OneToMany(mappedBy = "vehVehicule", fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Set<EntretienGarage> engEntretiengarages;

//	// bi-directional many-to-one association to PhvPhotovehicule
//	@OneToMany(mappedBy = "vehVehicule", fetch = FetchType.LAZY)
//	@JsonIdentityReference(alwaysAsId = true)
//	private Set<PhotoVehicule> phvPhotovehicules;

	// bi-directional many-to-one association to SitSite
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VEH_SIT_ID", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	private Site sitSite;

	//bi-directional many-to-one association to VehVehicule
	@ManyToOne
	@JoinColumn(name="VEH_MOD_ID", nullable=false)
//	@JsonManagedReference
	@JsonIdentityReference(alwaysAsId = true)
	private Modele modModele;
	
	public Vehicule() {
	}

//	public Vehicule(String marque) {
//		this();
//		this.setVEH_Marque(marque);
//	}

	public long getVehId() {
		return this.vehId;
	}

	public void setVehId(long vehId) {
		this.vehId = vehId;
	}

	public Date getVEH_DateFinService() {
		return this.VEH_DateFinService;
	}

	public void setVEH_DateFinService(Date VEH_DateFinService) {
		this.VEH_DateFinService = VEH_DateFinService;
	}

	public Date getVEH_DateMiseEnService() {
		return this.VEH_DateMiseEnService;
	}

	public void setVEH_DateMiseEnService(Date VEH_DateMiseEnService) {
		this.VEH_DateMiseEnService = VEH_DateMiseEnService;
	}

	public String getVEH_Immatriculation() {
		return this.VEH_Immatriculation;
	}

	public void setVEH_Immatriculation(String VEH_Immatriculation) {
		this.VEH_Immatriculation = VEH_Immatriculation;
	}

	public int getVEH_Kilometrage() {
		return this.VEH_Kilometrage;
	}

	public void setVEH_Kilometrage(int VEH_Kilometrage) {
		this.VEH_Kilometrage = VEH_Kilometrage;
	}

	
//	public String getVEH_Marque() {
//		return this.VEH_Marque;
//	}
//
//	public void setVEH_Marque(String VEH_Marque) {
//		this.VEH_Marque = VEH_Marque;
//	}
//
//	public String getVEH_Modele() {
//		return this.VEH_Modele;
//	}
//
//	public void setVEH_Modele(String VEH_Modele) {
//		this.VEH_Modele = VEH_Modele;
//	}

	public String getVEH_Details() {
		return VEH_Details;
	}

	public void setVEH_Details(String vEH_Details) {
		VEH_Details = vEH_Details;
	}
	
	public String getVEH_PhotoUrl() {
		return VEH_PhotoUrl;
	}

	public void setVEH_PhotoUrl(String vEH_PhotoUrl) {
		VEH_PhotoUrl = vEH_PhotoUrl;
	}
	
	public String getVEH_Couleur() {
		return VEH_Couleur;
	}

	public void setVEH_Couleur(String vEH_Couleur) {
		VEH_Couleur = vEH_Couleur;
	}

	public Modele getModModele() {
		return modModele;
	}

	public void setModModele(Modele modModele) {
		this.modModele = modModele;
	}

	public Set<CleVehicule> getCleClevehicules() {
		return this.cleClevehicules;
	}

	public void setCleClevehicules(Set<CleVehicule> cleClevehicules) {
		this.cleClevehicules = cleClevehicules;
	}

	public CleVehicule addCleClevehicule(CleVehicule cleClevehicule) {
		getCleClevehicules().add(cleClevehicule);
		cleClevehicule.setVehVehicule(this);

		return cleClevehicule;
	}

	public CleVehicule removeCleClevehicule(CleVehicule cleClevehicule) {
		getCleClevehicules().remove(cleClevehicule);
		cleClevehicule.setVehVehicule(null);

		return cleClevehicule;
	}

	public Set<DemandeLocation> getDemDemandelocations() {
		return this.demDemandelocations;
	}

	public void setDemDemandelocations(Set<DemandeLocation> demDemandelocations) {
		this.demDemandelocations = demDemandelocations;
	}

	public DemandeLocation addDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().add(demDemandelocation);
		demDemandelocation.setVehVehicule(this);

		return demDemandelocation;
	}

	public DemandeLocation removeDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().remove(demDemandelocation);
		demDemandelocation.setVehVehicule(null);

		return demDemandelocation;
	}

	public Set<EntretienGarage> getEngEntretiengarages() {
		return this.engEntretiengarages;
	}

	public void setEngEntretiengarages(Set<EntretienGarage> engEntretiengarages) {
		this.engEntretiengarages = engEntretiengarages;
	}

	public EntretienGarage addEngEntretiengarage(EntretienGarage engEntretiengarage) {
		getEngEntretiengarages().add(engEntretiengarage);
		engEntretiengarage.setVehVehicule(this);

		return engEntretiengarage;
	}

	public EntretienGarage removeEngEntretiengarage(EntretienGarage engEntretiengarage) {
		getEngEntretiengarages().remove(engEntretiengarage);
		engEntretiengarage.setVehVehicule(null);

		return engEntretiengarage;
	}

//	public Set<PhotoVehicule> getPhvPhotovehicules() {
//		return this.phvPhotovehicules;
//	}
//
//	public void setPhvPhotovehicules(Set<PhotoVehicule> phvPhotovehicules) {
//		this.phvPhotovehicules = phvPhotovehicules;
//	}
//
//	public PhotoVehicule addPhvPhotovehicule(PhotoVehicule phvPhotovehicule) {
//		getPhvPhotovehicules().add(phvPhotovehicule);
//		phvPhotovehicule.setVehVehicule(this);
//
//		return phvPhotovehicule;
//	}
//
//	public PhotoVehicule removePhvPhotovehicule(PhotoVehicule phvPhotovehicule) {
//		getPhvPhotovehicules().remove(phvPhotovehicule);
//		phvPhotovehicule.setVehVehicule(null);
//
//		return phvPhotovehicule;
//	}

	public Site getSitSite() {
		return this.sitSite;
	}

	public void setSitSite(Site sitSite) {
		this.sitSite = sitSite;
	}

}