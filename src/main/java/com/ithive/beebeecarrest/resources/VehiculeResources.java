package com.ithive.beebeecarrest.resources;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
public class VehiculeResources {

	private long id_vehicule;

	private String dateFinService;

	private String dateMiseEnService;

	private String immatriculation;

	private int kilometrage;

	private Long idMarque;
	
	private String marque;

	private Long idModele;
	
	private String modele;
	
	private Long idSite;

	private String nomSite;

	private String photo;

	private String details;
	
	private String couleur;
    
	public VehiculeResources() {
		super();
	}
	
}
