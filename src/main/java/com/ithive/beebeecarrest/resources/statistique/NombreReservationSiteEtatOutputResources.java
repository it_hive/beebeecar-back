package com.ithive.beebeecarrest.resources.statistique;

import com.ithive.beebeecarrest.model.sp.NombreReservationParSiteParEtat;
import lombok.Data;

@Data
public class NombreReservationSiteEtatOutputResources {

    private Long sitId;

    private String sitNom;

    private Long edmId;

    private String edmLibelle;

    private Integer demandesN;

    private Integer demandesN1;

    private Integer demandesN2;

    private Integer demandesN3;

    private Integer demandesN4;

    private Integer demandesN5;

    private Integer demandesN6;

    private Integer demandesN7;

    private Integer demandesN8;

    private Integer demandesN9;

    private Integer demandesN10;

    private Integer demandesN11;

    public NombreReservationSiteEtatOutputResources(NombreReservationParSiteParEtat nbResa){
        setSitId(nbResa.getSitId());
        setSitNom(nbResa.getSitNom());
        setEdmId(nbResa.getEdmId());
        setEdmLibelle(nbResa.getEdmLibelle());
        setDemandesN(nbResa.getDemandesN());
        setDemandesN1(nbResa.getDemandesN1());
        setDemandesN2(nbResa.getDemandesN2());
        setDemandesN3(nbResa.getDemandesN3());
        setDemandesN4(nbResa.getDemandesN4());
        setDemandesN5(nbResa.getDemandesN5());
        setDemandesN6(nbResa.getDemandesN6());
        setDemandesN7(nbResa.getDemandesN7());
        setDemandesN8(nbResa.getDemandesN8());
        setDemandesN9(nbResa.getDemandesN9());
        setDemandesN10(nbResa.getDemandesN10());
        setDemandesN11(nbResa.getDemandesN11());
    }
}
