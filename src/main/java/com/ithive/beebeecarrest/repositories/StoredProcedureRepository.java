package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.Vehicule;
import com.ithive.beebeecarrest.model.sp.*;

import java.util.Date;
import java.util.List;

public interface StoredProcedureRepository {

	List<ActiviteVehicule> getActiviteVehiculeOrganismeForPeriode(
			Long organismeId, 
			Date dateDebut, 
			Date dateFin
			);
	
	List<Vehicule> getAllActifVehiculeByOrganisme(
			Long organismeId, 
			Date dateDebut, 
			Date dateFin
			);
	
	List<ReservationToday> getReservationsForToday(
			Long organismeId,
			Date dateRef);

	List<Vehicule> getVehiculesDispoForPeriode(
			Long organismeId,
			Date dateDebut,
			Date dateFin
	);

	List<TauxOccupationVehicule> getTauxOccupationVehiculeForLastYear(
			Long organismeId,
			Date dateReference
	);

	List<TauxOccupationSite> getTauxOccupationSiteForLastYear(
			Long organismeId,
			Date dateReference
	);

	EtatParc getEtatParc(
			Long organismeId,
			Date dateReference
	);

	List<NombreReservationParSite> getNombreReservationParSite(
			Long organismeId,
			Date dateReference
	);

	List<NombreReservationParSiteParEtat> getNombreReservationParSiteParEtat(
			Long organismeId,
			Date dateReference
	);

	List<NombreUtilisateurActif> getNombreUtilisateurActifMonthByMonth(
		Long organismeId,
		Date dateReference
	);
}
