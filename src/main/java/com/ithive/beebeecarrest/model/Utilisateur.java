package com.ithive.beebeecarrest.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Set;


/**
 * The persistent class for the uti_utilisateur database table.
 * 
 */

@Entity
@Table(name="uti_utilisateur")
@NamedQuery(name="Utilisateur.findAll", query="SELECT u FROM Utilisateur u")
//@JsonIdentityInfo(
//		  generator = ObjectIdGenerators.PropertyGenerator.class, 
//		  property = "utiId")
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="UTI_ID", unique=true, nullable=false)
	private Long utiId;

	@Column(name="uti_haspermis",nullable=false)
	private byte UTI_HasPermis;

	@Column(name="UTI_Archived",nullable=false)
	private byte UTI_Archived;

	@Column(name="UTI_ISADMIN",nullable=false)
	private byte isAdmin;
	
	@Column(name="UTI_Mail", nullable=false, length=100)
	private String mail;

	@Column(name="uti_motdepasse", nullable=false, length=100)
	private String UTI_MotDePasse;

	@Column(nullable=false, length=100)
	private String UTI_Nom;

	@Column(nullable=false, length=100)
	private String UTI_Prenom;

	@Column(nullable=false, length=20)
	private String UTI_Telephone;

	@Column(nullable=true, length=400, name = "UTI_PHOTOURL")
	private String UTI_PhotoUrl;
	
	//bi-directional many-to-one association to CleClevehicule
	@OneToMany(mappedBy="utiUtilisateur")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<CleVehicule> cleClevehicules;

	//bi-directional many-to-one association to ComCommentairedemande
	@OneToMany(mappedBy="utiUtilisateur")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<CommentaireDemande> comCommentairedemandes;

	//bi-directional many-to-one association to DemDemandelocation
	@OneToMany(mappedBy="utiUtilisateur")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<DemandeLocation> demDemandelocations;

	@OneToMany(mappedBy="utiValideur")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<DemandeLocation> demDemandelocationsValideur;
	
	//bi-directional many-to-one association to OrgOrganisme
	@ManyToOne
	@JoinColumn(name="UTI_ORG_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Organisme orgOrganisme;

	public Utilisateur() {
	}

	public byte getIsAdmin() {
		return this.isAdmin;
	}

	public void setIsAdmin(byte UTI_IsAdmin) {
		this.isAdmin = UTI_IsAdmin;
	}

	public byte getUTI_IsArchived() {
		return this.UTI_Archived;
	}

	public void setUTI_IsArchived(byte UTI_Archived) {
		this.UTI_Archived = UTI_Archived;
	}
	
	public Long getUtiId() {
		return this.utiId;
	}

	public void setUtiId(Long utiId) {
		this.utiId = utiId;
	}

	public byte getUTI_HasPermis() {
		return this.UTI_HasPermis;
	}

	public void setUTI_HasPermis(byte UTI_HasPermis) {
		this.UTI_HasPermis = UTI_HasPermis;
	}

	public String getmail() {
		return this.mail;
	}

	public void setuti_mail(String UTI_Mail) {
		this.mail = UTI_Mail;
	}

	public String getUTI_MotDePasse() {
		return this.UTI_MotDePasse;
	}

	public void setUTI_MotDePasse(String UTI_MotDePasse) {
		this.UTI_MotDePasse = UTI_MotDePasse;
	}

	public String getUTI_Nom() {
		return this.UTI_Nom;
	}

	public void setUTI_Nom(String UTI_Nom) {
		this.UTI_Nom = UTI_Nom;
	}

	public String getUTI_Prenom() {
		return this.UTI_Prenom;
	}

	public void setUTI_Prenom(String UTI_Prenom) {
		this.UTI_Prenom = UTI_Prenom;
	}

	public byte getUTI_Archived() {
		return UTI_Archived;
	}

	public void setUTI_Archived(byte uTI_Archived) {
		UTI_Archived = uTI_Archived;
	}

	public String getUTI_PhotoUrl() {
		return UTI_PhotoUrl;
	}

	public void setUTI_PhotoUrl(String uTI_PhotoUrl) {
		UTI_PhotoUrl = uTI_PhotoUrl;
	}

	public String getUTI_Telephone() {
		return this.UTI_Telephone;
	}

	public void setUTI_Telephone(String UTI_Telephone) {
		this.UTI_Telephone = UTI_Telephone;
	}

	public Set<CleVehicule> getCleClevehicules() {
		return this.cleClevehicules;
	}

	public void setCleClevehicules(Set<CleVehicule> cleClevehicules) {
		this.cleClevehicules = cleClevehicules;
	}

	public CleVehicule addCleClevehicule(CleVehicule cleClevehicule) {
		getCleClevehicules().add(cleClevehicule);
		cleClevehicule.setUtiUtilisateur(this);

		return cleClevehicule;
	}

	public CleVehicule removeCleClevehicule(CleVehicule cleClevehicule) {
		getCleClevehicules().remove(cleClevehicule);
		cleClevehicule.setUtiUtilisateur(null);

		return cleClevehicule;
	}

	public Set<CommentaireDemande> getComCommentairedemandes() {
		return this.comCommentairedemandes;
	}

	public void setComCommentairedemandes(Set<CommentaireDemande> comCommentairedemandes) {
		this.comCommentairedemandes = comCommentairedemandes;
	}

	public CommentaireDemande addComCommentairedemande(CommentaireDemande comCommentairedemande) {
		getComCommentairedemandes().add(comCommentairedemande);
		comCommentairedemande.setUtiUtilisateur(this);

		return comCommentairedemande;
	}

	public CommentaireDemande removeComCommentairedemande(CommentaireDemande comCommentairedemande) {
		getComCommentairedemandes().remove(comCommentairedemande);
		comCommentairedemande.setUtiUtilisateur(null);

		return comCommentairedemande;
	}

	public Set<DemandeLocation> getDemDemandelocations() {
		return this.demDemandelocations;
	}

	public void setDemDemandelocations(Set<DemandeLocation> demDemandelocations) {
		this.demDemandelocations = demDemandelocations;
	}

	public DemandeLocation addDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().add(demDemandelocation);
		demDemandelocation.setUtiUtilisateur(this);

		return demDemandelocation;
	}

	public DemandeLocation removeDemDemandelocation(DemandeLocation demDemandelocation) {
		getDemDemandelocations().remove(demDemandelocation);
		demDemandelocation.setUtiUtilisateur(null);

		return demDemandelocation;
	}

	public Organisme getOrgOrganisme() {
		return this.orgOrganisme;
	}

	public void setOrgOrganisme(Organisme orgOrganisme) {
		this.orgOrganisme = orgOrganisme;
	}

}