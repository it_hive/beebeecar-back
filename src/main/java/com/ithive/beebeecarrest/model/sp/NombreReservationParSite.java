package com.ithive.beebeecarrest.model.sp;

import lombok.Data;

@Data
public class NombreReservationParSite {

    private Long sitId;

    private String sitNom;

    private Integer demandesN;

    private Integer demandesN1;

    private Integer demandesN2;

    private Integer demandesN3;

    private Integer demandesN4;

    private Integer demandesN5;

    private Integer demandesN6;

    private Integer demandesN7;

    private Integer demandesN8;

    private Integer demandesN9;

    private Integer demandesN10;

    private Integer demandesN11;

    public NombreReservationParSite(Long sitId, String sitNom, Integer demandesN, Integer demandesN1, Integer demandesN2, Integer demandesN3, Integer demandesN4, Integer demandesN5, Integer demandesN6, Integer demandesN7, Integer demandesN8, Integer demandesN9, Integer demandesN10, Integer demandesN11) {
        this.sitId = sitId;
        this.sitNom = sitNom;
        this.demandesN = demandesN;
        this.demandesN1 = demandesN1;
        this.demandesN2 = demandesN2;
        this.demandesN3 = demandesN3;
        this.demandesN4 = demandesN4;
        this.demandesN5 = demandesN5;
        this.demandesN6 = demandesN6;
        this.demandesN7 = demandesN7;
        this.demandesN8 = demandesN8;
        this.demandesN9 = demandesN9;
        this.demandesN10 = demandesN10;
        this.demandesN11 = demandesN11;
    }
}
