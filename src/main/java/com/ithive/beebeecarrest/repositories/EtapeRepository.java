package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.DemandeLocation;
import com.ithive.beebeecarrest.model.Etape;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EtapeRepository extends JpaRepository<Etape, Long> {

    List<Etape> findAllByDemDemandelocationOrderByOrdreAsc(DemandeLocation demande);

//    void deleteAllByDemDemande(DemandeLocation demande);
}
