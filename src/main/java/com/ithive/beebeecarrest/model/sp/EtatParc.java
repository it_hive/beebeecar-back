package com.ithive.beebeecarrest.model.sp;

import lombok.Data;

@Data
public class EtatParc {

    private Integer vehiculeDispo;

    private Integer vehiculeIndispo;
}
