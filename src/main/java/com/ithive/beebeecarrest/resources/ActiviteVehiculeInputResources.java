package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class ActiviteVehiculeInputResources {

    private String dateDeb;
    
    private String dateFin;
}
