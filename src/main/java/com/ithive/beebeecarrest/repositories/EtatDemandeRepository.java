package com.ithive.beebeecarrest.repositories;

import com.ithive.beebeecarrest.model.EtatDemande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtatDemandeRepository extends JpaRepository<EtatDemande, Long> {

    EtatDemande findFirstByLibelleEquals(String libelle);
}
