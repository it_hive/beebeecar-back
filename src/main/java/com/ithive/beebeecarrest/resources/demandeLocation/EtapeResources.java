package com.ithive.beebeecarrest.resources.demandeLocation;

import com.ithive.beebeecarrest.model.Etape;
import lombok.Data;

@Data
public class EtapeResources {

    private Long idEtape;

    private Long idDemandelocation;

    private String adresse;

    private int ordre;

    public EtapeResources(){
        super();
    }

    public EtapeResources(Etape etape){
        setIdEtape(etape.getEtapeId());
        setIdDemandelocation(etape.getDemDemande().getDemId());
        setAdresse(etape.getAdresse());
        setOrdre(etape.getOrdre());
    }
}
