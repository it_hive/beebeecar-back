package com.ithive.beebeecarrest.model;



import com.fasterxml.jackson.annotation.JsonIdentityReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="etp_etape")
@NamedQuery(name="Etape.findAll", query="SELECT e FROM Etape e")
public class Etape implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ETP_ID", unique=true, nullable=false)
    private Long etapeId;


    //bi-directional many-to-one association to DemandeLocation
    @ManyToOne
    @JoinColumn(name="ETP_DEM_ID", nullable=false)
    @JsonIdentityReference(alwaysAsId = true)
    private DemandeLocation demDemandelocation;


    @Column(name="ETP_Adresse", length=200, nullable = false)
    private String adresse;


    @Column(name="ETP_Ordre", nullable = false)
    private int ordre;

    public Long getEtapeId() {
        return etapeId;
    }

    public void setEtapeId(Long etapeId) {
        this.etapeId = etapeId;
    }

    public DemandeLocation getDemDemande() {
        return demDemandelocation;
    }

    public void setDemDemande(DemandeLocation demDemande) {
        this.demDemandelocation = demDemande;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }
}
