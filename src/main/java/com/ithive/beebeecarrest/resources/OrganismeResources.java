package com.ithive.beebeecarrest.resources;

import lombok.Data;

@Data
public class OrganismeResources {

    private Long idOrganisme;
    private String nom;

}
