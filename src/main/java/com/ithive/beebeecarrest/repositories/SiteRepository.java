package com.ithive.beebeecarrest.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ithive.beebeecarrest.model.Site;

public interface SiteRepository extends JpaRepository<Site, Long> {

	List<Site> findAllByOrgOrganisme_orgId(Long orgId);
	
}
