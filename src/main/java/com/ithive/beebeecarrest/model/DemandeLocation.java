package com.ithive.beebeecarrest.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the dem_demandelocation database table.
 * 
 */
@Entity
@Table(name="dem_demandelocation")
@NamedQuery(name="DemandeLocation.findAll", query="SELECT d FROM DemandeLocation d")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "demId")
public class DemandeLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DEM_ID", unique=true, nullable=false)
	private Long demId;

	@Column(name="DEM_ALLOWCOVOITURAGE", nullable=false)
	private byte DEM_AllowCovoiturage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEM_DATEDEBUT", nullable=false)
	private Date demDatedebut;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEM_DATEDEMANDE", nullable=false)
	private Date demDateDemande;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEM_DATEFIN")
	private Date demDatefin;

	@Column(length=1000, name = "DEM_Commentairelibre")
	private String commentaireLibre;

	@Column(length=1000, name = "DEM_Commentairecovoiturage")
	private String commentaireCovoiturage;

	//bi-directional many-to-one association to ComCommentairedemande
	@OneToMany(mappedBy="demDemandelocation")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<CommentaireDemande> comCommentairedemandes;

	//bi-directional many-to-one association to EdmEtatdemande
	@ManyToOne
	@JoinColumn(name="DEM_EDM_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private EtatDemande edmEtatdemande;

	//bi-directional many-to-one association to UtiUtilisateur
	@ManyToOne
	@JoinColumn(name="DEM_UTI_ID", nullable=false)
	@JsonIdentityReference(alwaysAsId = true)
	private Utilisateur utiUtilisateur;

	//bi-directional many-to-one association to VehVehicule
	@ManyToOne
	@JoinColumn(name="DEM_VEH_ID")
//	@JsonManagedReference
	@JsonIdentityReference(alwaysAsId = true)
	private Vehicule vehVehicule;


	//bi-directional many-to-one association to sitSite
	@ManyToOne
	@JoinColumn(name = "DEM_SIT_ID", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	private Site sitSite;

	//bi-directional many-to-one association to DtdDetaildemande
	@OneToMany(mappedBy="demDemandelocation")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<DetailDemande> dtdDetaildemandes;

	@ManyToOne
	@JoinColumn(name="DEM_UTI_VALIDEURID")
	@JsonIdentityReference(alwaysAsId = true)
	private Utilisateur utiValideur;

	//bi-directional many-to-one association to etpEtape
	@OneToMany(mappedBy="demDemandelocation")
	@JsonIdentityReference(alwaysAsId = true)
	private Set<Etape> etpEtape;
	
	public DemandeLocation() {
	}

	public Long getDemId() {
		return this.demId;
	}

	public void setDemId(Long demId) {
		this.demId = demId;
	}

	public byte getDEM_AllowCovoiturage() {
		return this.DEM_AllowCovoiturage;
	}

	public void setDEM_AllowCovoiturage(byte DEM_AllowCovoiturage) {
		this.DEM_AllowCovoiturage = DEM_AllowCovoiturage;
	}

	public Date getDemDatedebut() {
		return this.demDatedebut;
	}

	public void setDemDatedebut(Date demDatedebut) {
		this.demDatedebut = demDatedebut;
	}

	public Date getDemDateDemande() {
		return this.demDateDemande;
	}

	public void setDemDateDemande(Date demDateDemande) {
		this.demDateDemande = demDateDemande;
	}

	public Date getDemDatefin() {
		return this.demDatefin;
	}

	public void setDemDatefin(Date demDatefin) {
		this.demDatefin = demDatefin;
	}

	public Set<CommentaireDemande> getComCommentairedemandes() {
		return this.comCommentairedemandes;
	}

	public void setComCommentairedemandes(Set<CommentaireDemande> comCommentairedemandes) {
		this.comCommentairedemandes = comCommentairedemandes;
	}

	public CommentaireDemande addComCommentairedemande(CommentaireDemande comCommentairedemande) {
		getComCommentairedemandes().add(comCommentairedemande);
		comCommentairedemande.setDemDemandelocation(this);

		return comCommentairedemande;
	}

	public CommentaireDemande removeComCommentairedemande(CommentaireDemande comCommentairedemande) {
		getComCommentairedemandes().remove(comCommentairedemande);
		comCommentairedemande.setDemDemandelocation(null);

		return comCommentairedemande;
	}
	public Set<Etape> getEtpEtape() {
		return this.etpEtape;
	}

	public void setEtpEtape(Set<Etape> etpEtape) {
		this.etpEtape = etpEtape;
	}

	public EtatDemande getEdmEtatdemande() {
		return this.edmEtatdemande;
	}

	public void setEdmEtatdemande(EtatDemande edmEtatdemande) {
		this.edmEtatdemande = edmEtatdemande;
	}

	public Utilisateur getUtiValideur() {
		return this.utiValideur;
	}

	public void setUtiValideur(Utilisateur utiValideur) {
		this.utiValideur = utiValideur;
	}


	public Utilisateur getUtiUtilisateur() {
		return this.utiUtilisateur;
	}

	public void setUtiUtilisateur(Utilisateur utiUtilisateur) {
		this.utiUtilisateur = utiUtilisateur;
	}

	public Vehicule getVehVehicule() {
		return this.vehVehicule;
	}

	public void setVehVehicule(Vehicule vehVehicule) {
		this.vehVehicule = vehVehicule;
	}


	public Site getSitSite() {
		return this.sitSite;
	}

	public void setSitSite(Site sitSite) {
		this.sitSite = sitSite;
	}

	public Set<DetailDemande> getDtdDetaildemandes() {
		return this.dtdDetaildemandes;
	}

	public void setDtdDetaildemandes(Set<DetailDemande> dtdDetaildemandes) {
		this.dtdDetaildemandes = dtdDetaildemandes;
	}

	public DetailDemande addDtdDetaildemande(DetailDemande dtdDetaildemande) {
		getDtdDetaildemandes().add(dtdDetaildemande);
		dtdDetaildemande.setDemDemandelocation(this);

		return dtdDetaildemande;
	}

	public DetailDemande removeDtdDetaildemande(DetailDemande dtdDetaildemande) {
		getDtdDetaildemandes().remove(dtdDetaildemande);
		dtdDetaildemande.setDemDemandelocation(null);

		return dtdDetaildemande;
	}

	public String getCommentaireLibre() {
		return commentaireLibre;
	}

	public void setCommentaireLibre(String commentaireLibre) {
		this.commentaireLibre = commentaireLibre;
	}

	public String getCommentaireCovoiturage() {
		return commentaireCovoiturage;
	}

	public void setCommentaireCovoiturage(String commentaireCovoiturage) {
		this.commentaireCovoiturage = commentaireCovoiturage;
	}
}